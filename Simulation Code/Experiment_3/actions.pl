%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This module contains the action defintion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(actions,
	[action/3,
	action_list/1,
	reflect_knowledgebase/0,
	perform_actions/0,
	perform_action_with_Agent/1,
	publication/4 ]).
:- use_module('agents.pl').
:- dynamic(action_list/1, publication/4).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Basic functions to perform actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% reflect the knowlegebase (update beliefs)
reflect_knowledgebase:-
	forall(agent(_,Agent_ID,_),
		action(reflect_knowledgebase, AgentID, _Options) ).

%%% Finds all active agents and let them do actions
perform_actions:-
	message(apply_actions,_),
	findall(Agent_ID,agent(_,Agent_ID,_),List_Of_Agents),
	perform_action_with_Agent(List_Of_Agents).

%%% perform a random action of a specific agent
perform_action_with_Agent([]).
perform_action_with_Agent([Agent|Other_Agents]):-
	action_list(List_Of_Actions),
	%random_element_from_list(List_Of_Actions,Action),
	random_member(Action,List_Of_Actions),
	sim_time(Tick),
	% is Agent still active or does Agent leave the system?
	( check_if_agent_still_active(Agent),
		call(action(Action,Agent,Tick));
		true
	),
	perform_action_with_Agent(Other_Agents).

% reduce ressources of given agent by spcefied amount
spend_resources(AgentID,ResourcesToSpend):-
  agent(Type,AgentID, Attributes),
  Resources = Attributes.get(resources),
  NewResources is Resources - ResourcesToSpend,
  retractall(agent(Type,AgentID, Attributes)),
  assertz(agent(Type,AgentID,Attributes.put(resources,NewResources))).

% Bridged in this experiment
action(reflectknowledge, _AgentID , _Options):-
  true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% List of available actions in the simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Static definition of possible actions
/* action_list([doing_empirical_research,
	generating_ressources,
	studying,
	make_publication]).
*/

% Dynamic definition of possible actions (action may change over simulation run)
action_list(ListOfActions):-
  generate_action_list_from_database(ListOfActions).

generate_action_list_from_database(ListOfActions):-
  retractall(action_list(_)),
  findall(ActionName,clause(action(ActionName,_,_),ClauseList),ListOfActions),
  asserta(action_list(ListOfActions)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Definition of specific actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% making publication %%%

action(make_publication, AgentID, _Options):-
  agent(_Type,AgentID, Attributes),
  Resources = Attributes.get(resources),
  sim_time(Tick),
  Resources >= 2,
  spend_resources(AgentID,2),
  findall(BeliefContent,belief(AgentID,_,BeliefContent),BList),
  random_member(Belief,BList),
  uuid(PubID,[]),
  assertz(publication(AgentID,Tick,PubID,Belief)).

%%%  studying publications %%%
action(studying, AgentID, _Options):-
  agent(Type,AgentID, Attributes),
  Resources = Attributes.get(resources),
  Resources >= 1,
  spend_resources(AgentID,1),
  action(make_publication, AgentID, _Options),
  findall(PubID,publication(Atr,PT,PubID,PC),PublicationList),
  random_member(RndID,PublicationList),
  publication(Author,PubTime,RndID,PubContent),
  %CHECK DATA WITH OWN Belief
  %UPDATE reputation
  NPub = PubContent.put(original_author,Author),
  sim_time(Tick),
  assertz(belief(AgentID,Tick,NPub)).

% Application for founding; DFG Found
action(generating_ressources, AgentID, Options):-
  agent(Type,AgentID, Attributes),
  Resources = Attributes.get(resources),
  Reputation = Attributes.get(reputation),
  maxFounding(MaxFounding),
  maxInvestment(MaxInvestment),
  random(0,MaxFounding,ResourcesRandom),
  random(0,MaxInvestment,Investment),
  NewResources is (round(ResourcesRandom * Reputation) + Resources) - (round(Investment-Reputation)),
  retractall(agents:agent(_,AgentID,_)),
  %NewAttributes = Attributes.put(resources,Resources),
  %assertz(agents:agent(Type,AgentID,Attributes.put(resources,NewResources)))
  assertz(agent(Type,AgentID,Attributes.put(resources,NewResources))).



action(doing_empirical_research, AgentID, Options):-
  sim_time(Tick),
  agent(Type,AgentID, Attributes),
  Resources = Attributes.get(resources),
  Resources >= 3,
  random(1,3,AP),
  spend_resources(AgentID,AP),
  random(0,100,Observation),
  createFact(FactID,Observation),

  BeliefContent = belief_attributes{fact:FactID,degree:1.0},
  assertz(belief(AgentID,Tick,BeliefContent)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% action_type(Name_of_Action,Type_of_Agent)
%%% determines which action will be used by wich agent type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
