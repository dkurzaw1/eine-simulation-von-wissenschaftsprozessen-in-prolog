:- module(theory,[theory/2]).

theory(t2,X):-
  not(0 is X mod 2).

theory(t2e,X):-
  not(0 is X mod 2);
  X is 2.

theory(t1,X):-
  isPrime(X).

%%% Support definitions %%% 

divisible(X,Y) :- 0 is X mod Y, !.
divisible(X,Y) :- X > Y+1, divisible(X, Y+1).
isPrime(2) :- true,!.
isPrime(X) :- X < 2,!,false.
isPrime(X) :- not(divisible(X, 2)).
