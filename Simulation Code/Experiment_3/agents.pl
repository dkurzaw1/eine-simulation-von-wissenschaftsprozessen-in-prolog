%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prolog Database and definition: Agents %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-module(agents,
  [agent/3,
  belief/3,
  construct_agent/0,
  random_belief_for_each_agent/0,
  specific_belief_for_set_of_agents/2,
  potential_new_agents/0,
  potential_new_agents_p/0,
  career_decision/0,
  check_if_agent_still_active/1,
  agent_active/1,
  check_if_agent_active/0,
  check_agent_active/2]).

:-dynamic([belief/3,agent/3]).

potential_new_agents:-
	random(1,10,Rnd),
	foreach(between(0,Rnd,_),potential_new_agents_p).

potential_new_agents_p:-
	random(0,10,Rnd),
	( Rnd > 3,
	construct_agent );
	true.

check_if_agent_active:- 
  list_of_active_agents(ListOfActiveAgents),

  forall(member(AgentID,ListOfActiveAgents), agent_active(AgentID)).


%Is true, if given agent in tick active; fail if agent is inactive
check_if_agent_still_active(Agent):-
  agent(Type,Agent,Properties),
  (	Inactive = Properties.get(inactive) -> (fail);
	  (true) ).

agent_age(Agent,Age):-
  sim_time(Tick),
	agent(Type,Agent,Properties),
	Created = Properties.get(created),
	Age is Tick - Created.

set_agent_inactive(Agent):-
agent(Type,Agent,Properties),
  retract(agent(Type,Agent,Properties)),
  sim_time(Tick),
  NewProp = Properties.put(inactive,Tick),
  assert(agent(Type,Agent,NewProp)).

agent_active(Agent):-
  agent_age(Agent,Age),
  (check_agent_active(Agent, Age), fail; true).


check_agent_active(Agent, Age):-
  Age < 6, 
  random(0,100,Random),
  Random < 5, 
  set_agent_inactive(Agent),!.

check_agent_active(Agent, Age):-
% 0.05(x-45)²+55
  Age >= 45
  Probability is (0.05*(Age-45)^2+55),
  random(0,100,Random),
  Random > Probability,
  set_agent_inactive(Agent),!.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Construct Agents  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% constructing simple agent; no special attributes
construct_agent:-
  % Select a random type from list of possible types (defined by type/1), P(A) = 0,5%
  list_of_types(List),
  random_member(Type,List),
  % Create unique ID for identification; adds AGENT#id for easy handling
  uuid(ID),
  string_concat("AGENT#", ID, AgentID),
  sim_time(Tick),
  % gives agent action points (fixed or random, see configuration.pl)
  default_resources(Res),
  % finaly creates the agent
  assertz( agent(Type,AgentID,properties{created:Tick, resources:Res, reputation:1.0, ability:1.0} ) ).

% provides list of types; types might be dynamic
list_of_types(List):-
  findall(Type,agents:types(Type),List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agents set of random beliefs for initialisation  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives each existing agent a random belief; used for initalisation
random_belief_for_each_agent:-
  find_all_agents(List_of_Agents),
  find_all_facts(List_of_Facts),

  % Give every Agent a random belief
  ( member(Agent,List_of_Agents),
    %random_element_from_list(List_of_Facts,FactID),
    random_member(FactID, List_of_Facts),
  %  uuid(UUID),
    sim_time(Tick),
    BeliefContent = belief_attributes{fact:FactID,degree:1.0},
    assert( belief(Agent, Tick, BeliefContent) ),
    fail ; true
  ).

  %Find all possible facts
  %Approximation?
  %Wrong facts?
  %Create a random belief ...

% Protyp for automaticly construct agents
% agent_prototyp( [ [Name_of_entety,[Options_or_Range] ], (...) ] )
% agent_prototyp([ [type,[type1,type2] ], [id,[] ], [options,[] ] ] ).

% gives a specfic set of agents a random belief
specific_belief_for_set_of_agents(Set_Of_AgentIDs,FactID):-
  ( member(Agent,Set_Of_AgentIDs),
    sim_time(Tick),
    assert( belief(Agent, Tick, FactID) ),
    fail ; true
  ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agent a random beliefs %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives a specfic agent a random belief
% GGF. LÖSCHEN UND specific_belief_for_set_of_agents verwenden/2 ?
random_belief_for_agent(AgentID):-
  find_all_facts(List_of_Facts),
  %random_element_from_list(List_of_Facts,FactID),
  random_member(FactID,List_of_Facts),
  sim_time(Tick),
  assert( belief(AgentID, Tick, FactID) ),
  fail ; true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agent Types  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

types(type1).
types(type2).










/*
check_agent_still_active(Agent):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	agent(Type,Agent,Properties),
  agent_age(Agent,Age),
% Agent over max working time then not agent_active
  ( Age >= MaxWorkingTime -> ;
    Age < MaxWorkingTime

    )
% Probability of the agent dropping out in advance
*/

% decides if agent leaves the system (get inactive) for defined reasons
/*
career_decision:-
  forall( ( agent(Type,Agent,Properties) ),
    reason_for_leave_system(Agent) ).

reason_for_leave_system(Agent):-
  agent_active(Agent)->
  (
  reason_for_leave_system(Reason,Agent),
  fail;true); true.

reason_for_leave_system(carrer_switch,Agent):-
  random(0,100,Random),
  Random < 80,
  true.
  %set_agent_inactive(Agent).


reason_for_leave_system(carrer_switch,Agent):-
  random(0,100,Random),
  Random < 20,
  set_agent_inactive(Agent).

reason_for_leave_system(no_contract,Agent):-
  agent_age(Agent,Age),
  Age<10,
  random(0,100,Random),
  Random < 50,
  set_agent_inactive(Agent).

reason_for_leave_system(bad_health,Agent):-
  agent_age(Agent,Age),
  Age>30,
  random(0,100,Random),
  Random < 2,
  set_agent_inactive(Agent).

reason_for_leave_system(age,Agent):-
  maxWorkingTime(MaxWorkingTime),
  agent_age(Agent,Age),
  MaxWorkingTime=<Age,
  set_agent_inactive(Agent).

reason_for_leave_system(stay,Agent,Tick,MaxWorkingTime).

*/

/*
% Young and leave system
check_agent_active(Agent, Age):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	Age < MaxWorkingTime,
	agent(Type,Agent,Properties),
	random_between(0,75,Rnd),
	random_between(0,50,High),
  Rnd < High ,
  set_agent_inactive(Agent).

*/
% Old and leave system
