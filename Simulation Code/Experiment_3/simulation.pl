/**
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROLOG SOCIAL SIMULATION SYSTEM
%%% WRITTEN BY DANIEL KURZAWE
%%% -------------------------
%%% CONTACT: Daniel Kurzawe
%%% LICENCE: GNU/GPL V.3 (?) http://www.gnu.org/copyleft/gpl.html
%%% VERSION: ALPHA
%%% WEBSITE: daniel-kurzawe.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% For debuging %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Visualisation of program structure %%%
% :- use_module(library(callgraph)).
%%% run command:  module_dotpdf(user,[method(unflatten([fl(4),c(4)]))]).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moduls %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% :- module(simulation).

:- style_check(-singleton).
%:- module(kernel,[sim_time/1]).

%%% CSV file output library %%%
:- use_module(library(csv)).

%%% Action definitions and logic %%%
:- use_module('utils.pl').

%%% Action definitions and logic %%%
:- use_module('configuration.pl').

%%% Action definitions and logic %%%
:- use_module('actions.pl').

%%% Agents definitions and logic %%%
:- use_module('agents.pl').

%%% Create laws %%%
:- use_module('create_world.pl').

%%% World facts database %%%
:- use_module('world_facts.pl').

%%% Screen output %%%
:- use_module('screen_output.pl').

%%% File output %%%
:- use_module('file_output.pl').

%%% Publication addition %%%
:- use_module('publication.pl').

%%% For influxdb and grafana (monitoring)
:- use_module(library(http/http_client)).

% Dynamic defintion for predicates, acting as "global variables"
:- dynamic(sim_time/1, run_id/1, experiment_id/1, experiment_dir/1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initalisation of variables and Constants %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- cls.
version('0.5').
sim_time(1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation kernal and logic %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Main loop %%%

%%% Exit conditions for the end of a simulation run %%%
run(End,End):- message(finish,_).

%%% Main loop definition %%%
run(Counter,End):-
	Counter1 is Counter + 1,
	message(tick,Counter),
	tick,
% MONITORING
	thread_create_in_pool(main, monitor_status, _ID, [stack_limit(2 000 000 000)] ),
	retractall(sim_time(_)),
	assert(sim_time(Counter1)),
	run(Counter1,End).


monitor_status:-
%	thread_pool_property(main,backlog(B)),
%	writeln(B),
	find_all_agents(Agents),
	length(Agents, Number_Of_Agents),
	atom_concat('simulation,run=ID,tick=1 agents=',Number_Of_Agents,Atom),
	http_post( [protocol(http),	
		host('192.168.0.11'), 
		port(8086), 
		path('/write?db=mydb')], 
		atom(Atom),
		_R, []).
	

%%% Processes for single tick %%%
tick:-
	% reflect_knowledgebase,
	check_if_agent_active,
%	career_decision,
	potential_new_agents,
	perform_actions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Additional functions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation database %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Prolog Database: World Facts %%%
% This section is for additional world facts, iff not definied in world_facts.pl

% Die Welt wird durch Fakten objektive Fakten bestimmt, welche durch die Agenten erfahren werden können.
% Hierbei ist in dieser Version keine Subjektivität in Form von Messunschärfer, Interpretation o.ä. berücksichtigt
% Hier etwas wie world(fact(...),zeitpunkt(tick),).? oder ist die Welt auch ein Agent? Glaube nicht, da nicht eigene Handlugen ausführt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation initialisiation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% creates a given number of agents %%%
create_agents(Number_Of_Agents):-
	foreach(between(1, Number_Of_Agents, _X), construct_agent).

%%% create a given number of facts
create_facts(Number_Of_Facts):-
	foreach(between(1, Number_Of_Facts, _),generate_fact).

%%% assign a given number of random beliefs to each agent
assign_beliefs(Number_Of_Beliefs_Per_Agent):-
	foreach(between(1, Number_Of_Beliefs_Per_Agent, _),random_belief_for_each_agent).

%%% initalisation of a simulation run %%%
initialisation(run):-
% Reset internal simulation time to tick 1
	asserta(sim_time(1)),
% Create specific ID for this run
	run_id,
	run_id(Run_ID),
	% Create folder for saving files for this run
	make_directory(Run_ID),
% Create simulated environment (laws, objects,...)
	
	% create_world,
	
	% Create agents for the first tick
	number_of_agents_at_start(Number_Of_Agents),
	create_agents(Number_Of_Agents),
	number_of_facts_at_start(Number_Of_Facts),
	create_facts(Number_Of_Facts),
	assign_beliefs(5), % Sinn dieser Konstanten? In Config übertragen?
	findall(agent(Type,ID,Attributes),
	agent(Type,ID,Attributes),List_Of_Agents),
	create_first_run(List_Of_Agents).

%%% setup for the first run %%%
create_first_run(List):-
	member(Agent,List),
	assert(time(0,0,Agent)),
	fail;true.

initialisation(experiment):-
	experiment_id,
	prepare_folder.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation - File Output %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% write tick results to file %%%
write_tick_results:-
	output_filename(Filename),
	open(Filename,write,Stream),
	write(Stream, "Test"),
	close(Stream).

% Check if results dir exists
prepare_folder:-
	checking_results_dir,
	create_experiment_dir.

% Create folder for result files
checking_results_dir:-
	exists_directory('./results');
	make_directory('./results').

% Create folder for running experiment
create_experiment_dir:-
	experiment_id(Experiment_id),
	concat('./results/',Experiment_id,ExDir),
	asserta(experiment_dir(ExDir)),
	make_directory(ExDir).


% Check if output file exists
%-> create file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation  start %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Simulation start %%%
:- style_check(-singleton).
:- message(welcome,_).

% creates an ID for a simulation run
experiment_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([experiment,Year,Month,Day,Hour,Min,Sec],"-",Experiment_id),
	retractall(experiment_id(_)),
	asserta(experiment_id(Experiment_id)).

run_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([run,Year,Month,Day,Hour,Min,Sec],"-",Run_ID),
	retractall(run_id(_)),
	asserta(run_id(Run_ID)).

% Clean up memory
cleanup:-
  remove_world,
  retractall(sim_time(_)),
  retractall(agent(_,_,_)),
  retractall(belief(_,_,_)),
  retractall(publication(_,_,_,_)),
  retractall(fact(_,_,_)),
  retractall(fact(_)).

% Multiple run
mult_run(Seed):-
	%set_random(seed(Seed)),
	initialisation(run),
	%set_random(seed(Seed)),
	number_of_ticks(NT),
	run(0,NT),
	write_results(alter_csv),
	%write_results(agent_statistic_age_csv),
	message(summary,_),
	cleanup.
 	%write_results(agent_statistic_age_csv),
	%write_results(belief), write_results(belief_statistic_csv),
  %dynamic([belief/3,agent/3]),

% Simulation start
start:-
	initialisation(experiment),
	message(experiment,_),
	foreach( between(1,10,Seed),
		     time(mult_run(Seed)) ), 
	message(border,_),
	halt.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Remove?  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:-  thread_pool_create(main, 8, []).
start_with_threads:-
	message(border,_),
	write('Experiment ID: '),
	experiment_id,
	experiment_id(Experiment_id),
	writeln(Experiment_id),
	between(1,10,Seed),
	thread_create_in_pool(main,(mult_run(Seed),message(border,_)), _ID, [stack_limit(2 000 000 000)] ),
	fail;true.


















% http_open('http://192.168.0.11:8086/write?db=mydb', In, ["simulation,run=ID,tick=1 agents=178"]),
%http_open([ host('www.example.com'), path('/my/path'), search([ q='Hello world', lang=en])]).     
%http_open([ host('http://192.168.0.11:8086/'), path('write?db=mydb'), search([ simulation,run=ID,tick=1 agents=5555])], method(post)), 
%'http://192.168.0.11:8086/write?db=mydb' --data-binary 'simulation,run=ID,tick=1 agents=5555'
%http_open([method(post),post("http://192.168.0.11:8086/write?db=mydb' --data-binary 'simulation,run=ID,tick=1 agents=123456789"]),
% format('~w = ', [a]).



%format(atom(Param_Number_Of_Agents),"curl -i -XPOST 'http://192.168.0.11:8086/write?db=mydb' --data-binary 'simulation,run=ID,tick=1 agents=~w'", [Number_Of_Agents]),
/*
format(atom(Param_Number_Of_Agents),'simulation,run=ID,tick=1 agents=~w', [Number_Of_Agents]),

http_post( [protocol(http),	
			host('192.168.0.11'), 
			port(8086), 
			path('/write?db=mydb')], 
			Param_Number_Of_Agents,
			 _R, []),

*/


%writeln(CURL),

/*
format(atom(CURL_Number_Of_Agents),"curl -i -XPOST 'http://192.168.0.11:8086/write?db=mydb' --data-binary 'simulation,run=ID,tick=1 agents=~w'", [Number_Of_Agents]),



[ protocol(http),
            host(Host),
            port(Port),
            path(ActionPath)
          ]

*/
%shell(CURL),