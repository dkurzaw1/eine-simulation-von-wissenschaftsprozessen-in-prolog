%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Utilitis %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(utils,[
  random_sublist/3,
  find_all_actions/1,
  find_all_agents/1,
  find_all_facts/1,
  startup_time/6,
  list_of_active_agents/1,
  cls/0 ]).

%%% Choose a random element from a given list %%%
% KANN GEGEN random_member(X, From) getauscht werden!!!
%random_element_from_list([], []).
%random_element_from_list(List,Random_Element):-
%	length(List, Length),
%	random(0, Length, Index),
%	nth0(Index, List, Random_Element).

random_sublist(0, _, []).
random_sublist(Count, List, [X|SelectedFromRemaining]) :-
  random_member(X, List),
  select(X, List, RemainingList),
  C1 is Count - 1,
  random_sublist(C1, RemainingList, SelectedFromRemaining).

startup_time(Year,Month,Day,Hour,Minute,Second):-
	get_time(X),
	stamp_date_time(X,TimeString,localc),
	TimeString =..[_,Year,Month,Day,Hour,Minute,Second,_,_,_].

find_all_actions(List_of_available_actions):-
	findall(Actions,action(Action,Agent,Tick),List_of_available_actions).

find_all_agents(List_of_available_agents):-
	findall(ID,agent(Type,ID,List_of_Attributes),List_of_available_agents).

find_all_facts(List_of_facts_in_World):-
	findall(ID,fact(ID),List_of_facts_in_World).

list_of_active_agents(ListOfActiveAgents):-
  findall(AgentID,(agent(Type,AgentID,Prop),not(IA=Prop.get(inactive))),ListOfActiveAgents).




% cls = clear screen for clean output
cls :- write('\e[H\e[2J').
