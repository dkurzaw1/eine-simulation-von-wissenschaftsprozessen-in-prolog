:-module(screen_output,[message/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Screen output predicats %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Screen output %%%

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge all or specific screen output
%message(_,_):-true.
%message(generic,_):-true.
message(apply_actions,_):- true.
message(tick,_):-true.
%%%%%%%%%%%%%%%%%%%%%%%%

message(border,_):-
  writeln('==================================================================').

  message(stars,_):-
    writeln('******************************************************************').

message(welcome,_):-
  message(stars,_),
  message(stars,_),
  version(Version),
  write('******          PROLOG SOCIAL SIMULATION SYSTEM  * '),
  write(Version),
  writeln('      ******'),
  message(stars,_),
  message(stars,_),
  message(border,_),
  message(generic,'Files loaded'),
  message(generic, '... use "?- start." '),
  message(border,_),
  writeln('').

number_of_agents(total, NumberOfAgents):-
  findall(agent(Type,AgentID,Prop),
     agent(Type,AgentID,Prop),List), 
  length(List,NumberOfAgents).

number_of_agents(inactive, InactiveAgents):-
  findall(Inactive,(agent(Type,AgentID,Prop),IA=Prop.get(inactive)),List),
  length(List,InactiveAgents).

number_of_agents(active, NumberActiveOfAgents):-
  findall(Inactive,(agent(Type,AgentID,Prop),not(IA=Prop.get(inactive))),List),
  length(List,NumberActiveOfAgents).

message(summary,_):-
  writeln('[SUMMARY]'),
  format('~tAgents ~t~72|~n~n'),
 % format('At start: ~`.t ~2f~34|  At end: ~`.t ~D~72|~n', [NumberOfAgentsAtStart, TotalNumberOfAgents]),
 % format('At start: ~`.t ~2d5|  At end: ~`.t ~d5|~n', [NumberOfAgentsAtStart, TotalNumberOfAgents]),


  number_of_agents_at_start(NumberOfAgentsAtStart),
	write('>> Total number of agents at start of run: '),
  writeln(NumberOfAgentsAtStart),

  number_of_agents(total,TotalNumberOfAgents),
	write('>> Total number of agents at end of run: '),
  writeln(TotalNumberOfAgents),

  number_of_agents(inactive, InactiveAgents),
  write('>> Number of inactive agents: '), 
  writeln(InactiveAgents),

  number_of_agents(active, NumberActiveOfAgents),
  write('>> Number of active agents: '), 
  writeln(NumberActiveOfAgents),
  format('~tBeliefs ~t~72|~n~n'),
  format('~t(...) ~t~72|~n~n'),

/*
  write('>> Number of facts: '), writeln(' ... not defined '),
  write('>> Number of publications: '),writeln(' ... not defined '),
  write('>> Number of predicates: '), writeln(' ... not defined '),
  write('>> Number of beliefs: '),writeln(' ... not defined '),*/
  message(border,_).

message(experiment,_):-
  message(border,_),
	write('Experiment ID: '),
	experiment_id(Experiment_id),
	writeln(Experiment_id).

message(finish,_):-
  message(border,_),
  run_id(RunID),
  write('[FINISHED] '),
  writeln(RunID).

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge info screen output
% message(_,_):-true.
%%%%%%%%%%%%%%%%%%%%%%%%

message(tick,Counter):-
  message(border,_),
  write('>>  Tick : '), writeln(Counter),
  message(border,_).

message(loop,Counter):-
  message(border,_),
  write('>>>>  Inner Loop : '), writeln(Counter),
  message(border,_).

message(apply_actions,_):-
  write('[ACTIONS] Apply action in Tick '),
  sim_time(Tick),
  writeln(Tick).

message(generic,Content):-
  write(">> "),
  writeln(Content).
