% Reasoning

% => Aufbauend auf Wissensbasis
% => Augba

/**
beobachtung(boden(sauer)).
fakt(sauer,objekt(sulfonsaeure)).
fakt(sauer,objekt(ascorbinsaeure)).
fakt(sauer,objekt(zitrone)).
objekt(Bezeichnung,Art).
beinhaltet(Objekt,Objekt).
beobachtung(fakt(zitrone)).
**/

% objekt(Bezeichnung,Eignschaft).


% fakt (Agent, AbTick, objekt ...)
% fakt (Agent, AbTick, eigenschaft ...)

% Wissen:
belief(agent1, objectclass(zitrone,property(colour,yellow))).
belief(agent1, objectclass(zitrone,property(phwert,sauer))).
belief(agent1, objectclass(zitrone,property(farbe,gruen))).
belief(agent1, objectclass(zitrone,property(form,oval))).

observation(agent1, object(temp1, property(fable,gelb))).

objekt(zitrone,eigenschaft(farbe,gelb)).
objekt(zitrone, eigenschaft(phwert, sauer)).

loopObjekt(N,N) :-!.
loopObjekt(N,Max):-
  N < Max,
  random(RandA),
  random(RandB),
  assertz(object(RandA,RandB)),
  N1 is N + 1,
  loopObjekt(N1,Max).


loopZitrone(N,N) :-!.
loopZitrone(N,Max):-
  N < Max,
  random_between(4,8,RandomGewicht),
  assertz(object(zitrone,eigenschaft(gewicht,RandomGewicht))),
  N1 is N + 1,
  loopZitrone(N1,Max).

entityDescription(zitrone,[physicalObject]).
entityDescription(physicalObject,[weight,shape,ph]).
entityDescription(weight,[keineAhnung]).
entityDescription(shape,[shape]).
entityDescription(ph,[ph]).


/**

Durch unterschiedliche Messmethoden können Eigenschaften erfasst werden.
Einige sind durch die einfachste Methode, dem betrachten erfassbar, andere
benötigen Apperaturen. Das Gewicht lässt sich über verschiedene Möglichkeiten,
angefangen beim einfachen Wiegen, erfassen. Auch sind verschiedene Skaalen
gegeben und dadurch andere Interpretationen von Maßeinheiten zur Gewichtsangabe.

**/

/**
Wie können neue Grundbegriffe, Eigenschaften und Messmethoden gebildet werden?
Laesst sich alles auf wenige grundbegriffe reduzieren?
Wie lassen sich diese Reduktionen erkennen?

**/


/**

Action: observation
(1) "Beobachtung" Agent beobachtet Objekt ... gibt Objekt einen temporären Namen.
(2) Checkt dann ob Eigenschaften mit bekannten Objekten übereinstimmen.
--> ggf. Haupteigenschaft pro Objekt? Wenn diese übereinstimmt, dann ist es Teil dieser Objektklasse?
(3a) Wenn ja, Objekt ist eine Instanz von bekannte Objekten
(3b) Wenn nein, neues Objekt


Beispiel:
Eine gelbe ovale Frucht wird an einem Baum beobachtet. Der Baum aus der Familie der Rautengewächse (Rutaceae) ist
typisch für dies Art von Frucht und zählt zu der Gatting der Zitruspflanzen (Citrus). Entsprechend ist das beobactete Objekt eine Zitrone.
Als Eigenschaten werden eine ovale gelbe oder bei anderen beobachtungen grüne Oberfläche, sowie ein säuerlicher Saft beobachtet.
Zitronen haben als Grundeigenschaft die Fruch von Zitronenbäumen zu sein.

Action: reasoning
**/

% Regel: Alle Objekte mit der Bezeichnung x tragen Eigenschaften x y z
% Fakten: Substanz =  Zitronensäure, Sulfonsäure, Lakmus,
% Fakt: Substanz ist Art von Objekt
% Fakt: Objekt trägt Eigenschaften
% Regel: Gleichheit wenn alle Eigenschaften gleich
% Regel: Neue Eigenschaft, wenn zwei Objekte gleiche Eigenschaft aber nicht alle anderen Eigenschaften gleich? (anderer Siedepunkt etc..)
% Beobachtung: Mischung von Zitronensäure und Lakmus = Rot
% Beobachtung: Mischung von Sulfonsäure und Lakmus = Rot
% Frage: Wieso die Reaktion?
% Antwort:
% (H1) Lakmus bei Mischung immer Rot
% (H2) Zitronensäure = Sulfonsäure?
% (H3) Zitronensäure und Sulfonsäure tragen weitere Eigenschaft, welche Lakmus färbt
