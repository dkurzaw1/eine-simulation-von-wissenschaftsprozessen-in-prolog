:-module(screen_output,[message/2, msg_with_simple_name/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Screen output predicats %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Screen output %%%

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge all or specific screen output
%message(_,_):-true.
%message(generic,_):-true.
message(apply_actions,_):- true.
%message(tick,_):-true.
%%%%%%%%%%%%%%%%%%%%%%%%

:- set_prolog_flag(verbose, silent).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Display status of agents with "simple name" %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

msg_with_simple_name(AgentID,MSG):-
	agent(T,AgentID,Dict),
	SimpleName = Dict.get(simplename),
	agent_age(AgentID,Age),
	Resources = Dict.get(resources),
	Que = Dict.get(action_que),
	length(Que,LenQ),
	format('Agent ~w (Age ~w, Res ~w, Que ~w)~`.t ~w~80|~n', [SimpleName,Age,Resources, LenQ, MSG]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Statistics of simulation run %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


message(runstate,_):-
  format('~tEXPERIMENT OVERVIEW ~t~72|~n~n'),
  
  format('RUN-ID ~`.t~73| RESULT  ~n'),

  foreach(
    finish_state(RunID,State),
    format('>> ~`.w  ~`.t ~`.a~77| <<~n',[RunID,State])).


%	format('Agent ~w (Age ~w, Res ~w, Que ~w)~`.t ~w~80|~n', [SimpleName,Age,Resources, LenQ, MSG]).



  %findall([RunID,State],finish_state(RunID,State),StateListe),
  %format('>> Run ID: ~`.t ~D~34|  State: ~`.t ~D ~71|<<~n',[RunID,State])
	

message(border,_):-
  format('~`=t ~74|~n',[]).

message(stars,_):-
  format('~`*t ~74|~n',[]).

message(welcome,_):-
  format('~n',[]),
  message(stars,_),
  version(Version),
  format('~`*t~18|  PROLOG SOCIAL SIMULATION SYSTEM : ~w ~`*t  ~75|~n',[Version]),
 % write('******          PROLOG SOCIAL SIMULATION SYSTEM  * '),
  %writeln('             ******'),
  message(stars,_),
  message(border,_),
  message(generic,'                         All files loaded                           <<'),
  message(generic,'          Please type "start." to start the simulation              <<'),
  message(generic,'             and "doc_browser." for additional help.                <<'),
  message(generic,'                                                                    <<'),
  message(generic,' A complete code documentation and scientific background of the     <<'),
  message(generic,' simulation experiments and underlying theory can be found in:      <<'),
  message(generic,' Daniel Kurzawe: Eine Simulation von Wissenschaftsgemeinschaften    <<'), 
  message(generic,'                  (tbp) (english version in progress)               <<'),

  message(border,_),
  cores(NumberOfCores),
  statistics(threads,Threads),
  format('>> CPU-Cores detected: ~`.t ~D~34|  Active threads at startup: ~`.t ~D ~71|<<~n',
                                          [NumberOfCores,Threads]),
  message(border,_),
  writeln('').

number_of_agents(total, NumberOfAgents):-
  findall(agent(Type,AgentID,Prop),
     agent(Type,AgentID,Prop),List),
  length(List,NumberOfAgents).

number_of_agents(inactive, InactiveAgents):-
  findall(Inactive,(agent(Type,AgentID,Prop),IA=Prop.get(inactive)),List),
  length(List,InactiveAgents).

number_of_agents(active, NumberActiveOfAgents):-
  findall(Inactive,(agent(Type,AgentID,Prop),not(IA=Prop.get(inactive))),List),
  length(List,NumberActiveOfAgents).

number_of_beliefs(Number):- Number = 0.
  /*
  agents:belief(_, _,_, _, _,_)->
  (findall(belief(AgentID, Tick,ContentType, DataID, Domain, Strength),agents:belief(AgentID, Tick,ContentType, DataID, Domain, Strength),L),
  length(L,Number)); Number = 0.*/

number_of_events(total, Events):-
  findall(event(A,B,C,Location),create_world_module:event(A,B,C,Location),L),
  length(L,Events).

number_of_events(start, Events):- 
  findall(event(A,B,C,Location),create_world_module:event(A,B,0,Location),L),
  length(L,Events).

number_of_events(end, Events):-
  sim_time(Tick),
  findall(event(A,B,C,Location),create_world_module:event(A,B,Tick,Location),L),
  length(L,Events).

number_of_event_types(total, Types):- 
  findall(eventtype(A,B),create_world_module:eventtype(A,B),L),
  length(L,Types).

number_of_laws(total,Laws):-
  findall(LawID, create_world_module:law_pre(LawID,_Precondition) ,L),
  length(L,Laws).

message(summary,_):-

  %%%%%%%%%%%%%%%%%%%%%%%%
  % Collect data
  %%%%%%%%%%%%%%%%%%%%%%%%

  sim_time(Tick),
  number_of_agents(active, NumberActiveOfAgents),
  number_of_agents_at_start(NumberOfAgentsAtStart),
  number_of_agents(total,TotalNumberOfAgents),
  number_of_agents(inactive, InactiveAgents),
  number_of_beliefs(NumberActiveBeliefs),

  number_of_events(total, Events_total),
  number_of_events(start, Events_start),
  number_of_events(end, Events_end),
  number_of_event_types(total, Event_types),
  number_of_laws(total,Laws),

  %%%%%%%%%%%%%%%%%%%%%%%%
  % Convert type
  %%%%%%%%%%%%%%%%%%%%%%%%

  statistics(predicates,Predicates), 
  statistics(atoms,Atoms),
  statistics(modules,Modules),


  %%%%%%%%%%%%%%%%%%%%%%%%
  % Category: Agents
  %%%%%%%%%%%%%%%%%%%%%%%%

  format('~n~tAgents ~t~72|~n~n'),
  format('Total number (start): ~`.t ~D~34| Total number (end): ~`.t ~D~72|~n',
                                          [NumberOfAgentsAtStart,TotalNumberOfAgents]),
  format('Inactive (end): ~`.t ~D~34|  Active (end): ~`.t ~D~72|~n',
                                          [InactiveAgents,NumberActiveOfAgents]),

  %%%%%%%%%%%%%%%%%%%%%%%%
  % Category: Beliefs
  %%%%%%%%%%%%%%%%%%%%%%%%

  format('~n~tBeliefs ~t~72|~n~n'),
  format('Total number (start): ~`.t ~D~34|  Total number (end): ~`.t ~D~72|~n',
                                          [00000,NumberActiveBeliefs]),
  %%%%%%%%%%%%%%%%%%%%%%%%
  % Category: Statistics
  %%%%%%%%%%%%%%%%%%%%%%%%

  format('~n~tStatistics ~t~72|~n~n'),
  format('Ticks : ~`.t ~D~34|  Modules: ~`.t ~D~72|~n',
                                          [Tick,Modules]),
  format('Atoms : ~`.t ~D~34|  Predicates: ~`.t ~D~72|~n',
                                          [Atoms,Predicates]),
  %%%%%%%%%%%%%%%%%%%%%%%%
  % Category: World
  %%%%%%%%%%%%%%%%%%%%%%%%

  format('~n~tWorld ~t~72|~n~n'),
  format('Facts : ~`.t ~D~34|  Relations: ~`.t ~D~72|~n',
                                          [00000,00000]),
  format('Rules : ~`.t ~D~34|  Complexity: ~`.t ~D~72|~n',
                                          [00000,00000]),
  format('Events (total) : ~`.t ~D~34|  Event types (total): ~`.t ~D~72|~n',
                                          [Events_total,Event_types]),
  format('Events (start) : ~`.t ~D~34|  Events (end): ~`.t ~D~72|~n',
                                          [Events_start,Events_end]),
  format('Laws (total) : ~`.t ~D~34|  some (text): ~`.t ~D~72|~n',
                                          [Laws,00000]),
  %%%%%%%%%%%%%%%%%%%%%%%%
  % Category: Publications
  %%%%%%%%%%%%%%%%%%%%%%%%

  format('~n~tPublications ~t~72|~n~n'),
  format('Total number (start) : ~`.t ~D~34|  Total number (end): ~`.t ~D~72|~n',
                                          [00000,00000]),
  format('~n~n~t(...) ~t~72|~n~n').




message(experiment,_):-
  message(border,_),
	write('Experiment ID: '),
	experiment_id(Experiment_id),
	writeln(Experiment_id).

message(finish,_):-
  message(border,_),
  experiment_id(Experiment_id),
  run_id(RunID),
  format('~tRUN FINISHED ~t~72|~n~n'),

/*
  format('EID: ~`.t ~w~34|  RID: ~`.t ~w~72|~n',
                                          [RunID,Experiment_id]),

                                          */
  format('~t ~w ~t~72|~n~n',[Experiment_id]),
  format('~t ~w ~t~72|~n~n',[RunID]).

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge info screen output
% message(_,_):-true.
%%%%%%%%%%%%%%%%%%%%%%%%

message(tick,Counter):-
  message(border,_),
  write('>>  Tick : '), writeln(Counter),
  message(border,_).

message(loop,Counter):-
  message(border,_),
  write('>>>>  Inner Loop : '), writeln(Counter),
  message(border,_).

message(apply_actions,_):-
  write('[ACTIONS] Apply action in Tick '),
  sim_time(Tick),
  writeln(Tick).

message(generic,Content):-
  write(">> "),
  writeln(Content).
