:- module(file_output,[write_results/1,init_results/1,
  write_diary/2, export_laws/0]).
:- use_module(library(csv)).
:-dynamic([agents_in_tick/3,outputSteam/1,bel_in_tick/2]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agents diary %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
export_laws:-
  export_event_types,
  export_law_pre,
  export_law_target.

export_event_types:- 
  switch_dir(run),
  File = 'world.pl',
  open(File, append, Stream),
  with_output_to(Stream, 
    listing(create_world_module:eventtype/2)),
  close(Stream),
  switch_dir(simulation).

export_law_pre:-
  switch_dir(run),
  File = 'world.pl',
  open(File, append, Stream),
  with_output_to(Stream, 
    listing(create_world_module:law_pre/2)),
  close(Stream),
  switch_dir(simulation).

export_law_target:-
  switch_dir(run),
  File = 'world.pl',
  open(File, append, Stream),
  with_output_to(Stream, 
    listing(create_world_module:law_target/2)),
  close(Stream),
  switch_dir(simulation).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agents diary %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(overview):-
  switch_dir(experiment),
  File = 'overview.csv',
  open(File, append, Stream), 
  csv_write_stream(Stream,[row('RunID', 'State')], [] ),
  foreach(
    finish_state(RunID,State),
    csv_write_stream(Stream,
    	[row(RunID, State)],[]) ),
  close(Stream),
  switch_dir(simulation).
  

write_diary(AgentID, Content):-
  %%% INIT %%%
  switch_dir(diary),
  init_diary(AgentID, File), 
  run_id(RunID),
  sim_time(Tick),
  %%% GET DATA %%%
  agent(AgentType,AgentID,Dict),
  length( Dict.get(list_of_interests), Interests ),
  length( Dict.get(action_que), QueLen),

  findall(actions:belief(_, _, _, _,_, _),
  actions:belief(AgentID, _, _, _,_, _),BelList),
  length(BelList,BelLen),

  findall(DataID,actions:data(DataID, AgentID, _, _, _, _, _),DataList),
  length(DataList,DataLen),

  findall(Theory, actions:theory(AgentID, _, _,_,_, _), TheoryList),
  length(TheoryList, TheoryLen),
  findall(PublicationID,( 
      actions:publication(PublicationID, _, ListOfAuthors,  _, _,_),
      member(AgentID, ListOfAuthors)),Publist),
  length(Publist,PubLen),   
  %%% WRITE %%%
  diary_file_name(AgentID, File),
  open(File, append, Stream), 
  format(atom(Reputation),'~2f',[Dict.get(reputation)]),
  csv_write_stream(Stream, 
    [ row(RunID,AgentID,AgentType,Tick,
          Dict.get(created),Dict.get(resources),Reputation,
          Dict.get(ability),QueLen,Content,DataLen,BelLen,
          TheoryLen,PubLen,Interests) ],[] ), 
  %%% CLEAN UP %%%
  close(Stream),
  switch_dir(simulation).

init_diary(AgentID, File):-
  diary_file_name(AgentID, File),
  ( exists_file(File) ;
    create_diary_file(File)
  ).

create_diary_file(File):-
  open(File, append, Stream), 
  csv_write_stream(Stream,[row('RunID', 'AgentID', 'AgentType','Tick', 
    'Created', 'Ressources', 'Reputation', 'Ability', 'Que', 'Action','Data',
    'Belief','Theory', 'Publication', 'Interests')], [] ),
  close(Stream).

diary_file_name(AgentID, File):-
  agent(Type,AgentID,Dict),
  SimpleName = Dict.get(simplename),
  atom_concat(SimpleName, ".csv", File).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Theories %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_results(theories):-
  switch_dir(run),
  open('theories.csv', append, Stream), 
  findall(actions:theory(AID,TP,TID,Link,PotModID, ModID), 
    actions:theory(AID,TP,TID,Link,PotModID, ModID), 
    ListOfTheories),
  forall(member(Theory,ListOfTheories), write_theory(Theory,Stream)),
  close(Stream),
  switch_dir(simulation).

write_theory(Theory,Stream):-
  writeln(Stream,Theory),
  actions:theory(AID,TP,TID,Link,PotModID, ModID)->
  ( actions:pot_model(PotModID,BaseType, Types), writeln(Stream, 
    actions:pot_model(PotModID,BaseType, Types)),
  actions:model(ModID, PotModID, Relations), 
  writeln(Stream, actions:model(ModID, PotModID, Relations))) 
  ; true.

write_results(theory_net):-
  theory_net(PotModList),
  switch_dir(run),
  open('nets.csv', append, Stream), 
  forall(member([TID1,PotModel,TID2],PotModList),
    csv_write_stream(Stream,[row(TID1,PotModel,TID2)], [] )),
  close(Stream),
  switch_dir(simulation).

theory_net(PotModList):-
  findall([TID1,PotModel,TID2],(theory(Agent1,_,TID1,_,PotModel,Model1), 
  theory(Agent2,_,TID2,_,PotModel,Model2),
  TID1\=TID2,Model1\=Model2 ), PotModList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Tick results %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


init_results(between_ticks):-
  switch_dir(run),
  open('tick_stats.csv', append, Stream), 
  csv_write_stream(Stream,[row('RunID', 'Tick','AllAgents', 'Active',
     'Inactive','Theories','DataSets','AllPublications','OpenPublications',
     'ClosedPublications','HIJournal', 'HIRejected','MIJournal', 'MiRejected',
     'LIJournal', 'LowRejected','AllCitations','CitationsLow','CitationsMid',
     'CitationsHigh')], [] ),
  close(Stream),
  switch_dir(simulation).

find_citations(NBCitedPubs,NBlow,NBmid,NBhigh):-
  findall(Publication, 
    actions:citation(Agent,ReferenceAgent,Publication), CitedPubs),
  length(CitedPubs,NBCitedPubs),
  number_citations(low_impact,CitedPubs,NBlow),
  number_citations(mid_impact,CitedPubs,NBmid),
  number_citations(high_impact,CitedPubs,NBhigh).

number_citations(Journal,CitedPubs,NB):-
  findall(Journal, (member(Pub, CitedPubs),
  actions:publication(Pub,Journal, Authors, Tick, Type,Content)),Pubs),
  length(Pubs,NB).

write_results(between_ticks):-
  switch_dir(run),
  run_id(RunID),
  sim_time(Tick),
  open('tick_stats.csv', append, Stream), 
  find_all_agents(List_of_available_agents),
  length(List_of_available_agents,AllAgents),
  list_of_active_agents(ListOfActiveAgents), 
  length(ListOfActiveAgents, Active),
  list_of_inactive_agents(ListOfInactiveAgents),
  length(ListOfInactiveAgents, Inactive),
  findall(Theory,actions:theory(Theory,_,_,_,_,_),LOT),
  length(LOT,LenLOT),
  findall(DataID,actions:data(DataID,AgentID,_,_, _, _,_),Data),
  length(Data,LenData),
  find_citations(NBCitedPubs,NBlow,NBmid,NBhigh),
  findall(PublicationID,
  actions:publication(PublicationID,Journal,AuthorList,_,_,Content),Pubs),
  length(Pubs,LenPubs),
  findall(PublicationID,
  actions:publication(PublicationID,low_impact,AuthorList,_,_,Content),LowPubs),
  length(LowPubs,LenLowPubs), 
  findall(actions:publication_rejected(_, _, _, _, low_impact),
          actions:publication_rejected(_, _, _, _, low_impact), LowRej),
  length(LowRej, LenLowRej),
  findall(PublicationID,
  actions:publication(PublicationID,mid_impact,AuthorList,_,_,Content),MidPubs),
  length(MidPubs,LenMidPubs), 
  findall(actions:publication_rejected(_, _, _, _, mid_impact),
          actions:publication_rejected(_, _, _, _, mid_impact), MidRej),
  length(MidRej, LenMidRej),
  findall(PublicationID,
  actions:publication(PublicationID,high_impact,
    AuthorList,_,_,Content),HighPubs),
  length(HighPubs,LenHighPubs), 
  findall(actions:publication_rejected(_, _, _, _, high_impact),
          actions:publication_rejected(_, _, _, _, high_impact), HighRej),
  length(HighRej, LenHighRej),
  csv_write_stream(Stream,[row(RunID, Tick,AllAgents, Active, Inactive,LenLOT,
    LenData,LenPubs,OpenPubs,ClosedPubs,LenHighPubs,LenHighRej, LenMidPubs, 
    LenMidRej, LenLowPubs, LenLowRej, NBCitedPubs,NBlow,NBmid,NBhigh)], [] ),
  close(Stream),
  switch_dir(simulation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Results form simulation log %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(log):-
  switch_dir(run),
  open('log.csv', append, Stream), 
    forall(utils:log(RunID, Tick, AgentID, Type, Content, Value),
            csv_write_stream(Stream,[row(RunID, Tick, AgentID, 
              Type, Content, Value)], [] )),
  close(Stream),
  switch_dir(simulation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Results form simulation log %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(snapshot_events):-
  switch_dir(run),
  open('snap-events.csv', append, Stream), 
  forall(utils:events_in_range(ID1,List,Tick),
            csv_write_stream(Stream,[row(ID1, _List, Tick)], [] )),
  close(Stream),
  switch_dir(simulation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Events per tick %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(eventspertick):-
  switch_dir(run),
  open('eventspertick.csv', append, Stream), 
  run_id(RunID),
  sim_time(Tick),
    ( between(0,Tick,C), 
      number_of_event_per_tick(C,NumberOfEvents), 
      csv_write_stream(Stream,[row(RunID, C, 
        NumberOfEvents)], [] ), fail;true ),
  close(Stream),
  switch_dir(simulation).

number_of_event_per_tick(Tick,NumberOfEvents):-
  findall(ID, create_world_module:event(ID, Type, Tick, [X,Y]), ListOfEvents), 
  length(ListOfEvents,NumberOfEvents).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run results %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(alter_csv):-
  switch_dir(run),
  run_id(RunID),
  open("AgentsAge.csv",write,Stream),
  csv_write_stream(Stream, [ row('RunID', 'Agent', 'Age', 'Created', 
    'Inactive', 'Resources', 'Reputation', 'Ability', 'Que') ], [] ),
  assert(outputSteam(Stream)),
  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
    Agent =.. [agent,Type,AgentID,Attributes],
    Age is Attributes.get(inactive) - Attributes.get(created),
    length( Attributes.get(action_que), QueLen),
    csv_write_stream(Stream, 
      [ row(RunID,AgentID, Age, 
        Attributes.get(created), 
        Attributes.get(inactive),  
        Attributes.get(resources),
        Attributes.get(reputation), 
        Attributes.get(ability), 
        QueLen) ], 
        [] ),
    fail;true),
  close(Stream),
  switch_dir(simulation).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Specific events per tick %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(agent_statistic_age_csv):-
  % Change working directory into run folder
  run_id(Run_ID),
  switch_dir(run),

  sim_time(Tick),
  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
      Agent =.. [agent,Type,AgentID,Attributes],

      (( InAct = Attributes.get(inactive),
        ( between(Attributes.get(created),Attributes.get(inactive),ActiveTick),
          asserta( agents_in_tick(ActiveTick,AgentID,Type) ),
          fail;true)
        );
      ( not(InAct = Attributes.get(inactive)),
        ( between(Attributes.get(created),Tick,ActiveTick),
          asserta( agents_in_tick(ActiveTick,AgentID,Type) ),
          fail;true)
        )),

    fail;true
    ),
    run_id(RunID),

    open("AgentsPerTick.csv",write,Stream),
    csv_write_stream(Stream, [ row('RunID', 'Tick', 
      'ActiveAgentsPerTick') ], [] ),
    assert(outputSteam(Stream)),
    (between(0,Tick,Counter),
      findall(_,agents_in_tick(Counter,AID,Type),AgentsPerTickList),
      length(AgentsPerTickList,AgentsPerTick),
      csv_write_stream(Stream, [ row(RunID, Counter, AgentsPerTick) ], [] ),

     % write('Tick '),writeln(Counter),
     % write('Agents '),writeln(AgentsPerTick),
    fail;true),
    close(Stream),
    retractall(agents_in_tick(_,_,_)),
    switch_dir(simulation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(belief_statistic_csv):-
  % Change working directory into run folder
  simulation:working_directory(CWD, CWD),
  run_id(Run_ID),
  concat(CWD,Run_ID,NewCWD),
  simulation:working_directory(_, NewCWD),

  sim_time(Tick),
  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
      Agent =.. [agent,Type,AgentID,Attributes],
      findall(BT,belief(AgentID, BT,ContentType, DataID, Domain, Strength),
              BelList),
        (member(BelTick,BelList),
        ( between(BelTick,Attributes.get(inactive),BelActTick),
          asserta( bel_in_tick(BelActTick,AgentID) ),
          fail;true),
        fail;true),

    fail;true
    ),
    open("beliefPerTick.csv",write,Stream),
    csv_write_stream(Stream, [ row('Tick', 'BeliefsPerTick') ], [] ),
    assert(outputSteam(Stream)),
    (between(0,Tick,Counter),
      findall(_,bel_in_tick(Counter,BelAgentID),BelivePerTickList),
      length(BelivePerTickList,BelPerTick),
      csv_write_stream(Stream, [ row(Counter, BelPerTick) ], [] ),
    fail;true),
    close(Stream),
  %retractall(bel_in_tick/2),
    simulation:working_directory(_,CWD).

write_results(belief):-
  output_filename(Filename),
	open(Filename,write,Stream),
  writeln('[SAVE] Write terms to file'),
  with_output_to(Stream, listing(agents:belief)),
	write(Stream, "eof"),
	close(Stream),
  writeln('[OK] terms written!').

write_results(csv):-
  message(border,_),
  writeln('[SAVE] Write terms to csv file'),
	open("csv_Agent_Facts.csv",write,Stream),
  csv_write_stream(Stream, [ row('Tick', 'AgentID' ,'Fact') ], [] ),
  forall(belief(AgentID, Tick,ContentType, DataID, Domain, Strength),
         csv_write_stream(Stream,
                        [row(Tick, AgentID ,Fact)],
                        [] ) ),
	close(Stream),
  write_gephi_files.


write_gephi_files:-
  message(border,_),
  writeln('[SAVE] Write gephi output'),
	open("csv_Agent_Facts_G.csv",write,Stream),
  csv_write_stream(Stream, [ row('Tick', 'Source' ,'Target') ], [] ),
  forall(belief(AgentID,Tick,Fact),
         csv_write_stream(Stream,
                        [row(Tick, AgentID ,Fact)],
                        [] ) ),
	close(Stream),
  open("csv_Agent_Names.csv",write,Stream2),
  csv_write_stream(Stream2, [ row('AgentID') ], [] ),
  findall(AgentIDs,
    belief(AgentIDs, Tick,ContentType, DataID, Domain, Strength),Agents),
  sort(Agents,AgentsSort),

  forall(member(A, AgentsSort),
          csv_write_stream(Stream2, [row(A)],[] )
         ),
	close(Stream2),
  open("csv_Fact_Names.csv",write,Stream3),
  csv_write_stream(Stream3, [ row('FactID') ], [] ),
  findall(FactIDs,
    belief(AgentID, Tick,ContentType, FactIDs, Domain, Strength),Facts),
  sort(Facts,FactsSort),
  forall(member(F, FactsSort),
          csv_write_stream(Stream3, [row(F)],[] )
         ),
	close(Stream3),
  writeln('[OK] csv written!'),
  message(border,_).

