:- module(action_definition,
	[execute_action/3]).
:-multifile([execute_action/3]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% 					ACTION DEFINITION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Plan Action
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(plan_actions,AgentID,_Options):-
	msg_with_simple_name(AgentID,'action: plan_actions'),
	plan_action(AgentID).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: read publication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%execute_action(read_publication,AgentID,_Options):- true.

execute_action(read_publication,AgentID,_Options):- 
	get_interest(AgentID,Interest),
	% was machen, wenn es keine passenden Publikationen gibt?
	find_related_publications(Interest,PublicationsAuthors), 
	PublicationsAuthors \= [] ->
	( select_publication(PublicationsAuthors, Publication),
	  update_belief_with_publication(publication, AgentID, Publication) );
	no_publication(AgentID, Interest).

no_publication(AgentID, Interest):-
	write_diary(AgentID, "no publication in this area - new research topic!"),
	update_belief_with_publication(research_topic, AgentID,Interest).
	
update_belief_with_publication(publication, AgentID, Content):-
	% gibts das schon? dann bel stärken?
	% neu? Dann bel erzeugen
	sim_time(Tick),
	asserta(actions:belief(AgentID, Tick, theory, Content, Domain, 0.5)).

update_belief_with_publication(research_topic, AgentID, Content):-
	% gibts das schon? dann bel stärken?
	% neu? Dann bel erzeugen
	sim_time(Tick),
	asserta(actions:belief(AgentID, Tick, research_topic, Content, Domain, 0.5)).

find_related_publications(Interest,PublicationsAuthors):-
% unfold theory and find compatible to interests
	findall([PublicationID, AuthorList], 
			(actions:publication(PublicationID,Journal,AuthorList,Tick,theory,Content),
			types_in_theory(Content,Interest)),
		PublicationsAuthors).
		
types_in_theory(Theory,Type):-
	unfold_theory(Theory, UnfoldedTheory),
	UnfoldedTheory = [BaseType, Types, Relation], 
	Type = BaseType;
	member(Type, Types).

select_publication(PublicationsAuthors, Publication):-
	% gibt es eine Publikation von einem besonders gutem Wissenschaftler?
	% finde alle Autoren mit PubID
	% wähle beste Publikation
	find_reputation(PublicationsAuthors, ReputationList),
	select_best_publication(ReputationList, Publication).

find_reputation(PublicationsAuthors, SortReputation):-
	findall( [PubID, AutID, Reputation],
	( member(PubAut, PublicationsAuthors), 
	find_reputation_from_list(PubAut, Reputation) ), 
	List),
	sort(Reputation, SortReputation).

select_best_publication(ReputationList, Publication):-
	sort(ReputationList, SortReputationList),
	last(SortReputationList, Pub),
	Pub = [Rep, AutID,Publication].

find_reputation_from_list(PubAut, Reputation):-
	PubAut = [PubID, AutList],
	findall([Rep, AutID,PubID], 
		( member(AutID, AutList), agent_reputation(AutID, Rep) ), 
		Reputation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Create Theory (Type 1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(create_theory_type1, AgentID, _Options):-
	msg_with_simple_name(AgentID,'action: create_theory_type1'),
	agent(AgentType, AgentID, _Dict), 
	get_interest(AgentID,BaseType),

	get_observed_events(AgentID, BaseType, DataObjects),
	(	fitting_data(AgentID, BaseType, DataObjects)-> 
			msg_with_simple_name(AgentID,'create_theory_type1: ... new Theory'); 
		no_fitting_data(AgentID, BaseType, DataObjects) ).

no_fitting_data(AgentID, BaseType, DataObjects):-
	msg_with_simple_name(AgentID,'create_theory_type1: ... no fitting data!'),
	plan(select_new_interest, AgentID),
	plan(observation_types, AgentID).

fitting_data(AgentID, BaseType, DataObjects):-
	DataObjects \= [],
	determine_types_from_eventids(DataObjects, Types),
	multi_intersection(Types, CommonOccourence), % potential model
	relative_neighborevents_type_location(DataObjects, TypeLocationList),
	multi_intersection(TypeLocationList, CommonTypes), % RelationModel
	% Number of occurences of unique Types in TypeLocList
	distribution_of_elements(TypeLocationList, CommonRelations), 
	select_most_likely_candidate(CommonRelations, Relations),
	create_theory(AgentID, t1, BaseType,Types, Relations, Theory),
	sim_time(Tick),
	assertz(actions:belief(AgentID, Tick, theory, Theory, Domain, 0.5)).

create_theory(AgentID, TType, BaseType,Types, Relations, TID):-
	id('POTMOD',PotModID),
	id('MOD',ModID),
	id('THEORY',TID),
	asserta(actions:model(ModID, PotModID, Relations)),
	asserta(actions:pot_model(PotModID,BaseType, Types)),
	asserta(actions:theory(AgentID, TType, TID, Link, PotModID, ModID)).

get_observed_events(AgentID, BaseType, DataObjects):-
	findall(
		[BaseType, BaseLocation, DataContent],
		actions:data(DataID, AgentID, _Tick, _DataType, BaseType, BaseLocation, DataContent), 
		DataObjects).

determine_types_from_eventids(DataObjects, Types):-
	findall(TypesList, 
		( member([_BaseType, _BaseLocation, DataContent], DataObjects), 
		  find_types(DataContent,TypesList)), 
		Types).

relative_neighborevents_type_location(DataObjects, TypeLocationList):-
	findall([Type,RelLocation], 
		(	member([_BaseType, BaseLocation, EventList],DataObjects),
			member(Event, EventList),
			create_world_module:event(Event, Type, _, Location), 
			absolut_to_relativ_location(Location, BaseLocation, RelLocation)),
		TypeLocationList).

select_most_likely_candidate(CommonRelations, HighestValue):-
	sort(CommonRelations, CommonRelationsSort), 
	last(CommonRelationsSort, HighestValue).

unfold_theory(TheoryID, UnfoldedTheory):-
	actions:theory(_AID,_Type,TheoryID, _Link, PotModID, ModID),
	actions:model(ModID, PotModID, Relation),
	actions:pot_model(PotModID,BaseType, Types),
	UnfoldedTheory = [BaseType, Types, Relation].
/*
relative_neighborevents_type_location(EventID, TypeLocationList):-
	create_world_module:event(EventID, _, _, Location),  
	neighborevents(EventID,Neighborevents), 
	findall([NBType,RelLocation], 
		( member(Neighbor,Neighborevents),
			create_world_module:event(Neighbor, NBType, _, NeighborLocation), 
			absolut_to_relativ_location(NeighborLocation, Location, RelLocation)),
		TypeLocationList).
*/

find_types(EventIDs, EventTypes):-
	findall(Type, (member(Event, EventIDs), create_world_module:event(Event, Type,_,_)), EventTypes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Theory Type 2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(create_theory_type2, AgentID, _Options):-
	msg_with_simple_name(AgentID,'action: create_theory_type2'),
	(find_independent_data_series(AgentID,PotentialSeriesList), 
	select_base_type(PotentialSeriesList,BaseType),
	filter_series(PotentialSeriesList,BaseType,SeriesList),
	find_commons_in_t1(SeriesList,ComCommonsT1),
	find_commons_in_t2(SeriesList,ComCommonsT2),
	write_theory(AgentID, ComCommonsT1, ComCommonsT2));
	msg_with_simple_name(AgentID,'action: no theory t2 found').

write_theory(AgentID, ComCommonsT1, ComCommonsT2):-
	findall(TypeT1, member([TypeT1,_],ComCommonsT1), TypesT1),
	findall(TypeT2, member([TypeT2,_],ComCommonsT2), TypesT2),
	append(Types1,Types2,Types),
	create_theory(AgentID, type2, BaseType,Types, [ComCommonsT1,ComCommonsT2], TID).

find_commons_in_t1(SeriesList, CommonsT1):-
	filter_t1(SeriesList,T1List),
	extract_type_location(T1List, TypeLocationList),
	multi_intersection(TypeLocationList,CommonsT1).

find_commons_in_t2(SeriesList, CommonsT2):-
	filter_t2(SeriesList,T2List),
	extract_type_location(T2List, TypeLocationList),
	multi_intersection(TypeLocationList,CommonsT2).

filter_t1(SeriesList,T1List):-
	findall(T1, member([_P,[T1,_T2]],SeriesList), T1List).

filter_t2(SeriesList,T2List):-
	findall(T2, member([_P,[_T1,T2]],SeriesList), T2List).

extract_type_location(TList, TypeLocationList):-
	findall(TypeLocation, 
		(	member(DID,TList), 
			data(DID,_AID,_Tick,observation_data,_BaseType,Location,Observation), 
			actions:find_matching_typeslocations(Observation,Location,TypeLocation)),TypeLocationList).


select_base_type(SeriesList,SelectedBaseType):-
	find_independent_data_series(AgentID,SeriesList), 
	findall(BaseType,member([BaseType,_],SeriesList),Types),
	distribution_of_elements(Types,Dist),
	sort(Dist,Sort),
	last(Sort,[_,_,SelectedBaseType]).

filter_series(PotentialSeriesList,BaseType,SeriesList):-
	findall([BaseType,Content],member([BaseType,Content],PotentialSeriesList),SeriesList).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Publish 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(publish, AgentID, _Options):- 
    msg_with_simple_name(AgentID,'action: publish'),
	select_bel_to_publish(AgentID, Theory)*->
		(	review_draft_pub(AgentID, Theory, Result),
			get_observed_events(AgentID, BaseType, DataObjects),
			exec_publish([AgentID], theory, Theory, Result)
		);
	msg_with_simple_name(AgentID,'action: no fitting content to publish').

exec_publish([AgentID], theory, Theory, accepted):-
	publish([AgentID], theory, Theory),
	publish([AgentID], data, DataObjects).

exec_publish([AgentID], theory, Theory, rejected):-
    msg_with_simple_name(AgentID,'action: rejected').

select_bel_to_publish(AgentID, Theory):-
    findall(
      actions:belief(AgentID, Tick, theory, Theory, Domain, Str),
      (actions:belief(AgentID, Tick, theory, Theory, Domain, Str),Str>0.4),
      BelList),  
    random_member(actions:belief(_, _, theory, Theory, _, _), BelList).

review_draft_pub(AgentID, Theory, Result):-
	agents:agent(T,AgentID, Dict),
	AuthorDomain = Dict.get(research_domain),
	select_pub_reviewer(AgentID, AuthorDomain, Reviewer)*->
	conduct_pub_review(non-blind, Reviewer, AgentID, Theory, Result);
	(random_reviewer(Reviewer),conduct_pub_review(non-blind, Reviewer, AgentID, Theory, Result)).

conduct_pub_review(non-blind, ReviewerID, AgentID, Theory, Result):-
	agents:agent(_,AgentID, Dict),
	agents:agent(_,ReviewerID, RevDict),
	AuthorReputation = Dict.get(reputation),
	AuthorAbility = Dict.get(ability),
	Threshold = 1.5,
	check_draft_soundness(Theory, ReviewerID, Soundness),
	Review is (AuthorReputation + AuthorAbility) + Soundness,
	pub_review_result(AgentID, Threshold, Review, Result).

pub_review_result(AgentID, Threshold, Review, accepted):- 
	Review > Threshold,
	write_diary(AgentID, "Publication accepted"),
	msg_with_simple_name(AgentID,'action: publication accepted').

pub_review_result(AgentID, Threshold, Review, rejected):-
	Review =< Threshold,
	write_diary(AgentID, "Publication rejected"),
	msg_with_simple_name(AgentID,'action: publication rejected').


check_draft_soundness(Theory, ReviewerID, Soundness):-
	unfold_theory(Theory, [Topic|Model]),
	findall( DataID, 
			( actions:belief(ReviewerID, Tick, observation_data, DataID, Domain, Str),
			  actions:data(DataID,ReviewerID,_Tick,observation_data,Topic,_Location,_Types)
			),
		DataIDs),
		length(DataIDs, Soundness).

select_pub_reviewer(AgentID, AuthorDomain, Reviewer):-
	findall(RevID, 
			( agent(researcher, RevID, Dict), 
			RevDomain = Dict.get(research_domain), 
			RevID\=AgentID,
			intersection(RevDomain,AuthorDomain,Intersection),
			Intersection\=[]
			),
		ListOfPossibleReviewer ),
	random_member(Reviewer,ListOfPossibleReviewer).

random_reviewer(Reviewer):-
	list_of_active_agents(Agents),
	random_member(Reviewer, Agents).


publish(ListOfAuthors, Type, Content):-
	sim_time(Tick),
	id(publication,PublicationID),
	asserta(actions:publication(PublicationID,Journal,ListOfAuthors,Tick,Type, Content)).

publish(ListOfAuthors, Type, Journal, Content):-
	sim_time(Tick),
	id(publication,PublicationID),
	asserta(actions:publication(PublicationID,Journal,ListOfAuthors,Tick,Type, Content)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: read publication (random) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(read_publication_rnd,AgentID,_Options):- 
	agent(_,AgentID,_),
	findall(ID, actions:publication(ID,Journal,Authors,Tick,Type,Content),Pubs),
	length(Pubs, NP),
	select_publication_rnd(AgentID,NP).

select_publication_rnd(AgentID,NP):-
	NP<2, writeln('no publications to read').

select_publication_rnd(AgentID,NP):-
	NP>=2,
	agent(_,AgentID, Dict),
	LOI = Dict.get(list_of_interests),
	%get_interest(AgentID,Interest),
	%select_topic(Interest, TID),
	select_topic_list(LOI, TID),
	find_publication(TID,Publication),
	check_for_plagiatism(Publication);
	read_publication_rnd(AgentID, Publication).

check_for_plagiatism(Publication):-
	true.

double_publication(Publication, Author1, Author2):-
	actions:publication(Publication, Journal, Authors1,_, Tick1,TID),
	actions:publication(Publication, Journal, Authors2,_, Tick2,TID),
	intersection(Authors1,Authors2, Int), 
	Tick1 \= Tick2,
	Int = [].


read_publication_rnd(AgentID, []):-
	writeln('no publication').

read_publication_rnd(AgentID, Publication):-
	Publication \=[],
	update_belief_with_publication(publication, AgentID, Publication),
	actions:publication(Publication, Journal, Authors,_, theory_rnd,TID),
	actions:theory(AuthorID,rnd,TID,_,PotM,M), 
	agents:agent(_,AgentID, Dict),
	agents:agent(_,AuthorID, AuthorDict),
	Interests = Dict.get(list_of_interests),
	Domain = Dict.get(research_domain),
	AutInterests = AuthorDict.get(list_of_interests),
	AuthorDomain = AuthorDict.get(research_domain),
	intersection(Domain,AuthorDomain, Dom),
	intersection(Interests,AutInterests, Int),
	append(Dom,Int,Match),
	exec_citation(AgentID,AuthorID,Publication,Match).

exec_citation(AgentID,AuthorID,Publication,[]):-
	writeln('no intersection for publication').

exec_citation(AgentID,AuthorID,Publication,Match):-
	Match \=[],
	actions:plan(publish, AgentID),
	cite_rnd(AgentID,AuthorID,Publication).

cite_rnd(AgentID,CitedAuthor,Publication):-
	asserta(actions:citation(AgentID, CitedAuthor,Publication)).

select_topic(Interest, Theory):-
(	findall(PotID, actions:pot_model(PotID,Interest,_),PotMods), 
	random_member(PotMod,PotMods), 
	findall(TID, actions:theory(CreatorID, rnd, TID,_,PotMod, Mod), TIDs ),
	random_member(Theory, TIDs) ); Theory = [].

select_topic_list(InterestList, Theory):-
	(	findall(PotID, (member(Interst, InterestList), actions:pot_model(PotID,Interest,_)),PotMods), 
		random_member(PotMod,PotMods), 
		findall(TID, actions:theory(CreatorID, rnd, TID,_,PotMod, Mod), TIDs ),
		random_member(Theory, TIDs) ); Theory = [].

find_publication(TID,Publication):-
	findall(PubID, actions:publication(PubID, high_impact, Authors,_, theory_rnd,TID),HPubs), 
	findall(PubID, actions:publication(PubID, mid_impact, Authors,_, theory_rnd,TID),MPubs), 
	findall(PubID, actions:publication(PubID, low_impact, Authors,_, theory_rnd,TID),LPubs),
	%append([HPubs,MPubs,LPubs], [Publication|_]).
	(random_member(Publication,HPubs);
	random_member(Publication,MPubs);
	random_member(Publication,LPubs);
	( findall(PubID, actions:publication(PubID, _, Authors,_, theory_rnd,TID),Other), random_member(Publication,Other));
	Publication=[],!).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Publish (random content) 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
execute_action(publish_rnd, AgentID, _Options):- 
    msg_with_simple_name(AgentID,'action: publish'),
	findall(TID, theory(AgentID, rnd, TID, Link, _, _),PotT),
	plagiarize(AgentID);
	publish_rnd_no_j(AgentID,PotT).

publish_rnd_no_j(AgentID,[]):- writeln('nothing to publish').

publish_rnd_no_j(AgentID,PotT):-
	PotT \= [],
	random_member(SelectedTheoryID,PotT),
	publish_rnd(AgentID, oa, SelT, 1.01, 1 , 0, SelectedTheoryID).
/* Experiment 1(a)

execute_action(publish_rnd, AgentID, _Options):- 
	agent(Type,AgentID,Dict),
    msg_with_simple_name(AgentID,'action: publish'),
	findall(TID, theory(AgentID, rnd, TID, Link, _, _),PotT),
(	PotT \=[],
	select_journal(Journal, Threshold, RepFactor),
	random_member(SelectedTheoryID,PotT),
	quality_publication_rnd(AgentID,SelectedTheoryID,QualityPublication),
	rnd_rew(QualityPublication,AgentID, Result),
	publish_rnd(AgentID, Journal, SelT, RepFactor,Result,Threshold, SelectedTheoryID));true.
*/

publish_rnd(AgentID, Journal, SelT, RepFactor,Result,Threshold, SelectedTheoryID):-
	Result >= Threshold,
	exec_publish_rnd([AgentID], Journal, theory_rnd,SelectedTheoryID, accepted),
	gain_reputation(AgentID, RepFactor).

publish_rnd(AgentID, Journal, SelT, RepFactor,Result,Threshold, SelectedTheoryID):-
	Result < Threshold,
	rejected_rnd(AgentID,Result, SelectedTheoryID, Journal),
	reduce_reputation(AgentID).


plagiarize(AgentID):-
	sim_time(Tick),
	Tick > 110,
	actions:traitor(Traitor,TTick),!,
	Tick > TTick + 10,
	asserta(actions:traitor(AgentID, Tick)),
	write_diary(AgentID, "plagiarize"),
	findall(Publications, actions:publication(PublicationID,_Journal,Authors,_, Type,_Content), ListOfPubs),
	random_member(PublicationID, ListOfPubs),
	asserta(actions:publication(PublicationID,_Journal,[AgentID],Tick, Type,Content)).

quality_publication_rnd(AgentID,SelectedTheoryID,QualityPublication):-
	agents:agent(_,AgentID, Dict),
	Ability = Dict.get(ability),
	actions:theory(AgentID, rnd, SelectedTheoryID,_,_,ModID),
	actions:model(ModID, PotModID, Quality),
	QualityPublication = Quality * Ability + 1.

rejected_rnd(AgentID,Result,SelectedTheoryID, Journal):-
	sim_time(Tick),
	asserta(actions:publication_rejected(AgentID, Tick, Result, SelectedTheoryID, Journal)).

select_journal(Journal, Threshold,RepFactor):-
	random_member([Journal,Threshold,RepFactor,Type],
		[ [low_impact,4,1.01,closed], [mid_impact, 10,1.05,closed], [high_impact,25,1.2,closed] ]).

rnd_rew(QualityPublication,AgentID,Result):-
	select_reviewer_rnd(AgentID, ReviewerList),
	findall(Score, (member(Rev, ReviewerList), review_rnd(AgentID, Rev, QualityPublication, Score)), Scores),
	sum_list(Scores,Result).

select_reviewer_rnd(AgentID, ReviewerList):-
	agents:agent(_,AgentID, Dict),
	Domain = Dict.get(research_domain),
	(	findall(RevID, ( agents:agent(_,RevID, RevDict),
							RevDomain = RevDict.get(research_domain),
							intersection(Domain, RevDomain, IntDom),
							IntDom \=[] ),
							PotReviewerList),
		random_sublist(3,PotReviewerList,ReviewerList)) ; 
	(list_of_active_agents(Agents), random_sublist(3, Agents, ReviewerList )).

review_rnd(AgentID, Rev, QualityPublication, Score):-
	agents:agent(_,AgentID, Dict),
	agents:agent(_,Rev, RevDict),
	InterstsAgent = Dict.get(list_of_interests),
	Reputation = Dict.get(reputation),
	InterstsRev = RevDict.get(list_of_interests),
	append(InterstsAgent,InterstsAgent,AllInt),
	sort(AllInt,SortAllInt),
	length(SortAllInt,LenSortAllInt),
	intersection(InterstsAgent,InterstsRev,IntersectionInt),
	length(IntersectionInt,LenInt),
	Match is LenInt/LenSortAllInt*100,
	Score is (QualityPublication)+(Match/10)*Reputation.

conduct_pub_review_rnd(non-blind, ReviewerID, AgentID, Theory, Result):-
	agents:agent(_,AgentID, Dict),
	agents:agent(_,ReviewerID, RevDict),
	AuthorReputation = Dict.get(reputation),
	AuthorAbility = Dict.get(ability),
	Threshold = 1.5,
	check_draft_soundness(Theory, ReviewerID, Soundness),
	Review is (AuthorReputation + AuthorAbility) + Soundness,
	pub_review_result(AgentID, Threshold, Review, Result).

exec_publish_rnd([AgentID], Journal, theory_rnd, Theory, accepted):-
	publish([AgentID], theory_rnd, Journal, Theory).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Create Theory (random content)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(create_theory_rnd, AgentID, _Options):-
	msg_with_simple_name(AgentID,'action: create_theory_random'),
	agent(AgentType, AgentID, Dict), 
	get_interest(AgentID,BaseType),
	Ability = Dict.get(ability),
	findall(DataID, actions:data(DataID,AgentID,_,_, _, _,_), Datasets),
	length(Datasets, NBDataObjecs),
	random(0,Ability, RndAbility),
	SuccessChance = RndAbility + (NBDataObjecs/20),
	Quality = NBDataObjecs,
	create_theory_rnd(AgentID, rnd, BaseType, _, Quality, TID,SuccessChance).

create_theory_rnd(AgentID, TType, BaseType,Types, Relations, TID, SuccessChance):-
	SuccessChance =< 0.3, 
	writeln('no theory').

create_theory_rnd(AgentID, TType, BaseType,Types, Relations, TID, SuccessChance):-
	SuccessChance > 0.3,
	findall(PModID,actions:pot_model(PModID,BaseType, _),PotMods),
	exec_create_theory_rnd(AgentID, TType, BaseType,Types, Relations, TID,PotMods),
	plan(publish, AgentID).

exec_create_theory_rnd(AgentID, TType, BaseType,Types, Relations, TID,[]):-	
		id('POTMOD',NPotModID),
		id('MOD',ModID),
		id('THEORY',TID),
		asserta(actions:model(ModID, NPotModID, Relations)),
		asserta(actions:pot_model(NPotModID,BaseType, Types)),
		asserta(actions:theory(AgentID, TType, TID, Link, NPotModID, ModID)).

exec_create_theory_rnd(AgentID, TType, BaseType,Types, Relations, TID,PotMods):-
	PotMods \=[],
	random_member(PotModID,PotMods),
	id('MOD',NModID),
	id('THEORY',NTID),
	%asserta(actions:pot_model(PotModID,BaseType, Types)),
	asserta(actions:model(ModID, PotModID, Relations)),
	asserta(actions:theory(AgentID, TType, NTID, Link, PotModID, NModID)).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: publish_types findings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(publish_types, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: publish observation of event types'),
	findall(Observation, actions:data(DataID,AgentID,Tick,ContentType, _, _, Observation), ObservedTypes),
	%actions:belief(AgentID, Tick, observed_types, ObservedTypes, Domain, 0.7),
	publish([AgentID], observed_types, ObservedTypes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Funding (randomized, without strategy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(simple_funding, AgentID, _Options):-
	msg_with_simple_name(AgentID,'action: funding'),
	agent(Type,AgentID, Attributes),
	Resources = Attributes.get(resources),
	Reputation = Attributes.get(reputation),
	maxFounding(MaxFounding),
	maxInvestment(MaxInvestment),
	random(0,MaxFounding,ResourcesRandom),
	random(0,MaxInvestment,Investment),
	NewResources is (round(ResourcesRandom * Reputation) + Resources) - (round(Investment-Reputation)),
	%addlog(AgentID, action, generating_resources,NewResources),
	update_attributes(AgentID, resources, NewResources).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Funding (strategy: review process)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(funding, AgentID, _Options):-
	msg_with_simple_name(AgentID,'action: request funding'),
	% Write a proposal: Suggest observation of specific event type
	write_proposal(AgentID, Proposal),
	%format("Proposal ~w\n",[Proposal]),
	% Rewview proposal: Eligibility of the applicant, Evaluation of the idea
	review(AgentID, Proposal, Result),
	format("Result: ~w\n",[Result]),
	%writeln('#######################'),
	% Desission: funding or rejection 
	Result = "funding_recommended" ->
	execute_funding(AgentID); 
	reject_funding(AgentID).

write_proposal(AgentID, Proposal):-
	id(prop,PropID),
	agent(Type, AgentID, Dict),
	Subject = Dict.get(list_of_interests),
	Domain = Dict.get(research_domain), 
	Ability = Dict.get(ability), 
	Reputation = Dict.get(reputation), 
	Quality is (Ability + Reputation)/2,
	Proposal = [Subject, Domain, Quality].

review(AgentID, Proposal, Result):-
	Proposal = [Subject, Domain, Quality],
	select_reviewer(Domain,ListOfReviewerIDs)->
		conduct_nonblind_review( AgentID, Proposal, ListOfReviewerIDs, Result);
		new_subject(AgentID, Proposal, ListOfReviewerIDs, Result).

conduct_nonblind_review(AgentID, Proposal, ListOfReviewerIDs, Result):-
	writeln(ListOfReviewerIDs),
	Proposal = [Subject, Domain, Quality],
	Threshold = 0.7,
	findall(Fitting, 
			( member(Rev, ListOfReviewerIDs), 
			  reviewer_bias(Rev, Subject, Fitting) ), 
		Results),
	length(Results,LenResults),
	sum_list(Results, SumResults),
	originality(AgentID,Subject,OriginalityScore),
	familiarity(AgentID,Subject,FamiliarityScore),
	Result is ((SumResults/LenResults*Quality)+OriginalityScore)+FamiliarityScore,
	Result>Threshold *-> 
		Result = "funding_recommended" ; 
		Result = "funding_rejected".

conduct_blind_review(non-blind, AgentID, ProposalID, ListOfReviewerIDs, Result):-
	Proposal = [Subject, Domain, Quality],
	Threshold = 0.5,
	findall(Fitting, 
			( member(Rev, ListOfReviewerIDs), 
			  reviewer_bias(Rev, Subject, Fitting) ), 
		Results),
	Result is (SumResults/LenResults*Quality),
	Result>Threshold *-> 
		Result = "funding_recommended" ; 
		Result = "funding_rejected".

reviewer_bias(ReviwerID,PropSubject, Fitting):-
	agent(Type, ReviwerID, Dict),
	Subject = Dict.get(list_of_interests),
	subset(PropSubject,Subject)->Fitting =1; Fitting =0.

new_subject(AgentID, Proposal, ListOfReviewerIDs, Result):-
	Proposal = [Subject, Domain, Quality],
	originality(AgentID,Subject,OriginalityScore),
	familiarity(AgentID,Subject,FamiliarityScore),
	Evaluation is (Quality + OriginalityScore) + FamiliarityScore,
	Threshold = 0.5,
	Evaluation>Threshold -> 
		Result = "funding_recommended" ; 
		Result = "funding_rejected".

originality(AgentID,Subject,Originality):- 
	number_of_related_publications(AgentID, Subject, NumberRelatedPublications), 
	number_of_all_publications(AgentID, NumberOfAllPublications),

	(	NumberRelatedPublications >0,
		ShareRelatedPubs is (NumberRelatedPublications / NumberOfAllPublications) * 100,
		Originality is 100 - ShareRelatedPubs	);
	Originality = 0.


number_of_related_publications(AgentID, Subject, NumberRelatedPublications):-
	findall(RelatedPublicationID, 
		( actions:publication(RelatedPublicationID,_Journal,Authors,_Tick,_Type,_Content), 
		not(member(AgentID, Authors)),
		unfold_theory(Theory, Content), Content = [Subject|_Rest] ),
		RelatedPublications),
	length(RelatedPublications,NumberRelatedPublications).

number_of_all_publications(AgentID, NumberOfAllPublications):-
	findall(RelatedPublicationID, 
		(actions:publication(RelatedPublicationID,_Journal, Author,_Tick,_Type,_Content),
		not(member(AgentID, Authors))),
		AllPublications),
	length(AllPublications,NumberOfAllPublications).

familiarity(AgentID,Subject,FamiliarityScore):-
	findall(RelatedPublicationID, 
		(actions:publication(RelatedPublicationID,_Journal, Authors,_Tick,_Type,_Content), 
		member(AgentID, Authors),
		unfold_theory(Theory, Content), Content = [Subject|_Rest] ),
		RelatedPublications),
	length(RelatedPublications,NumberRelatedPublications),
	NumberRelatedPublications >0 ->
	FamiliarityScore = NumberRelatedPublications / 10;
	FamiliarityScore = 0.

reject_funding(AgentID):-
	msg_with_simple_name(AgentID,'funding rejected'),
	write_diary(AgentID, "Funding rejected"),
	reduce_reputation(AgentID).

reduce_reputation(AgentID):-
	agent(Type,AgentID, Attributes),
	Reputation = Attributes.get(reputation),
	NewReputation is Reputation * 0.7,
	update_attributes(AgentID, reputation, NewReputation).
	
select_reviewer(Domain,Reviewer):-
	findall(FID, ( agent(funder, FID, FDict), Domain = FDict.get(research_domain) ), PossibleReviewer),
	length(PossibleReviewer,Len),
	Len > 1 ->
	random(1,Len,NumberReviewer),
	random_sublist(NumberReviewer, PossibleReviewer, Reviewer),
	length(Reviewer,NBRev),
	NBRev > 1.
	% find reviewer
	% matching interest of reviewer and applicant
	% reputation of applicant
	% execute funding
	
execute_funding(AgentID):-
	msg_with_simple_name(AgentID,'execute approved'),
	agent(Type,AgentID, Attributes),
	Resources = Attributes.get(resources),
	maxFounding(MaxFounding),
	random(0,MaxFounding,ResourcesRandom),
	NewResources is ResourcesRandom +Resources,
	gain_reputation(AgentID),
	write_diary(AgentID, "Funding approved"),
	%addlog(AgentID, action, generating_resources,NewResources),
	update_attributes(AgentID, resources, NewResources).

gain_reputation(AgentID):-
	agent(Type,AgentID, Attributes),
	Reputation = Attributes.get(reputation),
	NewReputation is Reputation * 1.2,
	update_attributes(AgentID, reputation, NewReputation).

gain_reputation(AgentID, Factor):-
	agent(Type,AgentID, Attributes),
	Reputation = Attributes.get(reputation),
	NewReputation is Reputation * Factor,
	update_attributes(AgentID, reputation, NewReputation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Experiment - Common occurrence of events
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
execute_action(experiment_common_events, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: experiment_common_events'),
	agent(Type,AgentID, Attributes),
	sim_time(Tick),
	(ongoing_experiment(AgentID, Tick)*->
		continue_observation(AgentID, ListOfBaseEvents);
		make_new_observation(AgentID, ListOfBaseEvents)),
	observation(AgentID, Tick, ListOfBaseEvents).
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Observe event types
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(observation_types, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: observation (type)'),
	agent(Type,AgentID, Dict),
	sim_time(Tick), 
	events_in_tick(Tick) *->
	(	observe_location(Tick, PossibleObservation),
		extract_types(PossibleObservation,Types),
		analyse_observation(Dict,Types, Observation),
		publish_observation_types(AgentID,Tick,Observation));
	writeln('... no events to observe.').

events_in_tick(Tick):-
	create_world_module:event(_,_,Tick,_),!.


publish_observation_types(AgentID,Tick,[]):-
	writeln('... no observation to publish').

publish_observation_types(AgentID,Tick,Observation):-
	Observation \= [],
	ContentType = observed_types,
	uuid(DataID),
	assertz(actions:data(DataID,AgentID, Tick, ContentType, _, _, Observation)),
	assertz(actions:belief(AgentID, Tick, observed_types, [Observation], Domain, 0.7)),
	plan(publish_types, AgentID).

analyse_observation(Dict, [], _):-
	writeln('... not types.').

analyse_observation(Dict, Types, Observation):-
	Types \=[],
	Ability = Dict.get(ability),
	length(Types,NumberOfPossibleObservedTypes), 

	NumberOfObservedTypes is (round(Ability * NumberOfPossibleObservedTypes)),
	random_sublist(NumberOfObservedTypes,Types,Observation).

observe_location(Tick,PossibleObservation):-
	findall(EID, create_world_module:event(EID, _,Tick,_), EIDs)*->
	(	random_member(EventID,EIDs),
		neighborevents(EventID,PossibleObservation)	);
	writeln('... no events to observe.').

extract_types([],_):-
	writeln('... no types observed').

extract_types(Observation,SortTypes):-
	Observation \=[],
	findall(EventType, (member(Event,Observation), create_world_module:event(_EID, EventType, _T, L)), Types),
	sort(Types,SortTypes).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Select new interest
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(select_new_interest,AgentID, _):- 
	msg_with_simple_name(AgentID,'action: select new type (interest)'),

	findall(ObservedTypes, ( actions:belief(AgentID, _Tick, observed_types, ObservedTypes, Domain, Str), Str > 0.5 ), ListOfInterestingObservations),
	select_observation(ListOfInterestingObservations, Observation),
	select_new_type(Observation,Type),
	switch_interest(AgentID, Type).

select_observation([], _):-
	writeln('... no fitting observation.').

select_observation(ListOfInterestingObservations, Observation):- 
	random_member(Observation, ListOfInterestingObservations).

select_new_type([],Type):- 
	writeln('... no fitting types observed.').


select_new_type(Observation,Type):- 
	random_member(Type,Observation).

switch_interest(AgentID, []):- 
	write_diary(AgentID, "remain with old interest"),
	writeln('... sticking with existing interests').

switch_interest(AgentID, Interest):- 
	write_diary(AgentID, "new interest"),
	update_attributes(AgentID,list_of_interests,Interest).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Theory Type 1 Validation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(validation_type1, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: Theory Type 1 validation'),
	select_t1_theory(AgentID, TheoryID), 
	TheoryID \= []*->
	theory_t1_validation(AgentID, Theories);
	msg_with_simple_name(AgentID,'action: no theory for validation').

select_t1_theory(AgentID, TheoryID):-
	findall(TheoryID,
	(	actions:belief(AgentID, Tick, theory, TheoryID, Domain, Str),Str>0.4)*-> 
		actions:theory(_,type1,TheoryID,_,_,_), Type1TheoryList),
	Type1TheoryList \= [] *->
		random_member(TheoryID,Type1TheoryList);
		(	TheoryID = [], 
			msg_with_simple_name(AgentID,'action: no fitting belief')).

% Validates a given Theory of type t1 against own observation data 
theory_t1_validation(AgentID, TheoryID):-
	actions:theory(CreatorID,TheoryType,TheoryID,_Link,_PotMod,_Mod)*->
	(	unfold_theory(TheoryID,[Type,T1,T2]),
		actions:data(DataID_T1, AgentID, PTick_T1,ContentType_T1, Type, Location_T1, Observation_T1)*->
		(	T2 = [Count,Dist,[Elety,ElePos]], 
			(	member(Elety,Observation_T1)*->
					theory_t1_validation(AgentID, TheoryID, validated);
					theory_t1_validation(AgentID, TheoryID, not_validated)));
			msg_with_simple_name(AgentID,'action: no data'));
			msg_with_simple_name(AgentID,'action: no fitting theory').

theory_t1_validation(AgentID, TheoryID, validated):-
	msg_with_simple_name(AgentID,'action: Theory Type 1 validated'),
	retractall(actions:belief(AgentID, Tick, theory, TheoryID, Domain, Str)),
	asserta(actions:belief(AgentID, Tick, theory, TheoryID, Domain, 1.0)),
	actions:theory(CreatorID,_TheoryType,TheoryID,_Link,_PotMod,_Mod),
	gain_reputation(CreatorID).

theory_t1_validation(AgentID, TheoryID, validated):-
	msg_with_simple_name(AgentID,'action: Theory Type 1 validated'),
	retractall(actions:belief(AgentID, Tick, theory, TheoryID, Domain, Str)),
	asserta(actions:belief(AgentID, Tick, theory, TheoryID, Domain, 0.1)),
	actions:theory(CreatorID,_TheoryType,TheoryID,_Link,_PotMod,_Mod),
	reduce_reputation(CreatorID).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Validation Experiment 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(validation_experiment, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: conduct validation experiment'),
	agent(Type,AgentID, Attributes),
	sim_time(Tick),
	select_theory_from_bel(AgentID, Theory),
	set_up_experiment(Theory, Tick, Location),
	set_up_observation(Theory, Tick, Location, Observation),
	analyze_experiment(Observation),
	publish_experiment_results(AgentID, Tick, Results).

select_theory_from_bel(AgentID, Theory):- 
	findall(actions:belief(AgentID, Tick, theory, T, Domain, Str),
			actions:belief(AgentID, Tick, theory, T, Domain, Str),
			ListOfBel),
	random_member(actions:belief(AgentID, _, theory, Theory, _, _),ListOfBel).

set_up_experiment(TheoryID, Tick, Location):- 
	actions:theory(_AID,_TP, TheoryID,_Link,PotModID, ModID),
	actions:model(ModID, PotModID, Relations),
	writeln(Relations).

set_up_observation(Theory, Tick, Location, Results):- 
	%observe location in tick +1
	true. 

analyze_experiment(Observation):-true.

publish_experiment_results(AgentID, Tick, Results):- true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Observation of events
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(observation_of_events, AgentID, _Options):- 
	msg_with_simple_name(AgentID,'action: observation (events)'),
	agent(Type,AgentID, Attributes),
	sim_time(Tick),
	% Check if there is an ongoing observation
	(ongoing_experiment(AgentID, Tick)*->
		% If there is an ongoing observation -> continue with same type
		continue_observation(AgentID, ListOfBaseEvents);
		% If this is a new obs. experiment, get interests 
		make_new_observation(AgentID, ListOfBaseEvents)),
	% conduct observation
	observation(AgentID, Tick, ListOfBaseEvents).

% checks, if there is already a running experiment (existing data from tick -1)
ongoing_experiment(AgentID, Tick):-
	PTick is Tick - 1,
	actions:data(DataID,AgentID, PTick,_ContentType, _BaseType, _Location, _Observation),!.
	%addlog(AgentID, subaction, ongoing_experiment,_).

observe_event(AgentID, Event, Tick):-
	neighborevents(Event,Observation), 
	%utils:events_in_range(Event, Observation, Tick), 
	create_world_module:event(Event, BaseType,_Tick, Location),
	ContentType = observation_data,
	uuid(DataID),
	assertz(actions:belief(AgentID, Tick, ContentType, DataID, Domain, 1.0)),
	assertz(actions:data(DataID,AgentID, Tick, ContentType, BaseType, Location, Observation)),
	addlog(AgentID, action, doing_empirical_research,_).

% continue observation (there is aleady data in tick -1)
continue_observation(AgentID, ListOfEventIDs):-
	msg_with_simple_name(AgentID,'action: continue_observation'),
	sim_time(Tick), PTick is Tick -1, 
	actions:data(DataID,AgentID, PTick,_ContentType, BaseType, Location, _Observation),
	findall(ID, create_world_module:event(ID, EventType, PTick, Location), ListOfEventIDs),
	addlog(AgentID, subaction, continue_observation,_).

% make a new observation (there is no data to continue)
make_new_observation(AgentID, Observation):-
	msg_with_simple_name(AgentID,'action: make_new_observation'),
	addlog(AgentID, subaction, make_new_observation,_),
	sim_time(Tick),
	get_interest(AgentID, EventType),
	findall(ID, create_world_module:event(ID, EventType, Tick, Location), Observation).

% Make a observation / returns list of events
observation(AgentID, Tick, ListOfBaseEvents):-
	agent(_, AgentID, Attributes ),
	Ability is Attributes.get(ability),
	length(ListOfBaseEvents,ListOfBaseID ),
	Scope is round(ListOfBaseID*Ability),
	%( RndScope > 400 *->  Scope is 400; Scope is RndScope ), 
	random_sublist(Scope, ListOfBaseEvents, Observation),
	%writeln(Scope),
	%writeln(ListOfBaseEvents),
	%writeln(Observation),
	% forall(member(Event,Observation), observe_event(AgentID,Event,Tick)).
	cores(NumberOfCores),
	findall(Goal,observation_concurrent(Goal,Observation, AgentID,Event,Tick),Goals),
	concurrent(NumberOfCores, Goals, []).

observation_concurrent(Goal, Observation, AgentID,Event,Tick):-
	%forall(member(Event,Observation), observe_event(AgentID,Event,Tick)).
	member(Event,Observation),
 	Goal = observe_event(AgentID,Event,Tick).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action: Debug
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

execute_action(debug, AgentID, _):-
	sim_time(Tick),
	list_of_active_agents(ActiveAgents),
	length(ActiveAgents,LenActive),
	addlog(AgentID, action, debug,_).
	 % assertz(actions:debug(Tick, AgentID, ActiveAgents,LenActive)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% execute_action: Test hypothese
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*
execute_action(test_hypothese, AgentID, _Options):- true.
	%findall( belief(AgentID, Tick, hypothese, Hypothese, Domain, Percentage) 
	%select best hypothese 
	% plan to look for this kind of data
	%  
	belief(AgentID, Tick, hypothese, Hypothese, Domain, Percentage)
	Hypothese = [Relation, Intersection]
*/

% event(A,B,C,D,E)
%eventIDs_to_eventTypes(ListOfIDs,ListOfTypes):-

%['EVENTbd96cc18-6197-11ea-a20a-477109ff6423','EVENTbd977276-6197-11ea-b5d9-d7e77a9557a2']
% data(A1,B1,C,Location,E1), data(A2,B2,C,Location,E2),  B1 = X, X is B2+1, TS1 = [B1, E1], TS2 = [B2,E2], pattern(TS1,TS2).