/** <module>  PROLOG SOCIAL SIMULATION SYSTEM main module

This is the main simulation module and includes the main logic for simulation runs and ticks. 

@author Daniel Kurzawe
@version ALPHA
@see  http://www.daniel-kurzawe.de
@license GNU/GPL V.3 http://www.gnu.org/copyleft/gpl.html
*/


:- style_check(-singleton).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% For debuging %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Visualisation of program structure %%%
 :- use_module(library(callgraph)).
%%% run command:  module_dotpdf(user,[method(unflatten([fl(4),c(4)]))]).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moduls %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%:- module(simulation).


%:- module(kernel,[sim_time/1]).

%%% CSV file output library %%%
:- use_module(library(csv)).

%%% Action definitions and logic %%%
:- use_module('utils.pl').

%%% Action definitions and logic %%%
:- use_module('configuration.pl').

%%% Action definitions and logic %%%
:- use_module('actions.pl').

%%% Agents definitions and logic %%%
:- use_module('agents.pl').

%%% Create laws %%%
:- use_module('create_world_event.pl').

%%% World facts database %%%
% :- use_module('world_facts.pl').

%%% Screen output %%%
:- use_module('screen_output.pl').

%%% File output %%%
:- use_module('file_output.pl').

%%% Publication addition %%%
:- use_module('publication.pl').

%%% For calling influxdb and grafana (monitoring) over RESTful interface
:- use_module(library(http/http_client)).

% Dynamic defintion for predicates, acting as "global variables"
:- dynamic([sim_time/1, run_id/1, experiment_id/1, experiment_dir/1, diary_dir/1, abort_run/1, abort/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initalisation of variables and Constants %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
version('0.7').
:- cls.
:- current_prolog_flag(cpu_count,NumberOfCores), 
   thread_pool_create(threadpool, NumberOfCores, []).
:- message(welcome,_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation start %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% MAIN START: This predicate starts the simulation experiment (multiple runs)

%! start is nondet
% This predicate starts the simulation experiment.
start:-
	initialisation_experiment,
	message(experiment,_),
	number_of_runs(Runs),
	foreach( between(0,Runs,Seed),
		     time(run(Seed)) ),
	message(border,_), 
	message(runstate,_).

start_menue:-
	menu('Simulation Options',
	[ start : 'Start a simulation run'
	, check_configuration : 'Check configuration'
	, halt     : 'Quit'
	], Choice), call(Choice).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation: Main loop %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Main loop %%%

%! run(Seed) is nondet
% Starts a simulation run with a given seed
run(Seed):-
	% set_random(seed(Seed)),
	initialisation_run,
	number_of_ticks_for_prerun(NumberOfTicksPreRun),
	number_of_ticks(NumberOfTicks),
	update_number_of_ticks(NumberOfTicks,NumberOfTicksPreRun,NewNumberOfTicks),
	prerun(NumberOfTicksPreRun),
	StartActualRun is NumberOfTicksPreRun + 0,
	initialisation_agents,
	%writeln(StartActualRun),
	%writeln(NewNumberOfTicks),
	writeln('start run'),
	run(StartActualRun,NewNumberOfTicks),
	% TODO Results deaktiviert
	write_results,
	%message(summary,_),
	cleanup.

%! prerun(NumberOfTicks) is nondet
% Executes simulation runs before the actual start of the main simulation experiment
prerun(NumberOfTicksPrerun):-
	writeln('... performing prerun'),
	( between(0, NumberOfTicksPrerun, C),
	  update_world,
	  update_sim_time(C), 
	  %sim_time(ST),
	  % writeln(ST),
	  fail;true ).

%! run(End,End) is nondet
%%% Exit conditions for the end of a simulation run %%%
run(End,End):- 
	not(abort_run(true)),
	message(finish,_), 
	run_id(RunID), 
	export_laws,
	assertz(finish_state(RunID, completed)).

%! run(Counter,End) is nondet
% Main loop definition 
run(Counter,End):- 
	Counter<End,
	not(abort_run(true)),
	%tty_goto(0, 40),
	format('~t<< Tick: ~w >>~t~72|~n',[Counter]),
	Counter1 is Counter + 1,
	tick,
	% sim_time(Tick),
	% TODO Results deaktiviert
	% start_monitoring,
	write_results(between_ticks),

	update_sim_time(Counter1),
	run(Counter1,End).
	

run(_X,_Y):-
	abort_run(true),
	writeln(' >>> ABORT RUN <<<'),
	message(finish,_), 
	%run(Y,Y),
	run_id(RunID), 
	assertz(finish_state(RunID, failed)).
/*
run(Start,End):- 
	between(Start,End,Counter),
	(tick,	
	update_sim_time(Counter),
	writeln(Counter),
	(abort_run(true), !; abort_run(false), true),
	fail;true),  
	message(finish,_).
*/

%! update_agents is nondet
% Updates all agents. 
update_agents:-
	agent_activity,
	create_new_agents,
	update_list_of_active_agents.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation: Tick %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! tick is nondet
% Definition of a tick (single time step in Simulation).
tick:-
	sim_time(Tick),
	% Performs laws (creats new events)
	write('Update World | '),
	update_world,
	
	% Precalculation of the reach of each event (high ram consumption)
	% update_range,

	write('Update Agents | '),
	update_agents, 

	write('Perform Actions | '),
	perform_actions, 

	write('Check World  \nl'),
	check_world_consisty(Tick), 

	number_of_events_in_tick(Tick, NumberEvents),
	format('Events: ~w ~t~72|~n',[NumberEvents]),
	findall(log(_,_,_,_,_),
			log(_RunID, Tick, _, law, apply_law, _),Laws),
	length(Laws, LenLaws),
	format('Active Law: ~w ~t~72|~n',[LenLaws]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Consisty test %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! check_world_consisty(Tick) is nondet
% Checks if World is stable
check_world_consisty(Tick):-
	active_laws_in_tick(Tick),
	check_events(Tick),
	check_short_pattern(Tick),
	check_laws(Tick).

check_events(Tick):-
	number_of_events_in_tick(Tick, NumberEvents),
	format('Number Events: ~w ~t~72|~n',[NumberEvents]),
	NumberEvents=0 *-> (abort_run, writeln( ">>> ABORT - NO EVENTS <<"));true.

check_short_pattern(Tick):-
	law_pattern(Tick)->(abort_run,writeln( ">>> ABORT - SHORT PATTERN <<"));true.

law_pattern(Tick):-
	BTick is Tick -1, 
	BBTick is Tick -2, 
	BBBTick is Tick -3, 

	abort(Tick,LenActiveLaws),
	abort(BTick,LenActiveLawsB),
	abort(BBTick,LenActiveLaws),
	abort(BBBTick,LenActiveLawsB).

check_laws(Tick):-
	laws_repeate(Tick)->(abort_run,writeln( ">>> ABORT - ALL LAWS ACITVE<<"));true.

active_laws_in_tick(Tick):-
	findall(log(_,_,_,_,_),log(_,Tick, _,law, apply_law, _),Laws),
	length(Laws, LenActiveLaws),
	asserta(abort(Tick, LenActiveLaws)).

laws_repeate(Tick):-
	active_laws_in_timeframe(Tick,LenActiveLaws),
	abort(Tick,LenActiveLaws).

active_laws_in_timeframe(Tick,Laws):-
	BTick is Tick -1, 
	BBTick is Tick -2, 
	abort(Tick,LenActiveLaws),
	abort(BTick,LenActiveLaws),
	abort(BBTick,LenActiveLaws).


%! compare_active_laws(Tick) is nondet
% Looks, if laws repeat to often (all laws might be active each tick)
compare_active_laws(Tick):-
	OT is Tick -1,
	OTT is Tick -2,
	abort(Tick, LenLaws),
	abort(OT, LenLaws),
	abort(OTT, LenLaws),
	writeln('>>>>> RUN ABORTED - UNINTENDEND LOOP / ALL LAWS FIRE AT SAME TIME <<<<<').

%! abort_run is nondet
% Sets the abort signal for the run
abort_run:-
	retractall(abort_run(_)), 
	asserta(abort_run(true)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initialisiation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! create_agents(Number_Of_Agents) is nondet
% Creates a given number of agents.
create_agents(Number_Of_Agents):-
	foreach(between(1, Number_Of_Agents, _X), construct_agent).
%! create_facts(Number_Of_Facts) is nondet
% Create a given number of facts.
/*
create_facts(Number_Of_Facts):-
	foreach(between(1, Number_Of_Facts, _),generate_fact).
*/

%! assign_beliefs(Number_Of_Beliefs_Per_Agent) is nondet
% Assign a given number of random beliefs to each agent
assign_beliefs(Number_Of_Beliefs_Per_Agent):-
	foreach(between(1, Number_Of_Beliefs_Per_Agent, _),random_belief_for_each_agent).

%! initialisation_run is nondet
% Initalisation of a simulation run.
initialisation_run:-
% Reset internal simulation time to tick 1
	asserta(sim_time(0)),
	asserta(abort_run(false)),
	asserta(actions:traitor(0,0)),
% Create specific ID for this run
	run_id,
	% Create folder for saving files for this run
	create_dir(run),
	create_dir(diary),
   % Create simulated environment (laws, objects,...)
	create_world,
	create_domains,
	check_configuration,
	init_results(between_ticks).

initialisation_agents:-
	% Create agents for the first tick
	number_of_agents_at_start(Number_Of_Agents),
	create_agents(Number_Of_Agents),
	number_of_facts_at_start(Number_Of_Facts),
	% create_facts(Number_Of_Facts), NOT USED -> NOW EVENTS
	%assign_beliefs(5), % Sinn dieser Konstanten? In Config übertragen?
	% Creates research
	findall(agent(Type,ID,Attributes),
	agent(Type,ID,Attributes),List_Of_Agents),
	update_list_of_active_agents,
	%assertz(actions:publication(dummy, dummy,dummy,dummy,dummy,dummy)),
	create_first_run(List_Of_Agents).

%! create_first_run(List) is nondet
% Setup for the first run
create_first_run(List):-
	member(Agent,List),
	asserta(time(0,0,Agent)),
	fail;true.

%! initialisation_experiment
% Creates an ID and folder for an experiment
initialisation_experiment:-
	experiment_id,
	prepare_folder,
	read_name_file.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% IDs %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! experiment_id is nondet
% Creates unique IDs for the experiment (series of runs) and every single runs.
experiment_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([experiment,Year,Month,Day,Hour,Min,Sec],"-",Experiment_id),
	retractall(experiment_id(_)),
	asserta(experiment_id(Experiment_id)).

%! run_id is nondet
% Creates unique IDs for each run.
run_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([run,Year,Month,Day,Hour,Min,Sec],"-",Run_ID),
	retractall(run_id(_)),
	asserta(run_id(Run_ID)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Cleanup %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! cleanup is nondet
% Cleans memory after each run. Eg. retracting old run dependent information from memory. Use it after storing results. 
cleanup:-
	retractall(abort_run(_)),
	remove_world,
	retractall(abort(_,_)),
	retractall(agents:agent(_,_,_)),
	retractall(actions:belief(_,_,_,_,_,_)),
	retractall(fact(_,_,_)),
	retractall(fact(_)),
	retractall(actions:model(_,_,_)),
	retractall(actions:citation(_,_,_)),
	retractall(actions:pot_model(_,_,_)),
	retractall(actions:theory(_,_,_,_,_,_)),
	retractall(actions:data(_,_,_,_,_,_,_)),
	retractall(actions:log(_,_,_,_,_,_)),
	retractall(actions:traitor(_,_)),
	retractall(actions:publication_rejected(_,_,_,_,_)),
	retractall(events_in_range(_,_,_)),
	retractall(actions:publication(_,_,_,_,_,_)),
	retractall(research_domain(_,_)),
	retractall(sim_time(_)).
  
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  %%% Write results %%%
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  %! write_results is nondet
  % Writes results of a simulation run into files.
  write_results:-
	  write_results(alter_csv),
	  write_results(agent_statistic_age_csv),
	  write_results(theories),
	write_results(theory_net),
	  % write_results(log),
	  write_results(eventspertick),
	  write_results(overview).
  
