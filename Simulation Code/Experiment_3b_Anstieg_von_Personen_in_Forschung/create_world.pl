%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Module create_world %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This module creats a world, consisting of objects, relations and laws.
% Objects can have properties and be in relation to each other.
% Laws defining the progress of the world over time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For creating a visualisation of the module structure
% :- use_module(library(callgraph)).
% and call:
% module_dotpdf(user,[method(unflatten([fl(4),c(4)]))]).

% CLP(FD): Constraint Logic Programming over Finite Domains
% Has to be loaded from main module
% :- use_module(library(clpfd)).

:- module(create_world_module,
  [create_world/0, remove_world/0, update_world/0,
    object/1, type/2, property/2, property/3]).

:- dynamic( [create_world/0, object/1, object/3, type/2, property/2, property/3] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Constants - primary for create_world module %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Creates a list of types for convenience and readablity of code.
list_of_object_types(List):-
  findall(type(ID,Content),type(ID,Content), List).

list_of_properties(List):-
  findall(property(ID,Property), object_property(ID,Property), List).

list_of_propertyIDs(List):-
  findall(ID, object_property(ID,_Property), List).

list_of_object_prototypes(List):-
  findall(object_prototype(ID, Type, ListOfProperties),
          object_prototype(ID, Type, ListOfProperties),List).

list_of_relation_prototypes(List):-
  findall(relation_prototype(ID, ListOfObjects),
          relation_prototype(ID, ListOfObjects),List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% manage world %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a world with objects, relations and laws
create_world:-
  create_properties,
  create_object_types,
  create_object_prototypes,
  create_objects,
  create_relations.
  %create_laws.


% update world (call once in each tick)
update_world:-
  apply_laws.

% remove a world from memory
remove_world:-
  retractall(object_prototype(_,_,_)),
  retractall(type(_,_)),
  retractall(property(_,_)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% managing components %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_properties:-
  number_of_properties(NumberOfProperties),
  ( between(1,NumberOfProperties,_Counter),
    create_property(_),fail;true ).

create_object_types:-
  number_of_object_types(NumberOfObjectTypes),
  ( between(1,NumberOfObjectTypes,_Counter),
    create_object_type(_),fail;true ).

create_object_prototypes:-
  number_of_proto_objects(NumberOfProtoObjects),
  ( between(1,NumberOfProtoObjects,_Counter),
    create_object_prototype(_),fail;true ).

% :-  thread_pool_create(media, 20000, []).
create_objects:-
  number_of_objects(NumberOfObjects),
  ( between(1,NumberOfObjects,_Counter),
  %thread_create_in_pool(media, create_object(_), _Id, [stack_limit(2 000 000 000 000)]) ,fail;true ).
  %thread_create(create_object(_), _Id, [stack_limit(2 0 000 000)]) ,fail;true ).
    create_object(_),fail;true ).

create_relations:-
  number_of_relations(NumberOfRelations),
  ( between(1,NumberOfRelations,_Counter),
    create_relation(_),fail;true ).

create_laws:-
  number_of_laws(NumberOfLaws),
  ( between(1,NumberOfLaws,_Counter),
    create_law(_),fail;true ).

apply_laws:-
  % für alle Regeln ...
  apply_law(Law).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create actual objects and dependencies %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create an object of random type %%%

create_object(Object):-
  list_of_object_prototypes(Protolist),
  random_member(ProtoObject, Protolist),
  ProtoObject = object_prototype(ProtoObjectID, TypeID, _ValueList),
  create_specific_object(ProtoObjectID,Object).

%%% Create an object of with specific type %%%
% object(ObjectID, Properties)
% Example: object( 232(...), Properties (...))
% Creates a concrete object from a protoobject.
create_specific_object(PrototypID,Object):-
  object_prototype( PrototypID, ObjectTypeID, PropertyIDList ),
  uuid(ObjectID),
  forall(
    member(PropertyID,PropertyIDList),
    define_properties(ObjectID, PropertyID)),
%%% OLD STYLE OF OBJECT
  Object = object(ObjectID),
  asserta(Object),

%%%%%%%% OBJEKT BRAUCHT EIGENSCHAFTEN! %%%%%%%%%%%%
    % EIGENSCHAFTEN IN OBJEKT?
  ObjectNew = object(ObjectID, PropertyIDList),
  asserta(ObjectNew).

%%% Creates properties for a given object %%%
define_properties(ObjectID, PropertyID):-
  object_property(PropertyID, [Low, High]),
  random(Low, High, Value),
  Property = object_property(ObjectID, PropertyID, Value),
  asserta(Property).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% create prototypes %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Create an object prototype %%%
% object_prototype( ID, TID, [property('a12(...)', [default_value]), property('a12(...)', [default_value])] ).
% Example: object_prototype( 321(...), 813(...), (...) )
% Object types determine the type of an object
% by assigning a set of properties to it.
create_object_prototype(ObjectPrototype):-
  uuid(UniqueObjectID),
  list_of_object_types(Types),
  random_member(Type,Types),
  Type = type(TypeID,ListOfProperties),
  ObjectPrototype = object_prototype( UniqueObjectID, TypeID, ListOfProperties ),
  asserta(ObjectPrototype).

%%% Create an object type %%%
% type(ID,SelectionOfProperties).
% Example: type('814(...)', [property('a12(...)', [default_value]), property('a12(...)', [default_value])]).
% Object types determine the type of an object
% by assigning a set of properties to it.
create_object_type(Type):-
  uuid(ID),
  maximal_properties_per_type(MaxProperties),
  random_between(1,MaxProperties,NumberOfProperties),
  list_of_propertyIDs(ListOfPropertyIDs),
  % ... build a random sublist for $SelectionOfProperties
  findall( Property ,
            ( between(1,NumberOfProperties,_Counter),
              random_member(Property, ListOfPropertyIDs) ),
           SelectionOfProperties ),
  Type = type(ID,SelectionOfProperties),
  asserta(Type).

%%% Create a porperty %%%
% property(ID, Domain)
% Example: property('5baea2de-0ac1-11e8-b1a9-ab7a8c0e46b0', [23,123]).
% This predicate represents properties that define object types.
create_property(Property):-
  uuid(ID),
  % Value is empty and can be used by laws and objects
  create_domain(Domain),
  Property = object_property(ID,Domain),
  asserta(Property).

% Creates a domain for a property
create_domain(Domain):-
  random(0,100,Low),
  random(100,200,High),
  Domain = [Low,High].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create relations %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%relation(ID, Object1,...,ObjectN)

%object_property(PropertyID, [Low, High]),
%random(Low, High, Value),
%Property = object_property(ObjectID, PropertyID, Value),

create_relation_prototype(Relation_Prototype):-
  % Finde alle Objektearten
  list_of_object_types(Types),
  select_types(Types,SelectedTypes).
% relation_prototype(Types1, ... TypeN)

select_types(Types, SelectedTypes):-
  random_member(Types, Type).



create_relation(Relation):-
  objecttypes_in_relation(ListOfObjectTypesInRelation),
  uuid(ID),
  %append([relation,ID],ListOfTypesInRelation,RelationList),
  %Relation =.. RelationList,
  append([relation,ID],ListOfObjectTypesInRelation,ListOfObjectTypesInRelationID),
  %list_to_tuple(ListOfObjectTypesInRelationID,ToupleObjectRelationID),
  %RelationList = relation(ToupleObjectRelationID),
  Relation =.. ListOfObjectTypesInRelationID,
  asserta(Relation).

objecttypes_in_relation(ListOfObjectTypesInRelation):-
  relation_arity(MaxArity),
  random_between(1,MaxArity,Arity),
  list_of_propertyIDs(ListOfPropertyIDs),
  findall(Res,
    ( between(0,Arity,_Counter),
      random_member(PropertyID,ListOfPropertyIDs),
      object_property(PropertyID, [Low, High]),
      random(Low, High, Value),
      Res = object_property(OID, PropertyID,Value)
    ),
    ListOfObjectTypesInRelation).

find_objects_by_type(TypeID,ListOfObjects):-
  findall(ObjectID,
            ( object(ObjectID),
              object_property(ObjectID, PropertyID, Value)),
          ListOfObjects ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create laws %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% relation(ID, object_prototype(ID,Type,Prop), ... , object_prototype(ID,Type,Prop) )
% law(ListOfObjects,Relations):- ???

% Laws in the simulation are simplfied and have the structure
% Phi -> Psi
% and are closed under (and) (or) (not) (than)
% Example P_1 (and) ... (and) P_n (than) Q


%%% Writes a file with laws as prolog facts %%%
%%% Alternative mit Datei-Lösung %%%


% 1. mache ein Potential Model

/*
create_pot_law(PL):-
  [Objects, RelationsBetweenObjects],
  asserta(potential_law(Set)).

create_law_goal(LG):-


law(Objects,Relations):-
  potentail_law(Objects,Relations),
  goal



*/


create_law(Law):-
  % find all relations
  % select some relations
  % create a potential law
  % create a goal
  % law is: if potential law THAN goal

  list_of_relation_prototypes(RelationPrototype),


  % create laws ...%0



%  create_law_file(File).


create_law_file(File).

build_law_head(Head, RandomizedListOfRelationsInLaw):-
  trace,
  random_member(AllObject,RandomizedListOfRelationsInLaw),
  /*
  writeln('*********** RELATIONS IN LAW ***********'),
  writeln(RandomizedListOfRelationsInLaw),
  writeln('*********** RELATIONS IN LAW ***********'), */

  Head = test.


build_law_body(Body, RandomizedListOfRelationsInLaw):-
  RandomizedListOfRelationsInLaw = [ConsequentRelation|Precondition],
  writeln(ConsequentRelation),
  writeln(Precondition),
  %alist_to_tuple(AntecedentList,AntecedentTuple),
  %Ant = forall(AntecedentTuple,ConsequentRelation),
  %create_consequent,
  %create_antecedent,

  true.
  % create_event einbauen.

% Temp for testing
%:-dynamic(tick/1).
tick(1).

create_event(Event):-
  tick(Tick),
  NTick is Tick + 1,
  asserta(event(Event,NTick)).

write_law_file(File):-
  true.
  /*
  open(File,write,Out),
  writeln(Out,'%%% Automatic generated laws %%% '),
  writeln(Out,'%%% Modul description (...) %%% '),
  writeln(Out,':- module(theory_laws,[law/3]).'),
  writeln(Out,'%%% Laws %%%'),
  writeln(Out,'law(X,Y,Z):-writeln(X,Y,Z).'),
  close(Out).*/



% Find all types of given objects and returns a set
types_in_object_list_as_set(ObjectList,SetOfTypesInLaw):-
  findall(TypeOfObject,
            ( member(ObjectInLaw,ObjectList),
              ObjectInLaw = [ object_prototype(_,TypeOfObject,_) ]),
          ListOfTypesInLaw ),
          list_to_set(ListOfTypesInLaw,SetOfTypesInLaw),
          write('Types : '),
          writeln(SetOfTypesInLaw).

%for_all(ProtoObject,Relation):-
%  ProtoObject = object(_ID,Type,Properties),
%  forall(ProtoObject, Relation).
for_all(ProtoObject,Relation):-
  ProtoObject = object_prototype(_ID,Type,Properties),
  forall(ProtoObject, Relation).

law_element(Element):-true.

apply_law(Law):-true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Support predicates %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Converts a list into a tuple. Example: [1,2,3] --> (1,2,3)
% ?- list_to_tuple([1,2,3],Tuple).
% Tuple =  (1, 2, 3) .
list_to_tuple([A,B|L], (A,R)) :- list_to_tuple([B|L], R).
list_to_tuple([A,B], (A,B)).
list_to_tuple([A],(A)).

% Builds a list of variables. Example:
% ?- numbered_variable_list(3,ListOfVariables).
% ListOfVariables = ['Variable1', 'Variable2', 'Variable3'].
numbered_variable_list(Number,ListOfVariables):-
  findall(Counter, between(1,Number,Counter),Numbers),
  maplist(atom_concat('Variable'),Numbers,ListOfVariables).
