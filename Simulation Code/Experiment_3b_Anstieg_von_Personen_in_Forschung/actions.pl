%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% This module contains the action defintion
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(actions,
	[
	data/7,
	get_interest/2,
	perform_actions/0,
	plan/2,
	plan_actions/0,
	plan_action/1,
	perform_action/1,
	publication/6,
	series/2,
	find_independent_data_series/2,
	theory/6 ]).

:- use_module('action_definition.pl').
%:- use_module('agents.pl').

:- dynamic([publication/6, action_que/2, data/7, log/6, model/3, pot_model/3, theory/6]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Basic functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

find_valid_actions_for_agent(Agent,Actions):-
	agent(Type, Agent, _Attributes),
	findall(Action, ( action_type(Action,Types), member(Type,Types) ), Actions).

add_action_to_que(AgentID, Action):- 
	sim_time(Tick),
	ActionContent = execute_action(Action,AgentID,Tick),
	agent(Type, AgentID, Attributes),
	ActionQue = Attributes.get(action_que),
	append([ActionContent],ActionQue,NewActionQue),
	update_attributes(AgentID,action_que,NewActionQue).

get_interest(AgentID, EventType):-
	agent(Type,AgentID, Attributes),
	Interests = Attributes.get(list_of_interests),
	random_member(EventType,Interests).

dump_all_data(AgentID):-
	forall(actions:data(DataID,AgentID,Tick,observation_data,Location,EventID), 
			retract(actions:data(DataID,AgentID,Tick,observation_data, BaseType, Location,EventID) )).

archive_data(Data):-
	Data = actions:data(DataID,Agent1,Tick1,observation_data, BaseType, Location,EventIDs1),
	retract(actions:data(DataID,Agent1,Tick1,observation_data, BaseType, Location,EventIDs1)),
	assertz(actions:data(DataID,Agent1,Tick1,archive_data, BaseType, Location,EventIDs1)).

% Find types for given list of event IDs
find_matching_types(ListEventIDs,EventTypes):-
	findall(EventType,find_matching_type(ListEventIDs,EventType),EventTypes).

find_matching_type(ListEventIDs,EventType):-
	member(EventID,ListEventIDs), 
	create_world_module:event(EventID,EventType,_,_).

% Find [types, location] for given list of event IDs
find_matching_typeslocations(ListEventIDs, Base, EventTypesLocations):-
	findall(TypeLocation,find_matching_typelocation(ListEventIDs, Base,TypeLocation),EventTypesLocations).

find_matching_typelocation(ListEventIDs,Base, [EventType,RelativLocation]):-
	member(EventID,ListEventIDs), 
	create_world_module:event(EventID,EventType,_,Location),
	absolut_to_relativ_location(Location,Base, RelativLocation).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Plan actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Strategy for selection actions in respect to the agent type
% Two options: random action or with strategy 

plan_actions:-
	list_of_active_agents(ListOfActiveAgentIDs),
	forall( member(AgentID,ListOfActiveAgentIDs), plan_action(AgentID) ).


plan_action(AgentID):-
%	msg_with_simple_name(AgentID,'plans a action'),
	write_diary(AgentID, "plan_action"),
	(action_planing(random)   *-> random_action(AgentID);
	action_planing(strategy) *-> strategic_action(AgentID)).

%plan_action(AgentID):-strategic_action(AgentID).

plan_random_action_for_agent(AgentID):-
	find_valid_actions_for_agent(AgentID,List_Of_Actions),
	random_member(Action,List_Of_Actions),
	add_action_to_que(AgentID, Action).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action strategy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Defines the strategy of each agent type

strategic_action(AgentID):- 
%	debug_actions(AgentID).
	agent(Type,AgentID,_Dict),
	strategic_action(AgentID,Type).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Strategy student
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

strategic_action(AgentID,student):-
	graduation(AgentID)*->
		change_type(AgentID, researcher);
	(	publication(_A,_B,_C,_D,_E,_F)*->
		plan(read_publication, AgentID); 	
		msg_with_simple_name(AgentID,'nothing to learn')
		).

change_type(AgentID, NewType):-
	msg_with_simple_name(AgentID,'changes Type'),
	agents:agent(Type,AgentID,Dict),
	retractall(agents:agent(Type,AgentID,Dict)),
	asserta(agents:agent(NewType,AgentID,Dict)).

graduation(AgentID):-
	agent_age(AgentID,Age), 
	random(4,7,Threshold),
	Age > Threshold.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Strategy Researcher
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

strategic_action(AgentID,researcher):-
	sufficient_bel_for_publish(AgentID)*->
	plan(publish,AgentID);
	(	plan(analyze_data, AgentID),
		%plan(create_theory_type2,AgentID),
		%plan(validation_type1, AgentID),
		%plan(read_related_publication,AgentID),
		plan(experiment_series,AgentID)	).
% execute_action(validation_experiment, AgentID, _Options)

sufficient_bel_for_publish(AgentID):-
	findall(Theory, 
	( actions:belief(AgentID, Tick, theory, Theory, Domain, Str), Str >= 0.5 ), 
		List ), 
	length(List, Len), Len >1.

/*
	enough_resources(AgentID, 20)->(
		%plan(reflect_knowledgebase,AgentID);
		plan(experiment_series,AgentID),
		plan(make_theory,AgentID),
		plan(publish,AgentID)
		); 
	(	plan(proposal, AgentID),
		plan(funding, AgentID)).
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Strategy funder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

strategic_action(AgentID,funder):- true. 
%	publication(_A,_B,_C,_D,_E,_F)*->
%	plan(read_publication, AgentID); true.
	% plan(reflect_knowledge, AgentID).

% Gibt es genug Wissen?
% Gibt es genug Geld?
% Mach ein 

% enough budget

enough_resources(AgentID, Threshold):-
	agent(_Type,AgentID,Dict),
	Res = Dict.get(resources),
	Res > Threshold.

% A action for debug and testing
debug_actions(AgentID):-
	list_of_active_agents(ListOfActiveAgentIDs),
	sim_time(Tick),
	%assertz(actions:debugcall(Tick, ListOfActiveAgentIDs, AgentID)),
	add_action_to_que(AgentID, debug).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prepare planed actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plan and prepare actions and add them to the que of the agents

plan(validation_type1, AgentID):- add_action_to_que(AgentID,validation_type1).

plan(funding, AgentID):- add_action_to_que(AgentID, funding).

plan(publish, AgentID):- add_action_to_que(AgentID, publish).

plan(create_theory_type1, AgentID):- add_action_to_que(AgentID, create_theory_type1).

plan(observation_types, AgentID):- add_action_to_que(AgentID, observation_types).

plan(publish_types, AgentID):- add_action_to_que(AgentID, publish_types).

plan(select_new_interest, AgentID):- add_action_to_que(AgentID, select_new_interest).

plan(analyze_data, AgentID):-
	find_independent_data_series(AgentID,SeriesList) *-> 
		plan(create_theory_type2,AgentID);
		plan(create_theory_type1,AgentID).

plan(funding, AgentID):- add_action_to_que(AgentID, funding).

plan(read_related_publication, AgentID):- true. 

plan(read_publication, AgentID):- true.
	% add_action_to_que(AgentID, read_publication).

plan(create_theory_type2,AgentID):-add_action_to_que(AgentID, create_theory_type2).

plan(experiment_series,AgentID):-
	msg_with_simple_name(AgentID,'plan: experiment_series'),
	random(2,5,Random),
	(	between(0,Random,C),
		add_action_to_que(AgentID, observation_of_events),
		fail;true ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% data seriece and quality
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Finds data serices (observations of the same location in t and t+1)

find_independent_data_series(AgentID,SeriesList):-
	find_data_series(AgentID,SeriesList),
	length(SeriesList,LenSL),
	LenSL >2.

find_data_series(AgentID,SeriesList):-
	%msg_with_simple_name(AgentID,'check_data_series'),
	agent(_T,AgentID,_),
	findall([BaseType,Series],series(AgentID, BaseType, Series), SeriesList),
	SeriesList \= [].

series(AgentID, BaseType, Series):-
	actions:data(DataID1,AgentID,Tick1,observation_data,BaseType,Location,_EventIDs1), 
	actions:data(DataID2,AgentID,Tick2,observation_data,BaseType,Location,_EventIDs2),
	Tick2 is Tick1 + 1,
	DataID1 \= DataID2,
	Series = [DataID1, DataID2].


check_data_quantity(AgentID):-
	%msg_with_simple_name(AgentID,'plan: check_data_quantity'),
	findall(Tick, data(DataID,AgentID, Tick, observation_data, BaseType, Location, Observation), DataTimestamps),
	length(DataTimestamps, LenTimestamps),
	LenTimestamps > 500, 
	writeln('... Quantity ok for analysis'),
	format('Länge Datasets: ~w\n', [LenTimestamps]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Perform actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

perform_actions:-
	%	message(apply_actions,_),
		list_of_active_agents(ListOfActiveAgents),
		length(ListOfActiveAgents, Len), writeln(Len),
		forall(member(Agent,ListOfActiveAgents), perform_action(Agent)).
	
	perform_action(AgentID):- 
		

		msg_with_simple_name(AgentID,'performs an action'),
		agent(Type,AgentID, Attributes),
		ActionQue = Attributes.get(action_que),
		perform_action(AgentID,ActionQue).
	
	% Que is empty -> plan new action
	perform_action(AgentID,[]):- 
		msg_with_simple_name(AgentID,'empty que'),
		execute_action(plan_actions,AgentID,_Options).
	
	% Performs first action from que
	perform_action(AgentID, Que):-
		Que \= [],
		Que = [Action|Rest], 
		msg_with_simple_name(AgentID,'has something in que'),
		agent(Type,AgentID, Attributes), 
		Action =..[_,Act,_,_],
		price(Act, Price),
		agent_resources(AgentID, Resources),
		( Price < Resources ->
		perform_action(AgentID, Que, Price, resources);
		perform_action(AgentID, Que, Price, no_resources) ).

perform_action(AgentID, Que, Price, resources):-
	Que = [Action|Rest], 
		update_attributes(AgentID,action_que,Rest),
		msg_with_simple_name(AgentID,Action),
		Action =..[_,Act,_,_],
			( spend_resources(AgentID,Price),
				(	write_diary(AgentID, Act), call(Action) ) ; 
				(	format('Error while performing an action: ~w\n', [Action]), 
					concat("Error :",Act,EAct), write_diary(AgentID, EAct) )).

perform_action(AgentID, Que, Price, no_resources):-	
	Que = [Action|Rest], 
	format('Not enough resources for ~w with price ~w\n', [Action, Price]),
	update_attributes(AgentID,action_que,[]),
	plan(funding, AgentID).


price(Action, Price):-
	Action = 'funding'-> Price = 0;
	random(1,3,Price).

check_resources(AgentID, Price):-
	agent(Type,AgentID, Attributes),
	Resources = Attributes.get(resources),
	Resources >= Price.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Action type definitions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

action_type(make_publication,[student,researcher]).
action_type(make_theory_type1,[researcher]).
action_type(experiment,[researcher]).
action_type(studying,[student]).
action_type(funding,[researcher,funder]).
action_type(idle,[student,researcher,funder]).
action_type(plan_actions, [student,researcher,funder]).
action_type(reflect_knowledge, [student,researcher,funder]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Alternative definitions for increased multicore support 
%%% Attention: Can lead to instability in bigger simulation experiments 
%%% due to complex thread syncronisation. Handle with care!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Parallel multicore version (might be instable with bigger runs)
/*
plan_actions:-
	cores(NumberOfCores),
	message(apply_actions,_),
	findall(Goal,plan_actions_concurrent(Goal),Goals),
	concurrent(NumberOfCores, Goals, []).

plan_actions_concurrent(Goal):-
	list_of_active_agents(ListOfActiveAgentIDs),
	member(AgentID,ListOfActiveAgentIDs),
	Goal = plan_action(AgentID).
*/

































