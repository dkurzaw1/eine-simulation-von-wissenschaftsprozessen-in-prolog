%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Module create_world %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This module creats a world, consisting of objects, relations and laws.
% Objects can have properties and be in relation to each other.
% Laws defining the progress of the world over time.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% For creating a visualisation of the module structure
% :- use_module(library(callgraph)).
% and call:
% module_dotpdf(user,[method(unflatten([fl(4),c(4)]))]).

% CLP(FD): Constraint Logic Programming over Finite Domains
% Has to be loaded from main module
% :- use_module(library(clpfd)).

:- module(create_world_module,
  [create_world/0, remove_world/0, update_world/0,
    object/1, type/2, property/2, property/3]).

:- dynamic( [create_world/0, object/1, object/3, type/2, property/2, property/3, registry/3] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Constants - primary for create_world module %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Creates a list of types for convenience and readablity of code.
list_of_types(List):-
  findall(type(ID,Content),type(ID,Content), List).

list_of_properties(List):-
  findall(property(ID,Property), object_property(ID,Property), List).

list_of_propertyIDs(List):-
  findall(ID, object_property(ID,_Property), List).

list_of_object_prototypes(List):-
  findall(object_prototype(ID, Type, ListOfProperties),
          object_prototype(ID, Type, ListOfProperties),List).

list_of_relation_prototypes(List):-
  findall(relation_prototype(ID, ListOfObjects),
          relation_prototype(ID, ListOfObjects),List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% manage world %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a world with objects, relations and laws
create_world:-
  create_properties,
  create_object_types,
  create_object_prototypes,
  create_objects,
  create_relations.
  create_laws.


% update world (call once in each tick)
update_world:-
  apply_laws,
  update_types,
  update_relations.

% remove a world from memory
remove_world:-
  remove_relations,
  retractall(registry(_,_,_)),
  retractall(object_prototype(_,_,_)),
  retractall(type(_,_)),
  retractall(property(_,_)).

% Arity of relations is dynamic
% remove_relations ensures, that all relations will removed from memory
remove_relations:-
    relation_arity(MaxArity),
    FullArity is MaxArity + 3, % Arity + ID etc. 
    ( between(0,FullArity,Arity), 
      abolish(create_world_module:relation/Arity)
    ), fail;true. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% managing components %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_properties:-
  number_of_properties(NumberOfProperties),
  ( between(1,NumberOfProperties,_Counter),
    create_property(_),fail;true ).

create_object_types:-
  number_of_object_types(NumberOfObjectTypes),
  ( between(1,NumberOfObjectTypes,_Counter),
    create_object_type(_),fail;true ).

create_object_prototypes:-
  number_of_proto_objects(NumberOfProtoObjects),
  ( between(1,NumberOfProtoObjects,_Counter),
    create_object_prototype(_),fail;true ).

% :-  thread_pool_create(media, 20000, []).
create_objects:-
  number_of_objects(NumberOfObjects),
  ( between(1,NumberOfObjects,_Counter),
  %thread_create_in_pool(media, create_object(_), _Id, [stack_limit(2 000 000 000 000)]) ,fail;true ).
  %thread_create(create_object(_), _Id, [stack_limit(2 0 000 000)]) ,fail;true ).
    create_object(_),fail;true ).

create_relations:-
  number_of_relations(NumberOfRelations),
  ( between(1,NumberOfRelations,_Counter),
    create_relation(_),fail;true ).

create_laws:-
  number_of_laws(NumberOfLaws),
  ( between(1,NumberOfLaws,_Counter),
    create_law(_),fail;true ).

apply_laws:-
  % für alle Regeln ...
  apply_law(Law).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create actual objects and dependencies %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Create an object of random type %%%
create_object(Object):-
  list_of_object_prototypes(Protolist),
  random_member(ProtoObject, Protolist),
  ProtoObject = object_prototype(ProtoObjectID, TypeID, _ValueList),
  create_specific_object(ProtoObjectID,Object).

%%% Create an object of with specific type %%%
% object(ObjectID, Properties)
% Example: object( 232(...), Properties (...))
% Creates a concrete object from a protoobject.
create_specific_object(PrototypID,Object):-
  object_prototype( PrototypID, ObjectTypeID, PropertyIDList ),
  uuid(ObjectID),
  forall(
    member(PropertyID,PropertyIDList),
    define_properties(ObjectID, PropertyID)),
%%% OLD STYLE OF OBJECT
  Object = object(ObjectID),
  asserta(Object),

%%%%%%%% OBJEKT BRAUCHT EIGENSCHAFTEN! %%%%%%%%%%%%
    % EIGENSCHAFTEN IN OBJEKT?
  ObjectNew = object(ObjectID, PropertyIDList),
  asserta(ObjectNew).

%%% Creates properties for a given object %%%
define_properties(ObjectID, PropertyID):-
  object_property(PropertyID, [Low, High]),
  random(Low, High, Value),
  Property = object_property(ObjectID, PropertyID, Value),
  asserta(Property).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% create prototypes %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Create an object prototype %%%
% object_prototype( ID, TID, [property('a12(...)', [default_value]), property('a12(...)', [default_value])] ).
% Example: object_prototype( 321(...), 813(...), (...) )
% Object types determine the type of an object
% by assigning a set of properties to it.
create_object_prototype(ObjectPrototype):-
  uuid(UniqueObjectID),
  list_of_types(Types),
  random_member(Type,Types),
  Type = type(TypeID,ListOfProperties),
  ObjectPrototype = object_prototype( UniqueObjectID, TypeID, ListOfProperties ),
  asserta(ObjectPrototype).

%%% Create an object type %%%
% type(ID,SelectionOfProperties).
% Example: type('814(...)', [property('a12(...)', [default_value]), property('a12(...)', [default_value])]).
% Object types determine the type of an object
% by assigning a set of properties to it.
create_object_type(Type):-
  uuid(ID),
  maximal_properties_per_type(MaxProperties),
  random_between(1,MaxProperties,NumberOfProperties),
  list_of_propertyIDs(ListOfPropertyIDs),
  % ... build a random sublist for $SelectionOfProperties
  findall( Property ,
            ( between(1,NumberOfProperties,_Counter),
              random_member(Property, ListOfPropertyIDs) ),
           SelectionOfProperties ),
  Type = type(ID,SelectionOfProperties),
  asserta(Type).

%%% Create a porperty %%%
% property(ID, Domain)
% Example: property('5baea2de-0ac1-11e8-b1a9-ab7a8c0e46b0', [23,123]).
% This predicate represents properties that define object types.
create_property(Property):-
  uuid(ID),
  % Value is empty and can be used by laws and objects
  create_domain(Domain),
  Property = object_property(ID,Domain),
  asserta(Property).

% Creates a domain for a property
create_domain(Domain):-
  random(0,100,Low),
  random(100,200,High),
  Domain = [Low,High].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create relations %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%relation(ID, Object1,...,ObjectN)

%object_property(PropertyID, [Low, High]),
%random(Low, High, Value),
%Property = object_property(ObjectID, PropertyID, Value),

create_relation(Relation):-
  objecttypes_in_relation(ListOfObjectTypesInRelation),
  uuid(ID),
  %append([relation,ID],ListOfTypesInRelation,RelationList),
  %Relation =.. RelationList,
  append([relation,ID],ListOfObjectTypesInRelation,ListOfObjectTypesInRelationID),
  %list_to_tuple(ListOfObjectTypesInRelationID,ToupleObjectRelationID),
  %RelationList = relation(ToupleObjectRelationID),
  Relation =.. ListOfObjectTypesInRelationID,
  functor(Relation, Functor , Arity), 
  dynamic([relation/Arity]),
  asserta(registry(Functor,ID, Arity)),
  asserta(Relation).

objecttypes_in_relation(ListOfObjectTypesInRelation):-
  relation_arity(MaxArity),
  random_between(2,MaxArity,Arity),
  list_of_propertyIDs(ListOfPropertyIDs),
  findall(Res,
    ( between(1,Arity,_Counter),
      random_member(PropertyID,ListOfPropertyIDs),
      object_property(PropertyID, [Low, High]),
      random(Low, High, Value),
      Res = object_property(OID, PropertyID,Value)
    ),
    ListOfObjectTypesInRelation).

find_objects_by_type(TypeID,ListOfObjects):-
  findall(ObjectID,
            ( object(ObjectID),
              object_property(ObjectID, PropertyID, Value)),
          ListOfObjects ).

% Takes a relation id and returns property IDs of objects in this relation
relation_instantiation_tuple(RelationID,[InstanceID1,InstanceID2]):-
    relation(RelationID, ObjectPropA, ObjectPropB), % Relation about two Properties
    ObjectPropA=..[NameA,_,ID1,PROP1],  % Deconstruct Property (workaround for a Prolog bug)
    object_property(InstanceID1,ID1,PROP1), % Call a instanciation this property
    ObjectPropB=..[NameB,_,ID2,PROP2], % Deconstruct Property (workaround for a Prolog bug)
    object_property(InstanceID2,ID2,PROP2). % Call a instanciation this property

reassign_types:- true.
reassign_relations:- true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% create laws %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% relation(ID, object_prototype(ID,Type,Prop), ... , object_prototype(ID,Type,Prop) )
% law(ListOfObjects,Relations):- ???

create_law(Law):-
    create_potential_law(PL),
    create_goal(Goal),
    uuid(LawID),
    register_law(LawID, PL, Goal).


create_potential_law(PL):-
    % Find all relations
    findall(ID,registry(relation,ID, Arity), List_Of_all_Relations),
    % Select Relations
    select_relations(List_Of_all_Relations, Selection_Of_Relations),
    
    % find_properties(Selection_Of_Relations)
    % multi_intersection

    member(Selection_Of_Relations,Relation),
    relation(Relation).





/* 
Lösung:
Mache eine intersection aus allen Objekten, die in allen Relationen liegen. 
Die können dann entsprechend der drei Arten variiert werden - fertig!
**/ 
    % PROBLEM
    % GGF über Registry lösen?
    /**
    Jedee relation kommt mit vollem Aufruf in Registry
    z.B:
    relation('1f8f1524-46a4-11ea-a92b-dfc36ee473fc',
                             object_property(_,
                                             '1f84ee50-46a4-11ea-9f39-1bc7a61c1ef7',
                                             103),
                             object_property(_,
                                             '1f84ec66-46a4-11ea-82e9-afba50f22443',
                                             100),
                             object_property(_,
                                             '1f831666-46a4-11ea-b1d7-ef2929f2d0be',
                                             93)).

    **/
    % por_law:- r1(..), r2(...)
    % Find effected object types



select_relations(List_Of_Relations, Selection_Of_Relations):-
  law_complexity(Complexity),
  random(1,MaxComplexity,Complexity),
  random_sublist(MaxComplexity, List_Of_Relations, Selection_Of_Relations).


apply_laws:- 
    true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Support predicates %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Converts a list into a tuple. Example: [1,2,3] --> (1,2,3)
% ?- list_to_tuple([1,2,3],Tuple).
% Tuple =  (1, 2, 3) .
list_to_tuple([A,B|L], (A,R)) :- list_to_tuple([B|L], R).
list_to_tuple([A,B], (A,B)).
list_to_tuple([A],(A)).

% Builds a list of variables. Example:
% ?- numbered_variable_list(3,ListOfVariables).
% ListOfVariables = ['Variable1', 'Variable2', 'Variable3'].
numbered_variable_list(Number,ListOfVariables):-
  findall(Counter, between(1,Number,Counter),Numbers),
  maplist(atom_concat('Variable'),Numbers,ListOfVariables).
