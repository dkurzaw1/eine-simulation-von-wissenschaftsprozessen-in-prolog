%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Utilitis %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(utils,[
  agent_age/2,
  agent_resources/2,
  agent_reputation/2,
  find_all_beliefs_for_AgentID/2,
  check_configuration/0,
  checking_results_dir/0,
  cls/0,
  create_dir/1,
  distribution_of_elements/2,
  export_law_visualisation/0,
  find_all_actions/1,
  find_all_agents/1,
  find_all_facts/1,
  id/2,
  in_range/2,
  in_extended_range/2,
  length_of_que/2,
  list_of_active_agents/1,
  list_of_inactive_agents/1,
  log/6,
  addlog/4,
  monitor_status/0,
  multi_intersection/2,
  multi_append/2,
  neighborevents/2,
  number_of_events_in_tick/2,
  nextto/2,
  prepare_folder/0,
  prop_call/2,
  random_location/2,
  random_relative_location/1,
  random_simple_name/1,
  random_sublist/3,
  read_name_file/0,
  start_monitoring/0,
  update_range/0,
  update_sim_time/1,
  relative_to_absolut_location/3,
  absolut_to_relativ_location/3,
  remove_list/3,
  simple_name_to_id/2,
  shape/2,
  spend_resources/2,
  startup_time/6,
  sublist/2,
  switch_dir/1,
  update_attributes/3,
  update_list_of_active_agents/0,
  update_number_of_ticks/3,
  write_tick_results/0]).

:-dynamic([log/6, list_of_active_agents/1, events_in_range/3]).
%%% Choose a random element from a given list %%%
% KANN GEGEN random_member(X, From) getauscht werden!!!
%random_element_from_list([], []).
%random_element_from_list(List,Random_Element):-
%	length(List, Length),
%	random(0, Length, Index),
%	nth0(Index, List, Random_Element).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Math operations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mean_value(List,MeanValue):-
    sum_list(List,Sum),
    length(List,Length),
    MeanValue is div(Sum,Length).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% List operations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Returns a random subset of given list
% random_sublist(+Number_of_random_elements, +list, -Random_sublist).
random_sublist(0, _, []).
random_sublist(Count, List, [X|SelectedFromRemaining]) :-
  random_member(X, List),
  select(X, List, RemainingList),
  C1 is Count - 1,
  random_sublist(C1, RemainingList, SelectedFromRemaining).

% Returns the intersection about n lists
% multi_intersection(+[[List1],[List2],[List_n]],-Intersection).
multi_intersection([L1|R],Result):-
  multi_intersection(L1,R,Result).

multi_intersection(Result,[],Result):-!.

multi_intersection(L1,[L2|R],Result):-
  intersection(L1,L2, NResult),
  multi_intersection(NResult,R, Result).

% Returns the union about n lists
% multi_union(+[[List1],[List2],[List_n]],-Union).
multi_append([L1|R],Result):-
  multi_append(L1,R,Result).

multi_append(Result,[],Result):-!.

multi_append(L1,[L2|R],Result):-
  append(L1,L2, NResult),
  multi_append(NResult,R, Result).

distribution_of_elements(List, Distribution):-
  findall([Number, Element], countall(List,Element,Number), OCList),
  length(List, LenList),
  findall([OccurrenceCount, Percentage, BaseElement], 
    ( member([OccurrenceCount, BaseElement],OCList),
      Percentage  is  100/LenList*OccurrenceCount ), Distribution).


% List = [a,b,a,b], distribution_of_elements(List,Occurrence), length(List,LenList), findall(Percentage, ( member([ON,_],Occurrence), Percentage  is  100/LenList*ON), Sol).

% List = [a,b,a,b], distribution_of_elements(List,Occurrence), length(List,LenList), findall([ON, Percentage,Element], ( member([ON,Element],Occurrence), Percentage  is  100/LenList*ON), Sol).

count([],X,0).
count([X|T],X,Y):- count(T,X,Z), Y is 1+Z.
count([X1|T],X,Z):- X1\=X,count(T,X,Z).

countall(List,X,C) :-
    sort(List,List1),
    member(X,List1),
    count(List,X,C).



% Checks if is X sublist of Y
sublist( [], _ ).
sublist( [X|XS], [X|XSS] ) :- sublist( XS, XSS ).
sublist( [X|XS], [_|XSS] ) :- sublist( [X|XS], XSS ).

remove_list(L, [], L):- !.
remove_list([X|Tail], [X|Rest], Result):- !, remove_list(Tail, Rest, Result).
remove_list([X|Tail], L2, [X|Result]):- remove_list(Tail, L2, Result).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Dynamic listings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

find_all_actions(List_of_available_actions):-
	findall(Actions,action(Action,Agent,Tick),List_of_available_actions).

find_all_agents(List_of_available_agents):-
	findall(ID,agent(Type,ID,List_of_Attributes),List_of_available_agents).

find_all_beliefs_for_AgentID(BelList,AgentID):-
  sim_time(Tick),
  findall( belief(Agent, Tick, Type, BeliefContent, Domain, Degree) , 
  belief(Agent, Tick, Type, BeliefContent, Domain, Degree), BelList).

find_all_facts(List_of_facts_in_World):-
	findall(ID,fact(ID),List_of_facts_in_World).

update_list_of_active_agents:-
  findall(AgentID,(agent(Type,AgentID,Prop),not(IA=Prop.get(inactive))),ListOfActiveAgents),
  retractall(list_of_active_agents(_)),
  asserta(list_of_active_agents(ListOfActiveAgents)).

list_of_inactive_agents(ListOfInactiveAgents):-
  findall(AgentID,(agent(Type,AgentID,Prop),(IA=Prop.get(inactive))),ListOfInactiveAgents).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Length of specific lists
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

length_of_que(AgentID,LOQ):-
		agent(Type,AgentID, Attributes),
		Que = Attributes.get(action_que),
		length(Que, LOQ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update agents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% reduce ressources of given agent by spcefied amount
spend_resources(AgentID,ResourcesToSpend):-
	agent(Type,AgentID, Attributes),
	Resources = Attributes.get(resources),
	NewResources is Resources - ResourcesToSpend,
	update_attributes(AgentID,resources,NewResources).

update_attributes(AgentID,Selection,NewValue):-

	agent(Type,AgentID, Attributes),
	retract( agents:agent(Type, AgentID, Attributes) ),
	assertz( agents:agent(Type, AgentID, Attributes.put(Selection,NewValue)) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agent statistics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

agent_age(AgentID,Age):-
  sim_time(Tick),
	agent(Type,AgentID,Properties),
	Created = Properties.get(created),
	Age is Tick - Created.

agent_resources(Agent,Resources):-
	agent(Type,Agent,Properties),
	Resources = Properties.get(resources).

agent_reputation(Agent, Reputation):-
    agent(Type,Agent,Properties),
    Reputation = Properties.get(reputation).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Screen update
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% cls = clear screen for clean output
cls :- write('\e[H\e[2J').

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Date and time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

startup_time(Year,Month,Day,Hour,Minute,Second):-
	get_time(X),
	stamp_date_time(X,TimeString,localc),
	TimeString =..[_,Year,Month,Day,Hour,Minute,Second,_,_,_].

%! update_number_of_ticks(NumberOfTicks,NumberOfTicksPreRun,NewNumberOfTicks) is nondet
% Add the number of prerun ticks to the maximum of simulation ticks
update_number_of_ticks(NumberOfTicks,NumberOfTicksPrerun,NewNumberOfTicks):-
	retractall(configuration:number_of_ticks(NumberOfTicks)),
	NewNumberOfTicks is NumberOfTicks + NumberOfTicksPrerun,
	asserta(configuration:number_of_ticks(NumberOfTicks)).

%! update_sim_time(NewTime) is nondet
% Updates the internal simulation time. 
update_sim_time(NewTime):-
	retractall(sim_time(_)),
	assert(sim_time(NewTime)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation - File Output %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% write tick results to file %%%
write_tick_results:-
	output_filename(Filename),
	open(Filename,write,Stream),
	write(Stream, "Test"),
	close(Stream).

% Check if results dir exists
prepare_folder:-
  working_directory(SimDir,SimDir),
  asserta(sim_dir(SimDir)),
	checking_results_dir,
	create_dir(experiment).

% Create folder for result files
checking_results_dir:-
	exists_directory('./results');
	make_directory('./results').

% Create folder for running experiment
create_dir(experiment):-
	experiment_id(Experiment_id),
	concat('./results/',Experiment_id,ExDir),
	asserta(experiment_dir(ExDir)),
	make_directory(ExDir).

% Create folder for single runs
create_dir(run):-
  experiment_id(ExperimentID),
	run_id(RunID),
  format(atom(RunDir),'./results/~w/~w',[ExperimentID, RunID]),
	asserta(run_dir(RunDir)),
	make_directory(RunDir).

create_dir(diary):-
  experiment_id(ExperimentID),
	run_id(RunID),
  format(atom(Diary),'./results/~w/~w/~w',[ExperimentID, RunID,'diary']),
	asserta(diary_dir(Diary)),
	make_directory(Diary).

switch_dir(diary):-
  diary_dir(Diary),
  simulation:working_directory(_, Diary).

switch_dir(run):-
  run_dir(RunDir),
  simulation:working_directory(_, RunDir).

switch_dir(simulation):-
  sim_dir(SimDir),
  simulation:working_directory(_,SimDir).

switch_dir(experiment):-
  experiment_dir(ExDir),
  simulation:working_directory(_,ExDir).

addlog(AgentID, Type, Content, Value):-
  sim_time(Tick),
  run_id(RunID),
  assertz(log(RunID, Tick, AgentID, Type, Content, Value)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Monitoring with influxDB and Graphana %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Optional: Enables live monitoring with influxDB and Grafana
monitor_status:-
%	thread_pool_property(main,backlog(B)),
%	writeln(B),
	find_all_agents(Agents),
	length(Agents, Number_Of_Agents),
	atom_concat('simulation,run=ID,tick=1 agents=',Number_Of_Agents,Atom),
	http_post( [protocol(http),
		host('192.168.0.11'),
		port(8086),
		path('/write?db=mydb')],
		atom(Atom),
		_R, []).


%! start_monitoring is nondet
% Creates a thread for live monitoring with influxdb
start_monitoring:-
	thread_create_in_pool(main, monitor_status, _ID, [stack_limit(2 000 000 000)] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation - Basic failure tests %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


check_configuration:-
	check_file_names,
	check_action_planing.

check_file_names:-
	true.
/*
	( output_filename('results.csv'),
	  output_subdirectory('results') ) ;
	writeln('*** RESULT FILE NOT SET ***').
*/

check_action_planing:- true.
/*
	action_planing(random),
	action_planing(strategy),
	retract_all(action_planing(strategy)),
	writeln('*** CONFLICT IN CONFIG: RANDOM AND DEFINED AGENT STRATEGY - WILL USE RANDOM DEFINITION ***').
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Events - Shapes, locations, ect. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


random_location(X,Y):-
	universe_bounders_2d(MaxX,MaxY),
	random(0,MaxX,X),
	random(0,MaxY,Y).

draw_events:-
	sim_time(Tick),
	universe_bounders_2d(MaxX,MaxY),
	between(0,MaxX,C1),write('\n'),format('X~|~`0t~w~5| | ', C1),
	   ( between(0,MaxY,C2), 
			   (draw_event(Tick,C1,C2) )
			   ),fail;true.

draw_event(Tick, X,Y):-
	create_world_module:event(_ID, _Type, Tick, [X, Y]) -> write('O');write('+').

export_law_visualisation:-
    findall([A,B], lawtree(A,B), Chain),
    run_id(Run_ID),
    switch_dir(run),
    open("lawtree_visualisation.csv",write,Stream),
    csv_write_stream(Stream, [ row('Source', 'Target') ], [] ),
    forall(member(Pair,Chain),       
            ( Pair = [A,B], csv_write_stream(Stream, [ row(A, B) ], [] )) ),
    close(Stream),
    switch_dir(simulation).

%%%% neighborevents %%%
in_range(Base,List):-
    Base = [X,Y],
    MX is X-1,
    PX is X+1,
    PY is Y+1,
    MY is Y-1,
    List = [ [MX,MY], [MX,Y], [MX, PY],
             [X,MY], [X,Y], [X, PY],
             [PX,MY], [PX,Y], [PX, PY] ].

in_extended_range(Base,List):-
  Base = [BX,BY],
  LowX is BX -2, 
  LowY is BY -2, 
  HX is BX +2, 
  HY is BY +2,
  findall([X,Y],(between(LowX,HX,X), between(LowY,HY,Y)),List).

nextto(ID1,ID2):-
  create_world_module:event(ID1,Type1,Tick,C1),
  create_world_module:event(ID2,Type2,Tick,C2),
  in_range(C1,Range1),
  sublist([C2],Range1).

neighborevents(ID,Neighborevents):-
    findall(ID2, nextto(ID,ID2),Neighborevents ).

/*
update_range:-
  %retractall(utils:events_in_range/3),
  sim_time(Tick),
  writeln('... update range'),
  forall(create_world_module:event(ID1,Type1,Tick,C1), 
          (in_range(C1,Range),
          assertz( events_in_range(C1,Range,Tick) ) ) ).
*/

/* NICHT PARALLELE VERSION
update_range:-
  %retractall(utils:events_in_range/3),
  sim_time(Tick),
  writeln('... update range'),
  forall(create_world_module:event(ID1,Type1,Tick,C1), 
          (in_range(C1,Range),
          assertz( events_in_range(C1,Range,Tick) ) ) ).
*/

update_range.

/*
update_range:-
  writeln('... update range list'),
  %retractall(utils:events_in_range/3),
  sim_time(Tick),
  retractall(utils:events_in_range(_,_,_)),
  findall([ID1,Tick,Location],create_world_module:event(ID1,Type1,Tick,Location), EventList),
  concurrent_maplist(update_range_events, EventList),
  %forall(member(EV, EventList), update_range_events(EV)),
  writeln('... finish update range list').


update_range_events(Event):-
  Event = [ID1,Tick,Location1], 
  in_range(Location1,Range), 
  findall(ID2, (member(Location, Range), create_world_module:event(ID2, Type2, Tick, Location2)), List ),
  assertz( utils:events_in_range(ID1,List,Tick) ).
*/

%%%% draw shape to screen (debug) %%%
shape(Base,Shape):-
    Base = [X,Y],
    XLeft is X-1,
    XRight is X+1,
    YUp is Y-1,
    YDown is Y+1,
    between(XLeft, XRight, C1),
    write('\n'),
    (   between(YUp, YDown, C2),
        (   random_between(0, 9, R),
            random_type(Type),
            NCor = [C1,C2],
            (NCor\=Base->
            write(NCor);write(Base)),
            write(Type)
        )
    ),fail;true.

number_of_events_in_tick(Tick, NumberEvents):-
  findall(ID1,create_world_module:event(ID1,Type1,Tick,C1),List),
  length(List,NumberEvents).

%%% location conversion %%%
relative_to_absolut_location(RelativeLocation,BaseLocation,Location):-
    BaseLocation = [BaseX,BaseY],
    RelativeLocation = [RelativeX, RelativeY],
    TargetX is BaseX + RelativeX,
    TargetY is BaseY + RelativeY,
    Location = [TargetX,TargetY].

absolut_to_relativ_location(Location, BaseLocation , RelativLocation):-
  BaseLocation = [BaseX, BaseY],
  Location = [LocationX, LocationY],
  RelativX is  LocationX - BaseX,
  RelativY is  LocationY - BaseY, 
  RelativLocation = [RelativX,RelativY].
  
  

random_relative_location(Location):-
    random(-1,2,X),
    random(-1,2,Y),
    Location = [X,Y].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Naming %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

id(Naming,ID):-
  uuid(UUID),
  concat(Naming,UUID,ID).

read_name_file:-
  csv_read_file('names.txt', Rows, [functor(simple_name), arity(1)]),
  maplist(assert, Rows).

random_simple_name(FullName):-
  findall(N, simple_name(N),Names), 
  random_member(Name,Names),
  random(0,99,X),
  concat(Name,X,FullName).

simple_name_to_id(SimpleName,AgentID):-
  agent(_T,AgentID,Dict),
  SimpleName = Dict.get(simplename).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Other %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% executes predicate with defined probability
prop_call(Prop, Call):-
	Prop = [H,E],
	Ex is E+1,
	random(H,Ex,P),
	P is (E)-> call(Call); 
	true.
