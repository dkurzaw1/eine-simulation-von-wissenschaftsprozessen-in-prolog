/** <module> Create world (Events)
 This module creats a world, consisting of events and laws
 */

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                             Module create_world                         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This module creats a world, consisting of events and laws                 %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%                         Create a world with                             %%%
%%%     "initialisation_experiment, initialisation_run, create_world."      %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Module configuration %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(create_world_module,
    [ create_world/0, 
      remove_world/0,
      update_world/0,
      lawtree/2,
      lawtree/0,
      list_of_event_types/1
    ] ).

:- dynamic( [
    lawtree/2,
    eventtype/2, 
    event/4,
    list_of_event_types/1,
    ids_of_all_law_preconditions/1,
    ids_of_all_law_targets/1,
    state_of_the_universe/2,
    types_in_universe/2    
    ] ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create world %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! create_world_stand_alone is nondet
% create a world outside of a simulation run
% eg. for testing
create_world_stand_alone:-
    initialisation_experiment, 
    initialisation_run, 
    create_world.

%! create_world is nondet
% creates a world inside a run
create_world:- 
    init_config, 
    event_generator,
    sow_the_seeds_for_laws_and_events,
    create_lawtree,
    export_law_visualisation,
    performance_optimisation.

event_generator:-
    world_generation(Type,File),
    event_generator(Type, File).


event_generator(generator, _):-
    create_event_types,
    create_initial_events,
    create_laws.

event_generator(predefined, File):- 
    include(File).

%! performance_optimisation is nondet
% Prebuilds common used lists and reduces "findall" 
performance_optimisation:-
    find_IDs_of_all_law_preconditions(_),
    find_IDs_of_all_law_targets(_).

%! create_event_types is nondet
% Creates unique event types for each run
create_event_types:- 
    findall(Goal, create_event_types(Goal), Goals),
    cores(Cores),
    concurrent(Cores,Goals,[] ),
    update_list_of_event_types.

create_event_types(Goal):-
    number_of_types(NumberOfTypes),
    ( between(1,NumberOfTypes,C), 
      Goal = create_event_type).

%! create_initial_events is nondet
% Initial events needed for the first tick (input for laws)
create_initial_events:-
    findall(Goal, create_initial_events(Goal), Goals),
    cores(Cores),
    concurrent(Cores,Goals,[] ).

create_initial_events(Goal):-
    number_of_events(NumberOfEvents),
    ( between(1,NumberOfEvents,C),
    Goal = create_event ).

%! create_laws is nondet
% Laws transorm universe from tick to tick into new state
create_laws:-
    findall(Goal, create_laws(Goal), Goals),
    cores(Cores),
    concurrent(Cores,Goals,[] ).

create_laws(Goal):-
    number_of_laws(NumberOfLaws),
    ( between(1,NumberOfLaws,C),
        Goal = create_law).

%! create_lawtree is nondet
% Lawtree shows structure of the laws for analysis
create_lawtree:-

    findall(Goal, create_lawtree(Goal), Goals),
    cores(Cores),
    concurrent(Cores,Goals,[] ).

create_lawtree(Goal):-
    number_of_followup_laws(NumberOfLaws),
    ( between(1,NumberOfLaws,C),
        Goal = lawtree).

% Place for custom config options
init_config:- true. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Remove world - Will be called after every run %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! remove_world is nondet
% Removes world from memory
remove_world:-
    remove_config,
    remove_event_types,
    remove_events,
    remove_listings,
    remove_laws.

remove_config:-    
    retractall(lawtree(_,_)).

remove_events:-
    retractall(event(_,_,_,_)).

remove_event_types:-
    retractall( eventtype(_,_) ),
    retractall( state_of_the_universe(_,_) ),
    retractall( types_in_universe(_,_) ).

remove_laws:-
    retractall(ids_of_all_law_preconditions(_) ),
    retractall(ids_of_all_law_targets(_)),
    retractall(law_pre(_,_)),
    retractall(law_target(_,_)).


/* Only enable, if needed  for better performance; might effect simulation results
% Removes state of old univers each tick - could effect analysis of results
% Will breake some actions
remove_old_universe.
*/
remove_old_universe:-
    sim_time(Tick),
    retractall(event(_ID,_Type,Tick)).



remove_listings:-
    retractall(list_of_event_types(_)).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Update world - will be called once every tick %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
update_world:- 
    time(state_of_the_universe(_State)),
    time(types_in_universe(_Types)),
    time(update_universe),
    time(remove_old_universe), writeln('***********').
*/

update_world:- 
    state_of_the_universe(_State),
    types_in_universe(_Types),
    update_universe,
    remove_old_universe.



update_universe:- 
    cores(Cores),
    findall(Goal,create_world_module:update_universe(Goal),Goals),
    concurrent(Cores, Goals, []).

update_universe(Goal):-
    universe_bounders_2d(BounderX,BounderY),
    between(1, BounderX, C1),
    (   between(1, BounderY, C2),
        (   % thread_create_in_pool(threadpool , check_shapes([C1,C2]), _, [])
            % check_shapes([C1,C2])
            Goal = check_shapes([C1,C2])
        )).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Listings %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

update_list_of_event_types:-
    retractall(list_of_event_types(_)),
    findall(ID, eventtype(ID,_), List),
    asserta(list_of_event_types(List)).

state_of_the_universe(State):-
    sim_time(Tick), 
    findall([ID, Type], event(ID,Type,Tick, Location), State),
    asserta(state_of_the_universe(State, Tick)).

state_of_the_universe_with_location(State):-
    sim_time(Tick), 
    switch_dir(run),
    open('event_location.csv', append, Stream), 
    csv_write_stream(Stream,[row("ID","Type","Tick","X","Y")],[] ),
    forall(event(ID,Type,Tick, [X,Y]),  csv_write_stream(Stream,[row(ID,Type,Tick,X,Y)], [] )),
    close(Stream),
    switch_dir(simulation).
    

types_in_universe(TypesSet):-
    sim_time(Tick), 
    findall(Type, event(ID,Type,Tick, Location), Types),
    list_to_set(Types,TypesSet),
    asserta(types_in_universe(TypesSet, Tick)).

find_all_preconditions(PreLaws):-
    findall([ID,Precondition], law_pre(ID,Precondition), PreLaws).

find_IDs_of_all_law_preconditions(ListOfLawPre):-
    retractall(ids_of_all_law_preconditions(_) ),
    findall(ID, law_pre(ID,Content), ListOfLawPre),
    assertz(ids_of_all_law_preconditions(ListOfLawPre)).

find_IDs_of_all_law_targets(ListOfLawT):-
    retractall(ids_of_all_law_targets(_)),
    findall(ID, law_target(ID,Content), ListOfLawT),
    assertz( ids_of_all_law_targets(ListOfLawT) ).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create types and events %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_event_type:-
    id("EVENTTYPE",ID),
    asserta(eventtype(ID,_Options)).

create_event:-
    id("EVENT",ID),
    simulation:sim_time(Tick),
    random_type(Type),
    random_location(X,Y),
    asserta(event(ID, Type, Tick, [X,Y])).

create_specific_event(Type,Tick):-
    id("EVENT",ID),
    random_location(X,Y),
    assertz(event(ID, Type, Tick, [X,Y])).

random_type(Type):-
    list_of_event_types(ListOfTypes),
    random_member(Type, ListOfTypes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create Laws %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! create_law is nondet
% Creates a law
create_law:- 
    base_law(LawID, Types, Complexity),
%    random_sublist(Complexity, Types, Precondition),
%    random_sublist(Complexity, Types, TargetState),
    random(0, Complexity, ComplexityPre),
    random(0, Complexity, ComplexityTarget),
    findall(ElementPre, ( between(0,ComplexityPre, C),build_element(ElementPre) ), ElementsPre),
    assertz(law_pre(LawID,ElementsPre)), 
    findall(ElementTarget, ( between(0,ComplexityTarget, C),build_element(ElementTarget) ), ElementsTarget),
    assertz(law_target(LawID,ElementsTarget)).

%! build_element(Element) is nondet
% Returns an element: A location and event type
build_element(Element):-
    list_of_event_types(Types),
    random_member(Type,Types),
    random_relative_location(Location),
    Element = [Type,Location].

%! lawtree is nondet
% Builds laws, which build uppon existing laws
lawtree:- 
    base_law(LawID, Types, Complexity),
    findall([TID,TState], law_target(TID,TState), ChainDomain),
    random_member(ChainTuple, ChainDomain),
    ChainTuple = [SourceID, ChainPrecondition],
    assertz( lawtree(SourceID, LawID) ),
    assertz(law_pre(LawID,ChainPrecondition)),

    random(0, Complexity, ComplexityTarget),
    findall(ElementTarget, ( between(0,ComplexityTarget, C),build_element(ElementTarget) ), ElementsTarget),
    assertz(law_target(LawID,ElementsTarget)).

%! base_law(LawID, Types, Complexity) is nondet
% Creates law body
base_law(LawID, Types, Complexity):-
    uuid(UUID),
    concat("LAW",UUID,LawID),
    list_of_event_types(Types),
    law_complexity(MaxComplexity),
    random(1,MaxComplexity,Complexity).

%! check_shapes(BaseLocation) is nondet
% Tries all defined laws at given location
check_shapes(BaseLocation):-
    BaseLocation = [X,Y],
    ids_of_all_law_preconditions(ListOfLawPre),
    foreach( member(LawID,ListOfLawPre), check_law_shape(LawID, BaseLocation)).

%! check_law_shape(LawID,BaseLocation) is nondet
% Tries to apply a given law at given location
check_law_shape(LawID,BaseLocation):-
    law_pre(LawID,Content), 
    forall(member(Element, Content), check_element(Element,BaseLocation))->
    apply_law_location(LawID, BaseLocation); true.

%! check_element(Element, BaseLocation) is nondet
% Checks, if element of given type is at given position
check_element(Element, BaseLocation):- 
    Element = [Type, RelativeLocation], 
    sim_time(Tick),
    relative_to_absolut_location(RelativeLocation,BaseLocation,Location),
    event(ID, Type, Tick, Location),
    % test %
    retract(event(ID, Type, Tick, Location)).

apply_law_location(LawID, BaseLocation):-
    law_target(LawID, Content),
    addlog(LawID, apply_law, _ , _),
    sim_time(Tick),
	addlog(LawID, law, apply_law,Tick),
    forall(member(Element,Content), create_event_with_location(Element, BaseLocation)).  

create_event_with_location(Element, BaseLocation):-
    Element = [Type, RelativeLocation],
    sim_time(Tick),
    NewTick is Tick + 1,
    id("event",EventID),
    relative_to_absolut_location(RelativeLocation,BaseLocation,Location),
    addlog(EventID, event, create_event_with_location,_),
    assertz(event(EventID, Type, NewTick, Location)).

create_event_with_location_actual_tick(Element, BaseLocation):-
    Element = [Type, RelativeLocation],
    sim_time(Tick),
    id("event",EventID),
    relative_to_absolut_location(RelativeLocation,BaseLocation,Location),
    addlog(EventID, event, create_event_with_location,_),
    assertz(event(EventID, Type, Tick, Location)).

sow_the_seeds_for_laws_and_events:-
    findall(Prelaw, law_pre(ID, Prelaw), IDs),
    forall(member(ID, IDs), apply_prelaw(ID)).

apply_prelaw(ListPreCond):-
    law_pre(ID, Content),
    random_location(X,Y),
    BaseLocation =[X,Y],
    forall(member(Element,Content), create_event_with_location_actual_tick(Element, BaseLocation)).
