/** <module> Agents definition

*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prolog Database and definition: Agents %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-module(agents,
  [ agent/3,
    belief/6,
    create_domains/0,
    construct_agent/0,
    random_belief_for_each_agent/0,
    specific_belief_for_set_of_agents/2,
    create_new_agents/0,
    create_new_agents_p/0,
    check_if_agent_still_active/1,
    agent_active/1,
    agent_activity/0,
    check_agent_active/2
  ]).

:-dynamic([belief/6,agent/3,research_domain/2]).
                            
create_new_agents:-
	random(1,10,Rnd),
	foreach(between(0,Rnd,_),create_new_agents_p).

create_new_agents_p:-
	random(0,10,Rnd),
	( Rnd > 3,
	construct_agent );
	true.

agent_activity:-
  list_of_active_agents(ListOfActiveAgents),

  forall(member(AgentID,ListOfActiveAgents), agent_active(AgentID)).


%Is true, if given agent in tick active; fail if agent is inactive
check_if_agent_still_active(Agent):-
  agent(Type,Agent,Properties),
  (	Inactive = Properties.get(inactive) -> (fail);
	  (true) ).

agent_age(Agent,Age):-
  sim_time(Tick),
	agent(Type,Agent,Properties),
	Created = Properties.get(created),
	Age is Tick - Created.

set_agent_inactive(Agent):-
agent(Type,Agent,Properties),
  retract(agent(Type,Agent,Properties)),
  sim_time(Tick),
  NewProp = Properties.put(inactive,Tick),
  assertz(agent(Type,Agent,NewProp)).

agent_active(Agent):-
  agent_age(Agent,Age),
  (check_agent_active(Agent, Age), fail; true).

 /*check_agent_active(Agent, Age):-
 Age > 45 -> set_agent_inactive(Agent); true.*/

check_agent_active(Agent, Age):-
  Age < 6 *->
    (
    random(0,100,Random),
    Random < 20,
    set_agent_inactive(Agent)
    );
  Age >= 45 *->
    (
    Probability is (0.05*(Age-45)^2+55),
    random(1,100,Random),
    Random > Probability,
    set_agent_inactive(Agent)
    );
    (
      Probability is (0.5*Age),
      random(0,100,Random),
      Random < Probability,
      set_agent_inactive(Agent)
    ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Construct Agents  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% constructing simple agent; no special attributes
construct_agent:-
  select_agent_type(AgentType),
  id("AGENT", AgentID),
  random_simple_name(SimpleName),
  sim_time(Tick),
  default_resources(Res),
  select_random_domain(DomainID),
  determine_interests(DomainID, Interests),
  random(0,10,RndAbility), Ability is (RndAbility/10),
  assertz( agent(AgentType,AgentID,properties{created:Tick, resources:Res, reputation:1.0, ability:Ability, action_que:[], research_domain:[DomainID], list_of_interests: Interests, simplename:SimpleName} ) ).

% Select a random type from list of possible types (defined by type/1), probabllity is be default P(A) =  |E|/|A| 
select_agent_type(AgentType):-
  list_of_types(List),
  random_member(AgentType,List).

% Select event types for interests or "research subjects"
determine_interests(DomainID, Interests):-
  research_domain(DomainID, EventTypes), 
  length(EventTypes, NumberOfTypes),
  maxInterest(MaxInterest),
  NumberIntersts is round( ( NumberOfTypes / 100 ) * MaxInterest) +1,
  random_sublist(NumberIntersts,EventTypes, Interests).

% provides list of types; types might be dynamic
list_of_types(List):-
  findall(Type,agents:types(Type),List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agents set of random beliefs for initialisation  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives each existing agent a random belief; used for initalisation
random_belief_for_each_agent:-
  find_all_agents(List_of_Agents),
  find_all_facts(List_of_Facts),
  % Give every Agent a random belief
  ( member(Agent,List_of_Agents),
    %random_element_from_list(List_of_Facts,FactID),
    random_member(FactID, List_of_Facts),
  %  uuid(UUID),
    sim_time(Tick),
    assertz( belief(Agent, Tick, fact, BeliefContent, Domain, Degree) ),
    fail ; true
  ).

% gives a specfic set of agents a random belief
specific_belief_for_set_of_agents(Set_Of_AgentIDs,FactID):-
  ( member(Agent,Set_Of_AgentIDs),
    sim_time(Tick),
    assertz( belief(Agent, Tick, fact, FactID, Domain, Degree) ),
    fail ; true
  ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agent a random beliefs %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives a specfic agent a random belief
% GGF. LÖSCHEN UND specific_belief_for_set_of_agents verwenden/2 ?
random_belief_for_agent(AgentID):-
  find_all_facts(List_of_Facts),
  %random_element_from_list(List_of_Facts,FactID),
  random_member(FactID,List_of_Facts),
  sim_time(Tick),
  assertz( belief(AgentID, Tick, ContentType, FactID, Domain, Degree) ),
  fail ; true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agent Types  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


types(student).
types(researcher).
types(funder).

belief_types(Types):-
  Types = [fact, hypothesis, observation, publication].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Research Domain  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_domains:-
  number_of_domains(NumberOfDomains),
  between(0,50, C), 
  (create_random_research_domain(Domain)),fail;true.


create_random_research_domain(Domain):-
  list_of_event_types(EventTypes),
  number_of_types(Types),
  random(1,Types,Random),
  random_sublist(Random, EventTypes, Sublist),
  create_research_domain(Sublist,Domain).

create_research_domain(ListOfTypes, Domain):-
  id("RESDOM", DomainID),
  Domain = research_domain(DomainID, ListOfTypes),

  asserta(Domain).


select_random_domain(Domain):-
  findall(DomainID, research_domain(DomainID, ListOfTypes), Domains ),
  random_member(Domain,Domains).



/*
check_agent_still_active(Agent):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	agent(Type,Agent,Properties),
  agent_age(Agent,Age),
% Agent over max working time then not agent_active
  ( Age >= MaxWorkingTime -> ;
    Age < MaxWorkingTime

    )
% Probability of the agent dropping out in advance
*/

% decides if agent leaves the system (get inactive) for defined reasons
/*
career_decision:-
  forall( ( agent(Type,Agent,Properties) ),
    reason_for_leave_system(Agent) ).

reason_for_leave_system(Agent):-
  agent_active(Agent)->
  (
  reason_for_leave_system(Reason,Agent),
  fail;true); true.

reason_for_leave_system(carrer_switch,Agent):-
  random(0,100,Random),
  Random < 80,
  true.
  %set_agent_inactive(Agent).


reason_for_leave_system(carrer_switch,Agent):-
  random(0,100,Random),
  Random < 20,
  set_agent_inactive(Agent).

reason_for_leave_system(no_contract,Agent):-
  agent_age(Agent,Age),
  Age<10,
  random(0,100,Random),
  Random < 50,
  set_agent_inactive(Agent).

reason_for_leave_system(bad_health,Agent):-
  agent_age(Agent,Age),
  Age>30,
  random(0,100,Random),
  Random < 2,
  set_agent_inactive(Agent).

reason_for_leave_system(age,Agent):-
  maxWorkingTime(MaxWorkingTime),
  agent_age(Agent,Age),
  MaxWorkingTime=<Age,
  set_agent_inactive(Agent).

reason_for_leave_system(stay,Agent,Tick,MaxWorkingTime).

*/

/*
% Young and leave system
check_agent_active(Agent, Age):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	Age < MaxWorkingTime,
	agent(Type,Agent,Properties),
	random_between(0,75,Rnd),
	random_between(0,50,High),
  Rnd < High ,
  set_agent_inactive(Agent).

*/
% Old and leave system
