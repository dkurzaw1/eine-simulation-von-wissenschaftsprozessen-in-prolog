# Prolog Social Simulation System -  Module description 

## About the simulation
This manual gives an overview of the Prolog Social Simulation System for Scientific Communities. This simulation is aimed for simulating the interaction of researchers on a (1) social (2) theoretical and (3) society level. For this, it takes positions from the philosophy of science, sociology, and other related fields into account.  

## Reference
This reference gives just a overview over the predicates. For a propper understanding please have a look at Daniel Kurzawe: (...) (tbp).

## Authorship and License
@author Daniel Kurzawe
@version ALPHA
@see http://www.daniel-kurzawe.de
@license GNU/GPL V.3 http://www.gnu.org/copyleft/gpl.html

