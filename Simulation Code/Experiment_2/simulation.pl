 ﻿/**
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROLOG SOCIAL SIMULATION SYSTEM
%%% WRITTEN BY DANIEL KURZAWE
%%% -------------------------
%%% CONTACT: Daniel Kurzawe
%%% LICENCE: GNU/GPL V.3 (?) http://www.gnu.org/copyleft/gpl.html
%%% VERSION:  ALPHA
%%% WEBSITE: daniel-kurzawe.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moduls %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% :- module(simulation).

%%% For debuging %%%
%%% Visualisation of program structure %%%
:- use_module(library(callgraph)).
%%% run command:  module_dotpdf(user,[method(unflatten([fl(4),c(4)]))]).

:- style_check(-singleton).
%:- module(kernel,[sim_time/1]).

%%% CSV file output library %%%
:- use_module(library(csv)).

%%% Action definitions and logic %%%
:- use_module('utils.pl').

%%% Action definitions and logic %%%
:- use_module('configuration.pl').

%%% Action definitions and logic %%%
:- use_module('actions.pl').

%%% Agents definitions and logic %%%
:- use_module('agents.pl').

%%% Create laws %%%
:- use_module('create_world.pl').

%%% World facts database %%%
:- use_module('world_facts.pl').

%%% Screen output %%%
:- use_module('screen_output.pl').

%%% File output %%%
:- use_module('file_output.pl').

%%% Publication addition %%%
:- use_module('publication.pl').

% Dynamic defintion for predicates, acting as "global variables"
:- dynamic(sim_time/1).

%%% DATABASE %%%

% load agents

% load actions

% load facts

% load configuration

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initalisation of variables and Constants %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

version('0.3').
sim_time(1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation kernal and logic %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Main loop %%%

%%% Exit conditions for the end of a simulation run %%%
run(End,End):- message(finish,_).

%%% Main loop definition %%%
run(Counter,End):-
	Counter1 is Counter + 1,
	message(tick,Counter),
	tick,
	retractall(sim_time(_)),
	assert(sim_time(Counter1)),
	run(Counter1,End).

%%% Processes for single tick %%%
tick:-
	%%reflect,
	apply_actions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Additional functions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reflect:-
	action(reflect_knowledge_base, _, _).

%%% Finds all active agents and let them do actions
apply_actions:-
	message(apply_actions,_),
	maybe_new_agent,
	findall(Agent_ID,agent(_,Agent_ID,_),List_Of_Agents),
	apply_action_with_Agent(List_Of_Agents).

%%% Apply an action of a defined agent
apply_action_with_Agent([]).
apply_action_with_Agent([Agent|Other_Agents]):-
	action_list(List_Of_Actions),
	%random_element_from_list(List_Of_Actions,Action),
	random_member(Action,List_Of_Actions),
	sim_time(Tick),
	% is Agent still active or does Agent leave the system?
	( check_agent_still_active(Agent),
		call(action(Action,Agent,Tick));
		%check_agent_still_active(Agent);
		%writeln("Agent ist nicht aktiv")
		true
	),

	% Maybe new Agents join the system?
	apply_action_with_Agent(Other_Agents).


maybe_new_agent:-
	random(1,10,Rnd),
	foreach(between(0,Rnd,_),maybe_new_agent_p).

maybe_new_agent_p:-
	random(0,10,Rnd),
	( Rnd > 3,
	construct_agent );
	true.

check_agent_still_active(Agent):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	agent(Type,Agent,Properties),
	Inactive = Properties.get(inactive) ->
	fail;
	agent_active(Agent).

%:-dynamic(maxWorkingTime/1).

agent_active(Agent):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	%random_between(5,60,MaxWorkingTime),
	%retractall(maxWorkingTime(X)),
	%assert(maxWorkingTime(MaxWorkingTime)),
	agent(Type,Agent,Properties),
	Created = Properties.get(created),
	Age is Tick - Created,
	agent_active(Agent, Age).

% Young and leave system
agent_active(Agent, Age):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	Age < MaxWorkingTime,
	agent(Type,Agent,Properties),
	random_between(0,75,Rnd),
	random_between(0,50,High),
	Rnd < High ,
	( retract(agent(Type,Agent,Properties)),
	  NewProp = Properties.put(inactive,Tick),
	  assert(agent(Type,Agent,NewProp)),
	  !
	)	.


% Old and leave system
agent_active(Agent, Age):-
	sim_time(Tick),
	maxWorkingTime(MaxWorkingTime),
	MaxWorkingTime =< Age,
	agent(Type,Agent,Properties),
	retract(agent(Type,Agent,Properties)),
	NewProp = Properties.put(inactive,Tick),
	assert( agent(Type,Agent,NewProp) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation database %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Prolog Database: World Facts %%%
% This section is for additional world facts, iff not definied in world_facts.pl

% Die Welt wird durch Fakten objektive Fakten bestimmt, welche durch die Agenten erfahren werden können.
% Hierbei ist in dieser Version keine Subjektivität in Form von Messunschärfer, Interpretation o.ä. berücksichtigt
% Hier etwas wie world(fact(...),zeitpunkt(tick),).? oder ist die Welt auch ein Agent? Glaube nicht, da nicht eigene Handlugen ausführt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation  initialisiation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Initialisation %%%

%%% creates a given number of agents %%%
create_agents(Number_Of_Agents):-
	foreach(between(1, Number_Of_Agents, _X), construct_agent).

%%% create a given number of facts
create_facts(Number_Of_Facts):-
	foreach(between(1, Number_Of_Facts, _),generate_fact).

%%% assign a given number of random beliefs to each agent
assign_beliefs(Number_Of_Beliefs_Per_Agent):-
	foreach(between(1, Number_Of_Beliefs_Per_Agent, _),random_belief_for_each_agent).

%%% initalisation of a simulation run %%%
initialisation:-
  % Reset internal simulation time to tick 1
  asserta(sim_time(1)),
	% Create specific ID for this run
	run_id,
	run_id(Run_ID),
	% Create folder for saving files for this run
	make_directory(Run_ID),
  % Create simulated environment (laws, objects,...)
  create_world,
	% Create agents for the first tick
	number_of_agents_at_start(Number_Of_Agents),
	create_agents(Number_Of_Agents),
	number_of_facts_at_start(Number_Of_Facts),
	create_facts(Number_Of_Facts),
	assign_beliefs(5), % Sinn dieser Konstanten? In Config übertragen?
	findall(agent(Type,ID,Attributes),agent(Type,ID,Attributes),List_Of_Agents),
	create_first_run(List_Of_Agents).

%%% setup for the first run %%%
create_first_run(List):-
	member(Agent,List),
	assert(time(0,0,Agent)),
	fail;true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation - File Output %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% write tick results to file %%%
write_tick_results:-
	output_filename(Filename),
	open(Filename,write,Stream),
	write(Stream, "Test"),
	close(Stream).

% Check if results dir exists
%-> create dir
checking_results_dir:-
	exists_directory('./results');
	make_directory('./results').

% Check if output file exists
%-> create file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation  start %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Simulation start %%%
:- style_check(-singleton).
:- message(welcome,_).

% creates an ID for a simulation run
run_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([Year,Month,Day,Hour,Min,Sec],"-",Run_ID),
	retractall(run_id(_)),
	asserta(run_id(Run_ID)).

:- 	message(border,_),
	message(generic,'Files loaded'),
	message(generic, '... use "?- start." '),
	message(border,_).

% Clean up memory
cleanup:-
  remove_world,
	retractall(sim_time(_)),
  retractall(agent(_,_,_)),
  retractall(belief(_,_,_)),
  retractall(publiaction(_,_,_,_)),
  retractall(fact(_,_,_)),
  retractall(fact(_)).

% Multiple run
mult_run(Seed):-
	time(mult_run2(Seed)).

mult_run2(Seed):-
	%set_random(seed(Seed)),
	initialisation,
	%set_random(seed(Seed)),
	number_of_ticks(NT),
	run(0,NT),
	write_results(alter_csv),
	write_results(agent_statistic_age_csv),
	cleanup.
 	%write_results(agent_statistic_age_csv),
	%write_results(belief), write_results(belief_statistic_csv),
  %dynamic([belief/3,agent/3]),

% Simulation start
start:-foreach(between(1,10,Seed),mult_run(Seed)), halt.
