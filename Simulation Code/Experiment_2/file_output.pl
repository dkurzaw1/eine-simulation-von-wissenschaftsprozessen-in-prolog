:- module(file_output,[write_results/1]).
:- use_module(library(csv)).
:-dynamic(agents_in_tick/3,outputSteam/1,bel_in_tick/2).



write_results(alter_csv):-

  % Vereichnis wechseln
  simulation:working_directory(CWD, CWD),
  run_id(Run_ID),
  concat(CWD,Run_ID,NewCWD),
  simulation:working_directory(_, NewCWD),


  open("AgentsAge.csv",write,Stream),
  csv_write_stream(Stream, [ row('Agent', 'Age', 'Created', 'Inactive') ], [] ),
  assert(outputSteam(Stream)),


  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
  Agent =.. [agent,Type,AgentID,Attributes],
  Age is Attributes.get(inactive) - Attributes.get(created),
  csv_write_stream(Stream, [ row(AgentID, Age, Attributes.get(created), Attributes.get(inactive)) ], [] ),
  fail;true),
  close(Stream),
  simulation:working_directory(_,CWD).








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
write_results(agent_statistic_age_csv):-
  % Change working directory into run folder
  simulation:working_directory(CWD, CWD),
  run_id(Run_ID),
  concat(CWD,Run_ID,NewCWD),
  simulation:working_directory(_, NewCWD),

  sim_time(Tick),
  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
      Agent =.. [agent,Type,AgentID,Attributes],

      (( InAct = Attributes.get(inactive),
        ( between(Attributes.get(created),Attributes.get(inactive),ActiveTick),
          asserta( agents_in_tick(ActiveTick,AgentID,Type) ),
          fail;true)
        );
      ( not(InAct = Attributes.get(inactive)),
        ( between(Attributes.get(created),Tick,ActiveTick),
          asserta( agents_in_tick(ActiveTick,AgentID,Type) ),
          fail;true)
        )),

    fail;true
    ),

    open("AgentsPerTick.csv",write,Stream),
    csv_write_stream(Stream, [ row('Tick', 'AgentsPerTick') ], [] ),
    assert(outputSteam(Stream)),
    (between(1,Tick,Counter),
      findall(_,agents_in_tick(Counter,AID,Type),AgentsPerTickList),
      length(AgentsPerTickList,AgentsPerTick),
      csv_write_stream(Stream, [ row(Counter, AgentsPerTick) ], [] ),

     % write('Tick '),writeln(Counter),
     % write('Agents '),writeln(AgentsPerTick),
    fail;true),
    close(Stream),
    retractall(agents_in_tick(_,_,_)),
    simulation:working_directory(_,CWD).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(belief_statistic_csv):-
  % Change working directory into run folder
  simulation:working_directory(CWD, CWD),
  run_id(Run_ID),
  concat(CWD,Run_ID,NewCWD),
  simulation:working_directory(_, NewCWD),

  sim_time(Tick),
  findall(agent(T,ID,P),agent(T,ID,P),List_Of_Agents),
  ( member(Agent,List_Of_Agents),
      Agent =.. [agent,Type,AgentID,Attributes],
      findall(BT,belief(AgentID,BT,BeliefContent),BelList),
        (member(BelTick,BelList),
        ( between(BelTick,Attributes.get(inactive),BelActTick),
          asserta( bel_in_tick(BelActTick,AgentID) ),
          fail;true),
        fail;true),

    fail;true
    ),

    open("beliefPerTick.csv",write,Stream),
    csv_write_stream(Stream, [ row('Tick', 'BeliefsPerTick') ], [] ),
    assert(outputSteam(Stream)),
    (between(1,Tick,Counter),
      findall(_,bel_in_tick(Counter,BelAgentID),BelivePerTickList),
      length(BelivePerTickList,BelPerTick),
      csv_write_stream(Stream, [ row(Counter, BelPerTick) ], [] ),
    fail;true),
    close(Stream),
  %retractall(bel_in_tick/2),
    simulation:working_directory(_,CWD).




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_results(belief):-
  output_filename(Filename),
	open(Filename,write,Stream),
  writeln('[SAVE] Write terms to file'),
  with_output_to(Stream, listing(agents:belief)),
	write(Stream, "eof"),
	close(Stream),
  writeln('[OK] terms written!').

write_results(csv):-
  message(border,_),
  writeln('[SAVE] Write terms to csv file'),
	open("csv_Agent_Facts.csv",write,Stream),
  csv_write_stream(Stream, [ row('Tick', 'AgentID' ,'Fact') ], [] ),
  forall(belief(AgentID,Tick,Fact),
         csv_write_stream(Stream,
                        [row(Tick, AgentID ,Fact)],
                        [] ) ),
	close(Stream),
  write_gephi_files.

write_gephi_files:-
  message(border,_),
  writeln('[SAVE] Write gephi output'),
	open("csv_Agent_Facts_G.csv",write,Stream),
  csv_write_stream(Stream, [ row('Tick', 'Source' ,'Target') ], [] ),
  forall(belief(AgentID,Tick,Fact),
         csv_write_stream(Stream,
                        [row(Tick, AgentID ,Fact)],
                        [] ) ),
	close(Stream),

  % Agent Namens
  open("csv_Agent_Names.csv",write,Stream2),
  csv_write_stream(Stream2, [ row('AgentID') ], [] ),
  findall(AgentIDs,belief(AgentIDs,T,F),Agents),

  sort(Agents,AgentsSort),

  forall(member(A, AgentsSort),
          csv_write_stream(Stream2, [row(A)],[] )
         ),
	close(Stream2),

  %FACT NAMES
  open("csv_Fact_Names.csv",write,Stream3),
  csv_write_stream(Stream3, [ row('FactID') ], [] ),
  findall(FactIDs,belief(F,T,FactIDs),Facts),

  sort(Facts,FactsSort),

  forall(member(F, FactsSort),
          csv_write_stream(Stream3, [row(F)],[] )
         ),
	close(Stream3),
  writeln('[OK] csv written!'),
  message(border,_).
