:-module(screen_output,[message/2]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Screen output predicats %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Screen output %%%

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge all or specific screen output
message(_,_):-true.
message(generic,_):-true.

%%%%%%%%%%%%%%%%%%%%%%%%

message(border,_):-
  writeln('==================================================================').

  message(stars,_):-
    writeln('******************************************************************').

  message(welcome,_):-
    message(stars,_),
    version(Version),
    write('******          PROLOG SOCIAL SIMULATION SYSTEM  * '),
    write(Version),
    writeln('      ******'),
    message(stars,_),
    writeln('').

message(finish,_):-
  message(border,_),
  writeln('[OK] Simulation successfully finished'),
  message(border,_).

%%%%%%%%%%%%%%%%%%%%%%%%
% Bridge info screen output
% message(_,_):-true.
%%%%%%%%%%%%%%%%%%%%%%%%

message(tick,Counter):-
  message(border,_),
  write('>>  Tick : '), writeln(Counter),
  message(border,_).

message(loop,Counter):-
  message(border,_),
  write('>>>>  Inner Loop : '), writeln(Counter),
  message(border,_).

message(apply_actions,_):-
  write('[ACTIONS] Apply action in Tick '),
  sim_time(Tick),
  writeln(Tick).

message(generic,Content):-
  write(">> "),
  writeln(Content).
