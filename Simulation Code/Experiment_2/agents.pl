%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prolog Database and definition: Agents %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-module(agents,
  [agent/3,
  belief/3,
  construct_agent/0,
  random_belief_for_each_agent/0,
  specific_belief_for_set_of_agents/2]).

:-dynamic([belief/3,agent/3]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Construct Agents  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% constructing simple agent; no special attributes
construct_agent:-
  % Select a random type from list of possible types (defined by type/1), P(A) = 0,5%
  list_of_types(List),
  random_member(Type,List),
  % Create unique ID for identification; adds AGENT#id for easy handling
  uuid(ID),
  string_concat("AGENT#", ID, AgentID),
  sim_time(Tick),
  % gives agent action points (fixed or random, see configuration.pl)
  default_action_points(AP),
  % finaly creates the agent
  assertz( agent(Type,AgentID,properties{created:Tick, actionPoints:AP, reputation:1.0} ) ).

% provides list of types; types might be dynamic
list_of_types(List):-
  findall(Type,agents:types(Type),List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agents set of random beliefs for initialisation  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives each existing agent a random belief; used for initalisation
random_belief_for_each_agent:-
  find_all_agents(List_of_Agents),
  find_all_facts(List_of_Facts),

  % Give every Agent a random belief
  ( member(Agent,List_of_Agents),
    %random_element_from_list(List_of_Facts,FactID),
    random_member(FactID, List_of_Facts),
  %  uuid(UUID),
    sim_time(Tick),
    BeliefContent = belief_attributes{fact:FactID,degree:1.0},
    assert( belief(Agent, Tick, BeliefContent) ),
    fail ; true
  ).

  %Find all possible facts
  %Approximation?
  %Wrong facts?
  %Create a random belief ...

% Protyp for automaticly construct agents
% agent_prototyp( [ [Name_of_entety,[Options_or_Range] ], (...) ] )
% agent_prototyp([ [type,[type1,type2] ], [id,[] ], [options,[] ] ] ).

% gives a specfic set of agents a random belief
specific_belief_for_set_of_agents(Set_Of_AgentIDs,FactID):-
  ( member(Agent,Set_Of_AgentIDs),
    sim_time(Tick),
    assert( belief(Agent, Tick, FactID) ),
    fail ; true
  ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Gives Agent a random beliefs %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% gives a specfic agent a random belief
% GGF. LÖSCHEN UND specific_belief_for_set_of_agents verwenden/2 ?
random_belief_for_agent(AgentID):-
  find_all_facts(List_of_Facts),
  %random_element_from_list(List_of_Facts,FactID),
  random_member(FactID,List_of_Facts),
  sim_time(Tick),
  assert( belief(AgentID, Tick, FactID) ),
  fail ; true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Agent Types  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

types(type1).
types(type2).
