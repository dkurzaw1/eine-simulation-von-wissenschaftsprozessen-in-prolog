 ﻿/**
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROLOG SOCIAL SIMULATION SYSTEM
%%% WRITTEN BY DANIEL KURZAWE
%%% -------------------------
%%% CONTACT: Daniel Kurzawe
%%% LICENCE: GNU/GPL V.3 (?) http://www.gnu.org/copyleft/gpl.html
%%% VERSION:  ALPHA
%%% WEBSITE: daniel-kurzawe.de
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Moduls %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%:- module(kernel,[sim_time/1]).

%%% CSV file output library %%%
:- use_module(library(csv)).

%%% Action definitions and logic %%%
:- use_module('utils.pl').

%%% Action definitions and logic %%%
:- use_module('configuration.pl').

%%% Action definitions and logic %%%
:- use_module('actions.pl').

%%% Agents definitions and logic %%%
:- use_module('agents.pl').

%%% World facts database %%%
:- use_module('world_facts.pl').

%%% Screen output %%%
:- use_module('screen_output.pl').

%%% File output %%%
:- use_module('file_output.pl').

%%% Publication addition %%%
:- use_module('publication.pl').

% Dynamic defintion for predicates, acting as "global variables"
:- dynamic(sim_time/1).

%%% DATABASE %%%

% load agents

% load actions

% load facts

% load configuration

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Initalisation of variables and Constants %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

version('0.3').
sim_time(1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation kernal and logic %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Main loop %%%

%%% Exit conditions for the end of a simulation run %%%
run(End,End):- message(finish,_).

%%% Main loop definition %%%
run(Counter,End):-
	Counter1 is Counter + 1,
	message(tick,Counter),
	tick,
	retractall(sim_time(_)),
	assert(sim_time(Counter1)),
	run(Counter1,End).

%%% Processes for single tick %%%
tick:-
	%reflect,
	apply_actions.


%%% Inner loop exit condition %%%
%inner_loop(End,End).

%%% Inner loop definition %%%
%inner_loop(Counter,End):-
%	Counter1 is Counter + 1,
	% message(loop,Counter),
	%%% All agents reflect their beliefs
%	reflect,
	%%% All agents have possibillity for doing things
%	apply_actions,
	% saving results
%	inner_loop(Counter1,End).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Additional functions %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reflect:-
	action(reflect_knowledge_base, _, _).

%%% Finds all active agents and let them do actions
apply_actions:-
	message(apply_actions,_),
	maybe_new_agent,
	findall(Agent_ID,agent(_,Agent_ID,_),List_Of_Agents),
	apply_action_with_Agent(List_Of_Agents).

%%% Apply an action of a defined agent
apply_action_with_Agent([]).
apply_action_with_Agent([Agent|Other_Agents]):-
	action_list(List_Of_Actions),
	%random_element_from_list(List_Of_Actions,Action),
	random_member(Action,List_Of_Actions),
	sim_time(Tick),
	% is Agent still active or does Agent leave the system?
	( check_agent_still_active(Agent),
		call(action(Action,Agent,Tick));
		%check_agent_still_active(Agent);
		%writeln("Agent ist nicht aktiv")
		true
	),

	% Maybe new Agents join the system?
	apply_action_with_Agent(Other_Agents).

maybe_new_agent:-

	random(0,100,Rnd),
	(	Rnd > 30,
		construct_agent,
		message(generic,"[Erstellt] Ein neuer Agent erstellt")
	);
	message(generic,"Kein neuer Agent erstellt").


check_agent_still_active(Agent):-
	sim_time(Tick),
	agent(Type,Agent,Properties),
	( Inactive = Properties.get(inactive),fail ;
		not(Inactive = Properties.get(inactive) ),
		( Created = Properties.get(created) ,
			WorkingAge is Tick - Created,
				maxWorkingTime(MaxWorkingTime),
					(	WorkingAge < MaxWorkingTime,
						random(1,100,Rnd),
						Rnd < 98 ) ;
				%message(generic,"Agent ist aktiv"),
					( retract(agent(Type,Agent,Properties)),
						NewProp = Properties.put(inactive,Tick),
						assert(agent(Type,Agent,NewProp))
					)
		)
	).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation database %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Prolog Database: World Facts %%%
% This section is for additional world facts, iff not definied in world_facts.pl

% Die Welt wird durch Fakten objektive Fakten bestimmt, welche durch die Agenten erfahren werden können.
% Hierbei ist in dieser Version keine Subjektivität in Form von Messunschärfer, Interpretation o.ä. berücksichtigt
% Hier etwas wie world(fact(...),zeitpunkt(tick),).? oder ist die Welt auch ein Agent? Glaube nicht, da nicht eigene Handlugen ausführt

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation  initialisiation %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Initialisation %%%

%%% creates a given number of agents %%%
create_agents(Number_Of_Agents):-
	between(1, Number_Of_Agents, _X),
	construct_agent,
	false;true.

%%% create a given number of facts
create_facts(Number_Of_Facts):-
	between(1, Number_Of_Facts, _),
	generate_fact,
	false;true.

%%% assign a given number of random beliefs to each agent
assign_beliefs(Number_Of_Beliefs_Per_Agent):-
	between(1, Number_Of_Beliefs_Per_Agent, _),
	random_belief_for_each_agent,
	false;true.


%%% initalisation of a simulation run %%%
initialisation:-
	% create specific ID for this run
	run_id,
	run_id(Run_ID),
	%create folder for saving files for this run
	make_directory(Run_ID),
	% Erstelle X Agenten
	number_of_agents_at_start(Number_Of_Agents),
	create_agents(Number_Of_Agents),
	number_of_facts_at_start(Number_Of_Facts),
	create_facts(Number_Of_Facts),
	assign_beliefs(5), % Sinn dieser Konstanten? In Config übertragen?
	findall(agent(Type,ID,Attributes),agent(Type,ID,Attributes),List_Of_Agents),
	create_first_run(List_Of_Agents).

%%% setup for the first run %%%
create_first_run(List):-
	member(Agent,List),
	assert(time(0,0,Agent)),
	fail;true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation - File Output %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% write tick results to file %%%
write_tick_results:-
	output_filename(Filename),
	open(Filename,write,Stream),
	write(Stream, "Test"),
	close(Stream).

% Check if results dir exists
%-> create dir
checking_results_dir:-
	exists_directory('./results');
	make_directory('./results').

% Check if output file exists
%-> create file

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Simulation  start %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Simulation start %%%
:- style_check(-singleton).
:- message(welcome,_).

run_id:-
	get_time(TimeStamp),
	stamp_date_time(TimeStamp,Time,'UTC'),
	Time=..[_,Year,Month,Day,Hour,Min,Sec|P],
	atomic_list_concat([Year,Month,Day,Hour,Min,Sec],"-",Run_ID),
	retractall(run_id/1),
	asserta(run_id(Run_ID)).

:- 	message(border,_),
	message(generic,'Files loaded'),
	message(generic, '... use "?- start." '),
	message(border,_).

% Multiple run
%:- foreach(between(1,100,NB), initialisation, set_random(seed(X)), number_of_ticks(NT), run(0,NT).

% Single run
:- initialisation, number_of_ticks(NT), run(0,NT), write_results(agent_statistic_age_csv), write_results(belief). %write_results(belief_statistic_csv).

%:- write_results(belief).
%:- write_results(csv).
