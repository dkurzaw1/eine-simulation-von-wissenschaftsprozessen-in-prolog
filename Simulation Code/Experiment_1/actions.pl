:- module(actions,[action/3,action_list/1]).
:- use_module('agents.pl').

% Bridged in this experiment
action(reflect_knowledge, AgentID , _Options):-
  true.

%%% making publication %%%
action(make_publication, AgentID, _Options):-
  agent(Type,AgentID, Attributes),
  ActionPoints = Attributes.get(actionPoints),
  sim_time(Tick),
  ActionPoints >= 2,
  spend_actionPoints(AgentID,2),
  findall(BeliefContent,belief(AgentID,_,BeliefContent),BList),
  random_member(Belief,BList),
  uuid(PubID,[]),
  assertz(publication(AgentID,Tick,PubID,Belief)).

%%%  studying publications %%%
action(studying, AgentID, _Options):-
  agent(Type,AgentID, Attributes),
  ActionPoints = Attributes.get(actionPoints),
  ActionPoints >= 1,
  spend_actionPoints(AgentID,1),
  findall(PubID,publication(Atr,PT,PubID,PC),PublicationList),
  random_member(RndID,PublicationList),
  publication(Author,PubTime,RndID,PubContent),
  %CHECK DATA WITH OWN Belief
  %UPDATE reputation
  NPub = PubContent.put(original_author,Author),
  sim_time(Tick),
  assertz(belief(AgentID,Tick,NPub)).

% Application for founding; DFG Found
action(generating_ressources, AgentID, Options):-
  agent(Type,AgentID, Attributes),
  ActionPoints = Attributes.get(actionPoints),
  Reputation = Attributes.get(reputation),
  maxFounding(MaxFounding),
  maxInvestment(MaxInvestment),
  random(0,MaxFounding,APRandom),
  random(0,MaxInvestment,Investment),
  NewActionPoints is (round(APRandom * Reputation) + ActionPoints) - (round(Investment-Reputation)),
  retractall(agents:agent(_,AgentID,_)),
  %NewAttributes = Attributes.put(actionPoints,ActionPoints),
  assertz(agents:agent(Type,AgentID,Attributes.put(actionPoints,NewActionPoints))).

action(doing_empirical_research, AgentID, Options):-
  sim_time(Tick),
  agent(Type,AgentID, Attributes),
  ActionPoints = Attributes.get(actionPoints),
  ActionPoints >= 3,
  random(1,3,AP),
  spend_actionPoints(AgentID,AP),
  random(0,100,Observation),
  createFact(FactID,Observation),

  BeliefContent = belief_attributes{fact:FactID,degree:1.0},
  assertz(belief(AgentID,Tick,BeliefContent)).


spend_actionPoints(AgentID,AP):-
  agent(Type,AgentID, Attributes),
  ActionPoints = Attributes.get(actionPoints),
  NewActionPoints is ActionPoints - AP,
  retractall(agents:agent(_,AgentID,_)),
  assertz(agents:agent(Type,AgentID,Attributes.put(actionPoints,NewActionPoints))).



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% action_type(Name_of_Action,Type_of_Agent)
%%% determines which action will be used by wich agent type
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

action_list([doing_empirical_research,generating_ressources, studying, make_publication]).
