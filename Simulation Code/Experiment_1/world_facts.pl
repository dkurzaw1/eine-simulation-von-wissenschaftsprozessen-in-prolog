%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prolog Database and definition: World facts %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:- module(world_facts,[fact/1, fact/3, generate_fact/0, createFact/2]).
% fact(object,attribute).

% DIE WELT BESTEHT AUS FAKTEN, DIESE MUESSEN NICHT AUFGELOEST WERDEN!
% WERDEN DIESE UNTERSUCHT, ENTSTEHEN DATEN; DIESE BILDEN ASPEKTE EINES FAKTS
% AB UND HABEN EINE ABWEICHUNG

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generate random facts  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
:-dynamic(fact/1, fact/3).

generate_fact:-
  uuid(FACT_ID, []),
  assert( fact(FACT_ID) ).

createFact(FactID,Observation):-
  sim_time(Tick),
  uuid(FactID, []),
  worldTheory(Observation,Y),
  Content = content{x:Observation,y:Y,type:tuple},
  assertz( fact(FactID, Tick, Content) ).

worldTheory(X,Y):- Y is X^3.

% observation(agent1, object(objectid, attributes{fable:gelb})).


% Question: Why is 3 + 3 = 6?
% Explainatin:
 /*
entityDescription(object1,class1).
entityDescription(class1,attributes{perceptible:true}).
prototype_object(object1,attributes{}).
%entityDescription(unit1,attributes{measurable:true,representation:[a,b,c]}).

entityDescription(lemon,fruit).
entityDescription(fruit,attributes{class:true}).
entityDescription(ph,attributes{unit:true,values:[acid,neutral,base]}).

prototype_object(object1,attributes{measurable:true,color:[yellow,green],ph:acid}).
*/
% Aussagen:
% Beobachtugn, Allgennerierung: Alle beobachteten Objekte bezizen folgende Eigenschaften ...
% Beobachtung, Gesetz: Alle Objekte mit der Eigenschaft ... reagieren folgend ...
%
% zweites Newtonsches Gesetz:
/*
    F = m · a
    "F" ist die Kraft in Newton [ N ]
    "m" ist die Masse des Körpers in Kilogramm [ kg ]
    "a" ist die Beschleunigung in Meter pro Sekunde-Quadrat [ m/s2 ]

    F/m = a

*/
%entityDescription(unit1,attributes{measurable:true,representation:[yellow,green,blue]}).



% --> object(ObjectID,Property)
% --> entityDescription(ObjectId od. Entitity,Entity)
% objekt(zitrone,eigenschaft(farbe,gelb)).
% entityDescription(zitrone,[physicalObject]).
% entityDescription(physicalObject,[weight,shape,ph]).
% entityDescription(weight,[keineAhnung]).
% entityDescription(shape,[shape]).
% entityDescription(ph,[ph]).
% belief(agent1, objectclass(zitrone,property(colour,yellow))).
% belief(agent1, objectclass(zitrone,property(phwert,sauer))).
% belief(agent1, objectclass(zitrone,property(farbe,gruen))).
% belief(agent1, objectclass(zitrone,property(form,oval))).
% observation(agent1, object(temp1, property(fable,gelb))).
