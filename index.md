# Description

This repository contains simulation experiments and code for a modeling en the scientific research process. 
The simulation is written in a custom Prolog framework. A detailed description of the experiments and used theory is given in:
>  Kurzawe, Daniel. 2023. Die Dynamik von Forschung und Gesellschaft: Simulationen von Wissenschaftsprozessen. Hildesheim, München: Olms Universitätsbibliothek Ludwig-Maximilians-Universität München.
> DOI: 10.5282/edoc.29687
> Open Access: https://edoc.ub.uni-muenchen.de/29687/


# Beschreibung
Dieses Repositorium enthält eine eine Simulationsumgebung für eine modellhafte Abbildung und Untersuchung von prototypischen Wissenschaftsprozessen mit entsprechenden Rahmenhandlungen der Wissenschaft.
Die Simulation ist in einem eigens zu diesem Zweck geschaffenen Prolog Umgebung implementiert. 
Eine Beschreibung der konkreten Simulationsexperimente und der zugrunde liegenden Theorie findet sich in:
>  Kurzawe, Daniel. 2023. Die Dynamik von Forschung und Gesellschaft: Simulationen von Wissenschaftsprozessen. Hildesheim, München: Olms Universitätsbibliothek Ludwig-Maximilians-Universität München.
> DOI: 10.5282/edoc.29687
> Open Access: https://edoc.ub.uni-muenchen.de/29687/
